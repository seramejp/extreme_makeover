<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $asfTransactions->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $asfTransactions->date !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $asfTransactions->customer_id !!}</p>
</div>

<!-- Vehicle Id Field -->
<div class="form-group">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    <p>{!! $asfTransactions->vehicle_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $asfTransactions->product_id !!}</p>
</div>

<!-- Detail Id Field -->
<div class="form-group">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    <p>{!! $asfTransactions->detail_id !!}</p>
</div>

<!-- Odo Reading Field -->
<div class="form-group">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    <p>{!! $asfTransactions->odo_reading !!}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{!! $asfTransactions->subtotal !!}</p>
</div>

<!-- Km Run Field -->
<div class="form-group">
    {!! Form::label('km_run', 'Km Run:') !!}
    <p>{!! $asfTransactions->km_run !!}</p>
</div>

<!-- Is Done Field -->
<div class="form-group">
    {!! Form::label('is_done', 'Is Done:') !!}
    <p>{!! $asfTransactions->is_done !!}</p>
</div>

<!-- Is Asf Field -->
<div class="form-group">
    {!! Form::label('is_asf', 'Is Asf:') !!}
    <p>{!! $asfTransactions->is_asf !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $asfTransactions->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $asfTransactions->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $asfTransactions->deleted_at !!}</p>
</div>

