@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Asf Transactions
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($asfTransactions, ['route' => ['asfTransactions.update', $asfTransactions->id], 'method' => 'patch']) !!}

                        @include('asf_transactions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection