<table class="table table-hover table-striped after-sales-table" id="reports-table">
    <thead>
    <tr>
        <th>Date</th>
        <th>Customer</th>
        <th>Vehicle</th>
        <th>Detail</th>
        <th>Personnel</th>
        <th>Odo Reading</th>
        <th>Subtotal</th>
        <th>Km Run</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($asfTransactions as $asfTransactions)
        <tr>
            <td>{{ Carbon\Carbon::parse($asfTransactions->date)->format('Y/m/d') }}</td>
            <td><a href="<?=url('/')?>/transaction/transaction/{{ $asfTransactions->id }}">{!! $asfTransactions->customer_name !!}</a></td>
            <td>{!! $asfTransactions->vehicle_name !!}</td>
            <td>{!! $asfTransactions->detail_plate !!} ({!! $asfTransactions->detail_color !!})</td>
            <td>{!! $asfTransactions->personnel_name !!}</td>
            <td>{!! $asfTransactions->odo_reading !!}</td>
            <td>{!! $asfTransactions->subtotal !!}</td>
            <td>{!! $asfTransactions->km_run !!}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>