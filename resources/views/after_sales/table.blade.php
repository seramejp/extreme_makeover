<table class="table table-hover table-striped after-sales-table" id="reports-table">
    <thead>
    <tr>
        <th>Date</th>
        <th>Customer</th>
        <th>Vehicle</th>
        <th>Personnel</th>
        <th>Detail</th>
        <th>Odo Reading</th>
        <th>Subtotal</th>
        <th>Km Run</th>
        <th>ASF?</th>
    </tr>
    </thead>
    <tbody>
    @foreach($afterSales as $asf)
        <tr id="done{{$asf->id}}">
            <td>{{ Carbon\Carbon::parse($asf->date)->format('Y/m/d') }}</td>
            <td><a href="<?=url('/')?>/transaction/transaction/{{ $asf->id }}">{!! $asf->customer_name !!}</a></td>
            <td>{!! $asf->vehicle_name !!}</td>
            <td>{!! $asf->personnel_name !!}</td>
            <td>{!! $asf->detail_plate !!} ({!! $asf->detail_color !!})</td>
            <td>{!! $asf->odo_reading !!}</td>
            <td>{!! $asf->subtotal !!}</td>
            <td>{!! $asf->km_run !!}</td>
            <td class="table-cta-data">
                    <a href="#" onclick="return markasasf({{ $asf->id }});" class="table-edit-cta"><span class="glyphicon glyphicon-ok"></span></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<script>
  function markasasf(id)
    {
        var con = confirm("This transaction is going to ASF. Proceed?");
        if (con)
        {
            var url = "<?=url('/')?>/transaction/markAsAsf/" + id + "/";
            $.ajax({url: url, success: function(){
                $("#done" + id).fadeOut(300);
                $("#done" + id).hide();

            }});
            return false;
        }
    }
</script>