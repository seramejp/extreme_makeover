@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            After Sales
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($afterSales, ['route' => ['afterSales.update', $afterSales->id], 'method' => 'patch']) !!}

                        @include('after_sales.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection