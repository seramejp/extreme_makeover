<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Vehicle Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    {!! Form::number('vehicle_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Detail Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    {!! Form::number('detail_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Odo Reading Field -->
<div class="form-group col-sm-6">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    {!! Form::number('odo_reading', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Km Run Field -->
<div class="form-group col-sm-6">
    {!! Form::label('km_run', 'Km Run:') !!}
    {!! Form::number('km_run', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Done Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_done', 'Is Done:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_done', false) !!}
        {!! Form::checkbox('is_done', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('afterSales.index') !!}" class="btn btn-default">Cancel</a>
</div>
