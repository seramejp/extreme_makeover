<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $afterSales->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $afterSales->date !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer', 'Customer Id:') !!}
    <p>{!! $afterSales->customer_name !!} ({!! $afterSales->customer_contact !!})</p>
</div>

<!-- Vehicle Id Field -->
<div class="form-group">
    {!! Form::label('vehicle', 'Vehicle:') !!}
    <p>{!! $afterSales->vehicle_name !!}</p>
    <p>{!! $afterSales->detail_plate !!} ({!! $afterSales->detail_color !!})</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product', 'Product:') !!}
    <p>{!! $afterSales->product_name !!}</p>
</div>

<!-- Odo Reading Field -->
<div class="form-group">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    <p>{!! $afterSales->odo_reading !!}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{!! $afterSales->subtotal !!}</p>
</div>

<!-- Km Run Field -->
<div class="form-group">
    {!! Form::label('km_run', 'Km Run:') !!}
    <p>{!! $afterSales->km_run !!}</p>
</div>