@extends('layouts.admin')
@section('content') 
 <div id="page-wrapper">
    <div class="container-fluid">
      <h1 class="page-header">
        <span class="fa fa-pencil-square-o"></span> Edit {{ $userinfo->name }}
      </h1>
      @if(Session::has('flash_message'))
          <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
      @endif
      <div class="panel panel-default">
          <div class="panel-heading">Fill in user information</div>
          <div class="panel-body">
            <form action="{{ route('updateUser', $userinfo->id) }}" class="form-horizontal" method="POST">
                <input type="hidden" name="_method" value="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="col-md-4 control-label">Email</label>
                    <div class="col-md-6">
                        <input type="email" name="email" value="{{ $userinfo->email }}" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-4 control-label">New Password</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="role" class="col-md-4 control-label">Role</label>
                    <div class="col-md-6">
                        {!! Form::select('role_id', $roles, 0, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                      <a href="{{ $back }}" class="btn btn-default">Cancel</a>
                      <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </form>
          </div>
      </div>
    </div>
</div>
@stop