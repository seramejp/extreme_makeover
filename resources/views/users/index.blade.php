@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">
       <!-- Page Heading -->
       <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
              <span class="glyphicon glyphicon-user"></span> Users
          </h1>
          <a href="{!! route('user.create') !!}" class="cta-link"><span class="fa fa-plus-circle"></span> Add User</a>
        </div>
       </div>
       <!-- /.row -->
       <div class="row">
           <div class="col-lg-12">

                <table class="table table-hover table-striped" id="reports-table">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Role</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach( $userList as $user )
                          <tr>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->display_name }}</td>
                              <td class="table-cta-data"><a href="{{ url('/') }}/users/edit/{{ $user->id }}" class="table-edit-cta" title="Edit user"><span class="glyphicon glyphicon-edit"></span></a></td>
                          </tr>
                      @endforeach
                   </tbody>
                </table>
          </div>
       </div>
       <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@stop