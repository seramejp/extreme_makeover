@extends('layouts.admin')
@section('content') 
 <div id="page-wrapper">
    <div class="container-fluid">
      <h1 class="page-header">
        <span class="glyphicon glyphicon-user"></span> My Account
      </h1>
      @if(Session::has('flash_message'))
          <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
      @endif
      <div class="panel panel-default">
          <div class="panel-heading">Fill in your information</div>
          <div class="panel-body">
            <form action="{{ route('updateAccount', \Auth::user()->id) }}" method="POST">
                <input type="hidden" name="_method" value="POST">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" name="name" value="{{ \Auth::user()->name }} " class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" name="email" value="{{ \Auth::user()->email }} " class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="password">New Password</label>
                  <input id="password" type="password" class="form-control" name="password">
                </div>
                <a href="{{ $back }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
          </div>
      </div>
    </div>
</div>
@stop