<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <th>Name</th>
        <th></th>
    </thead>
    <tbody>
    @foreach($brands as $brands)
        <tr>
            <td><a href="{!! route('brands.show', [$brands->id]) !!}" title="View brand">{!! $brands->name !!}</a></td>
            <td class="table-cta-data">
                <a href="{!! route('brands.edit', [$brands->id]) !!}" class="table-edit-cta" title="Edit product" ><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::open(['route' => ['brands.destroy', $brands->id], 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>