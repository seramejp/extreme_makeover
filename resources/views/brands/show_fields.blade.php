<div class="data-full-info">
    <h1 class="page-header">{!! $brands->name !!}</h1><!-- Id Field -->
    <div class="panel panel-default">
        <div class="panel-heading">Brand information</div>
        <div class="panel-body">
          <div class="form-group col-md-6">
              {!! Form::label('id', 'Id:') !!}
              <p>{!! $brands->id !!}</p>
          </div>
          <!-- Created At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('created_at', 'Created At:') !!}
              <p>{!! $brands->created_at !!}</p>
          </div>
          <!-- Updated At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('updated_at', 'Updated At:') !!}
              <p>{!! $brands->updated_at !!}</p>
          </div>
          <!-- Deleted At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('deleted_at', 'Deleted At:') !!}
              <p>{!! $brands->deleted_at !!}</p>
          </div>
      </div>
    </div>
</div>