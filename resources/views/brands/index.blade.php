@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="glyphicon glyphicon-tag"></span> Brands
                    </h1>
                </div>
            </div>
            <a href="{!! route('brands.create') !!}" class="cta-link"><span class="fa fa-plus-circle"></span> Add Brand</a>
            <div class="content">
                <div class="clearfix"></div>

                @include('flash::message')

                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                        @include('brands.table')
                    </div>
                </div>
            </div>
        </div>
    </div>    
@endsection

