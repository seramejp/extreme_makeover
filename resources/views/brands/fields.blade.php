<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('name', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <a href="{!! route('brands.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::submit('Save Brand', ['class' => 'btn btn-default']) !!}
    </div>
</div>