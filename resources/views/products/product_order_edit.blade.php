@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Edit Product Order</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in product information</div>
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/update/order/' . $order_id)) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-6" id="transaction_date">
                                  <label for="date" class="col-md-2 control-label">Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="date" type="text" id="date"  value="{{ date('Y/m/d', strtotime($product_order->order_date)) }}"></div>
                              </div>
                              <div class="form-group col-md-6">
                                  <label for="supplier" class="col-md-2 control-label">Supplier</label>
                                  <div class="col-md-6">
                                      <select name="supplier" class="form-control">
                                          @foreach($suppliers as $supplier)
                                              @php
                                                  $selected = ($product_order->supplier_id == $supplier->id) ? 'selected' : '';
                                              @endphp
                                                  <option value="{{ $supplier->id }}" {{ $selected }}>{{ $supplier->name }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-6">
                                  <label for="order_type" class="col-md-2 control-label">Type</label>
                                  <div class="col-md-6">
                                      <select name="order_type" class="form-control">
                                              @foreach(array('Non Cash', 'Cash') as $key => $option)
                                                  @php
                                                      $selected = ($product_order->is_cash == $key) ? 'selected' : '';
                                                  @endphp                                                  
                                                  <option value="{{ $key }}" {{ $selected }}>{{ $option }}</option>
                                              @endforeach
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group col-md-6">
                                  <label for="" class="col-md-2 control-label">Charge Invoice</label>
                                  <div class="col-md-6">
                                      <input type="text" class="form-control" placeholder="Charge Invoice" id="charge_invoice" name="charge_invoice" value="{{$product_order->charge_invoice}}">
                                  </div>
                              </div>


                              <div class="col-md-12 product-order-detail transaction-products">
                              
                                  @foreach($product_order_details as $key => $product_order_detail)
                                          <div class="product-order-detail-row initial-product">
                                              <!-- Product Id Field -->
                                              <div class="col-md-3">
                                                  <label for="product_id" class="control-label">Product</label>
                                                  <p>{{ $product_order_detail->name }}</p>
                                                  <input type="hidden" name="product_ids[]" value="$product_order_detail->product_id">
                                              </div>

                                              <!-- Qty Field -->
                                              <div class="col-md-2">
                                                  <label for="qty" class="control-label">Qty</label>
                                                  <input class="form-control qty-field" name="qtys[]" type="number" value="{{ $product_order_detail->qty }}">
                                              </div>

                                              <!-- Price Field -->
                                              <div class="col-md-2">
                                                  <label for="price" class="control-label">Price</label>
                                                  <input class="form-control price-field" name="prices[]" type="number" value="{{ $product_order_detail->price }}">
                                              </div>

                                              <!-- Subtotal Field -->
                                              <div class="col-md-3">
                                                  <label for="subtotal" class="control-label">Subtotal</label>
                                                  <input class="form-control sub-total-field" name="subtotals[]" type="number" value="{{ $product_order_detail->sub_total }}">
                                              </div>
                                          </div>
                                  @endforeach

                              </div>
                              <div class="col-md-6">
                                  <label for="grand_total" class="col-md-2 control-label">Total</label>
                                  <div class="col-md-6"><input class="form-control total-amount" name="grand_total" type="number" value="{{ $product_order->grand_total }}"></div>
                              </div>                              
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Save Product Order Info">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
