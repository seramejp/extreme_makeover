<div class="data-full-info">
    <h1 class="page-header">{!! $products->name !!}</h1>
    <a href="{!! route('products.index') !!}" class="btn btn-default">Back</a> 
    <a href="{!! url('/products/' . $products->id . '/edit') !!}" class="btn btn-default">Edit Product</a> 

    <a href="" class="btn btn-danger pull-right" data-toggle='modal' data-target='#modalConfirmDeleteProduct'>Delete</a>
    <br><br>

    <div class="panel panel-default">
        <div class="panel-heading">Product information</div>
        <div class="panel-body">
            <!-- Id Field -->
            <div class="form-group col-md-6">
                {!! Form::label('id', 'Id:') !!}
                <p>{!! $products->id !!}</p>
            </div>
            <!-- Duration Field -->
            <div class="form-group col-md-6">
                {!! Form::label('duration', 'Duration:') !!}
                <p>{!! $products->duration !!}</p>
            </div>

            <!-- Type Id Field -->
            <div class="form-group col-md-6 hidden">
                {!! Form::label('type_id', 'Type:') !!}
                <p>{!! $products->type_id !!}</p>
            </div>

            <!-- Brand Id Field -->
            <div class="form-group col-md-6">
                {!! Form::label('brand_id', 'Brand:') !!}
                <p>{!! $products->brand_id !!}</p>
            </div>

            <!-- Price Field -->
            <div class="form-group col-md-6">
                {!! Form::label('price', 'Price:') !!}
                <p>{!! $products->price !!}</p>
            </div>

            <!-- Qty Field -->
            <div class="form-group">
                {!! Form::label('qty', 'Quantity', ['class' => 'col-md-4 control-label']) !!}
                <p>{!! $products->qty !!}</p>
            </div>

            <!-- ODO Reading Field -->
            <div class="form-group col-md-6">
                {!! Form::label('odo_reading', 'Price:') !!}
                <p>{!! $products->odo_reading !!}</p>
            </div>

            <!-- Created At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('created_at', 'Created At:') !!}
                <p>{!! $products->created_at !!}</p>
            </div>
            
            <!-- Updated At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('updated_at', 'Updated At:') !!}
                <p>{!! $products->updated_at !!}</p>
            </div>

            <!-- Deleted At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('deleted_at', 'Deleted At:') !!}
                <p>{!! $products->deleted_at !!}</p>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalConfirmDeleteProduct" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true" style="position: fixed; top: 20%;">
        
        <div class="modal-dialog">

            <!-- Modal content-->
            
                <div class="modal-content">
                    <div class="modal-header" style="background-color: black;">
                        <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                        <h4 class="modal-title text-white"><span class="text-danger"><i class="ti-alert"></i></span> Confirm <span class="text-danger"><strong>delete</strong></span> product.</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                            
                                
                                    <div class="form-group">
                                        <label for="" class="">Are you sure you want to delete this product?</label>
                                    </div>

                                    
                                
                            </div>
                           
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a id="btnDeleteProduct" href="{{ url('/products/delete/' . $products->id) }}" role="button" class="btn btn-danger btn-outline">Yes, I confirm</a>
                        <button type="button" class="btn btn-default btn-outline" data-dismiss="modal" id="cancelDeleteTransaction">Cancel</button>
                    </div>

                </div>
           
        </div>
    </div>


</div>
