@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
            </section>
            <div class="content">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row" style="padding-left: 20px">
                            @include('products.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
