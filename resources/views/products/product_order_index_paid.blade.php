@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="fa fa-list"></span> Paid Product Orders
                    </h1>
                </div>
            </div>
            <div class="content">
                <div class="clearfix"></div>
                    <table class="table table-hover table-striped product-order-table" id="po-table">
                        <thead>
                            <tr>
                                <th>Order Date</th>
                                <th>Supplier</th>
                                <th>Paid Amount</th>
                                <th>Total Amount</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($product_orders as $product_order)
                            <tr>
                                <td>{{ Carbon\Carbon::parse($product_order->order_date)->format('Y/m/d') }}</td>
                                <td><a href="{{ url('/') }}/product-orders/order/{{ $product_order->order_id }}">{{ $product_order->name }}</a></td>
                                <td>{{ number_format($product_order->paid, 2) }}</td>
                                <td>{{ number_format($product_order->grand_total, 2) }}</td>
                                <td class="table-cta-data">
                                    <a href="{{ url('/') }}/product-orders/edit/order/{{ $product_order->order_id }}" class="table-edit-cta"><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::open(['url' => 'product-orders/delete/order/' . $product_order->order_id, 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                                    {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

