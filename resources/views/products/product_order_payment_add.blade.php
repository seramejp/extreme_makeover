@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Product Order Payment</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in payment information</div>
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/add-payment/' . $order_id . '/insert')) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-12" id="transaction_date">
                                  <label for="date" class="col-md-3 control-label">Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="payment_date" type="text" id="date"  value="{{ date('Y/m/d') }}" required></div>
                              </div>
                              <div class="form-group col-md-12" id="delivery_date">
                                  <label for="amount" class="col-md-3 control-label">Amount</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="payment_amount" type="text" id="text"  value="" required></div>
                              </div>
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders/order/{{ $order_id }}" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Add">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
