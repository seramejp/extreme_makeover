@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Product Order Delivery</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in delivery information</div>
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/add-delivery/' . $order_id . '/insert' . $parameters)) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-12" id="transaction_date">
                                  <label for="date" class="col-md-3 control-label">Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="delivery_date" type="text" id="date"  value="{{ date('Y/m/d') }}" required></div>
                              </div>
                              <div class="form-group col-md-12">
                                  <label for="recieved_by" class="col-md-3 control-label">Received By</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="received_by" type="text" id="text"  value="" required></div>
                              </div>
                              @foreach($product_order_details as $product_order_detail)
                                  <div class="form-group col-md-12">
                                      <label for="delivered" class="control-label col-md-3">{{ $product_order_detail->name }} Delivered</label>
                                      <input class="form-control delivered-field" name="product[]" type="hidden" value="{{ $product_order_detail->product_id }}">
                                      <div class="col-md-6"><input class="form-control qty-field" name="delivered[]" type="number" value="0" required></div>
                                  </div>
                              @endforeach
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders/order/{{ $order_id }}" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Add">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
