<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('duration', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Type Id Field -->
<div class="form-group hidden">
    {!! Form::label('type', 'Type', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('type_id', $types, null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Brand Id Field -->
<div class="form-group">
    {!! Form::label('brand', 'Brand', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('brand_id', $brands, null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('price', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Quantity', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('qty', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- ODO Reading Field -->
<div class="form-group hidden">
    {!! Form::label('odo_reading', 'ODO Reading', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::hidden('odo_reading', 0, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Service Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('service_id', $services, null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::submit('Save Product', ['class' => 'btn btn-default']) !!}
    </div>
</div>

