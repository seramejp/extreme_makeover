@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">
        <div class="data-full-info">
            <h1 class="page-header">Product Order</h1>
        </div>
        <a href="{{ url('/') }}/product-orders/edit/order/{{ $product_order->order_id }}"" class="cta-link"><span class="fa fa-plus-circle"></span> Edit Product Order</a>
        @if(Session::has('product-order-update-message'))
            <div class="alert alert-success">
                {{ Session::get('product-order-update-message') }}
            </div>
        @endif
        @if(Session::has('product-delivery-update-message'))
            <div class="alert alert-success">
                {{ Session::get('product-delivery-update-message') }}
            </div>
        @endif     
        @if(Session::has('product-delivery-add-message'))
            <div class="alert alert-success">
                {{ Session::get('product-delivery-add-message') }}
            </div>
        @endif  
        @if(Session::has('product-payment-add-message'))
            <div class="alert alert-success">
                {{ Session::get('product-payment-add-message') }}
            </div>
        @endif 
        @if(Session::has('product-payment-update-message'))
            <div class="alert alert-success">
                {{ Session::get('product-payment-update-message') }}
            </div>
        @endif         
        <div class="panel panel-default">
            <div class="panel-heading">Product order information</div>
            <div class="panel-body">
                    <div class="form-group col-md-6">
                        <label for="contact">Supplier:</label>
                        <p>{{ $product_order-> name }}</p>
                    </div>
                    
                    <div class="form-group col-md-6">
                        <label for="contact">Charge Invoice:</label>
                        <p>{{ $product_order->charge_invoice }}</p>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="make">Order Date:</label>
                        <p>{{ Carbon\Carbon::parse($product_order->order_date)->format('Y/m/d') }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact">Order Type:</label>
                        <p>
                            @if($product_order-> is_cash > 0)
                                Cash
                            @else
                                Non Cash
                            @endif
                        </p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact">Status:</label>
                        @if($product_order->is_paid == 1)
                            <p>Paid</p>
                        @else
                            <p>Unpaid</p>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact">Total Balance:</label>
                        <p>{{ number_format($product_order->balance, 2) }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact">Total Paid:</label>
                        <p>{{ number_format($product_order->paid, 2) }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="contact">Total Amount:</label>
                        <p>{{ number_format($product_order->grand_total, 2) }}</p>
                    </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Product order items</div>
            <div class="panel-body">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th>Received</th>
                            <th>Unreceived</th>

                        </tr>
                    </thead>
                        <tbody>
                            @foreach($product_order_details as $product_order_detail)
                                <tr>
                                    <td>{{ $product_order_detail->name }}</td>
                                    <td>{{ $product_order_detail->price }}</td>
                                    <td>{{ $product_order_detail->qty }}</td>
                                    <td>{{ $product_order_detail->sub_total }}</td>
                                    <td>{{ $product_order_detail->received_qty }}</td>
                                    <td>{{ $product_order_detail->unreceived_qty }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
         <a href="{{ url('/') }}/product-orders/add-delivery/{{ $product_order->order_id }}"" class="cta-link-right"><span class="fa fa-plus-circle"></span> Add Product Order Delivery</a>
        <div class="panel panel-default">
            <div class="panel-heading">Product order delivery log</div>
            <div class="panel-body">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th>Received By</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($delivery_logs as $delivery_log)
                                <tr>
                                    <td>{{ Carbon\Carbon::parse($delivery_log->received_date)->format('Y/m/d') }}</td>
                                    <td><a href="{{ url('/') }}/product-orders/edit-delivery/{{ $delivery_log->id }}">{{ $delivery_log->product_name }}</a></td>
                                    <td>{{ $delivery_log->received_qty }}</td>
                                    <td>{{ $delivery_log->received_by }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
         <a href="{{ url('/') }}/product-orders/add-payment/{{ $product_order->order_id }}"" class="cta-link-right"><span class="fa fa-plus-circle"></span> Add Product Order Payment</a>
        <div class="panel panel-default">
            <div class="panel-heading">Product order payment log</div>
            <div class="panel-body">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Payment Date</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                        <tbody>
                            @foreach($payment_logs as $payment_log)
                                <tr>
                                    <td><a href="{{ url('/') }}/product-orders/edit-payment/{{ $payment_log->id }}">{{ Carbon\Carbon::parse($payment_log->payment_date)->format('Y/m/d') }}</a></td>
                                    <td>{{ $payment_log->payment_amount }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
            </div>
        </div>
   </div>
   <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@stop