@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="glyphicon glyphicon-edit"></span> Edit Product Order Delivery</h1>
            </section>
            <div class="panel panel-default">
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/edit-payment/' . $payment_detail->id . '/update')) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-12"  id="transaction_date">
                                  <label for="date" class="col-md-3 control-label">Payment Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="payment_date" type="text" id="date"  value="{{ date('Y/m/d', strtotime($payment_detail->payment_date)) }}" required></div>
                              </div>
                              <div class="form-group col-md-12" id="delivery_date">
                                  <label for="recieved_by" class="col-md-3 control-label">Payment Amount</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="payment_amount" type="text" id="text"  value="{{ $payment_detail->payment_amount }}" required></div>
                              </div>
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders/order/{{ $payment_detail->order_id }}" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Save">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
