@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Purchase Order</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in product information</div>
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/insert')) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-6" id="transaction_date">
                                  <label for="date" class="col-md-2 control-label">Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="date" type="text" id="date"  value="{{ date('Y/m/d') }}"></div>
                              </div>
                              <div class="form-group col-md-6">
                                  <label for="supplier" class="col-md-2 control-label">Supplier</label>
                                  <div class="col-md-6">
                                      <select name="supplier" class="form-control">
                                          @foreach($suppliers as $supplier)
                                              <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="form-group col-md-6">
                                  <label for="order_type" class="col-md-2 control-label">Type</label>
                                  <div class="col-md-6">
                                      <select name="order_type" class="form-control">
                                          <option value="1">Cash</option>
                                          <option value="0">Non Cash</option>
                                      </select>
                                  </div>
                              </div>

                              <div class="form-group col-md-6">
                                  <label for="" class="col-md-2 control-label">Charge Invoice</label>
                                  <div class="col-md-6">
                                      <input type="text" class="form-control" placeholder="Charge Invoice" id="charge_invoice" name="charge_invoice">
                                  </div>
                              </div>

                              <div class="col-md-12 product-order-detail transaction-products">
                                  <div class="product-order-detail-row initial-product">
                                      <!-- Product Id Field -->
                                      <div class="col-md-3">
                                          <label for="product_id" class="control-label">Product</label>
                                          <select class="form-control" name="product_ids[]">
                                              @foreach($products as $product)
                                                  <option value="{{ $product->id }}">{{ $product->name }}</option>
                                              @endforeach
                                          </select>
                                      </div>

                                      <!-- Qty Field -->
                                      <div class="col-md-2">
                                          <label for="qty" class="control-label">Qty</label>
                                          <input class="form-control qty-field" name="qtys[]" type="number" value="1">
                                      </div>

                                      <!-- Price Field -->
                                      <div class="col-md-2">
                                          <label for="price" class="control-label">Price</label>
                                          <input class="form-control price-field" name="prices[]" type="number" value="0">
                                      </div>

                                      <!-- Subtotal Field -->
                                      <div class="col-md-3">
                                          <label for="subtotal" class="control-label">Subtotal</label>
                                          <input class="form-control sub-total-field" name="subtotals[]" type="number" value="0">
                                      </div>
                                      <!-- add / minus -->
                                      <div class="col-md-2">
                                          <div class="transaction-form-cta">
                                              <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="copy hide">
                                      <div class="product-order-detail-row">
                                          <!-- Product Id Field -->
                                          <div class="col-md-3">
                                              <label for="product_id" class="control-label">Product</label>
                                              <select class="form-control" name="product_ids[]">
                                                  @foreach($products as $product)
                                                      <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                  @endforeach
                                              </select> 
                                          </div>

                                          <!-- Qty Field -->
                                          <div class="col-md-2">
                                              <label for="qty" class="control-label">Qty</label>
                                              <input class="form-control qty-field" name="qtys[]" type="number" value="1">
                                          </div>

                                          <!-- Price Field -->
                                          <div class="col-md-2">
                                              <label for="price" class="control-label">Price</label>
                                              <input class="form-control price-field" name="prices[]" type="number" value="0">
                                          </div>

                                          <!-- Subtotal Field -->
                                          <div class="col-md-3">
                                              <label for="subtotal" class="control-label">Subtotal</label>
                                              <input class="form-control sub-total-field" name="subtotals[]" type="number" value="0">
                                          </div>

                                          <!-- add / minus -->
                                          <div class="col-md-2">
                                              <div class="transaction-form-cta">
                                                  <div class="col-md-6">
                                                      <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                                  </div>
                                                  <div class="col-md-6">
                                                      <button class="btn btn-success clone-add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                                  </div>  
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <label for="grand_total" class="col-md-2 control-label">Total</label>
                                  <div class="col-md-6"><input class="form-control total-amount" name="grand_total" type="number" value="0"></div>
                              </div>                              
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Save">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>


@endsection
