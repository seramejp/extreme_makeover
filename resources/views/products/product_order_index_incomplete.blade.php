@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="fa fa-list"></span> Incomplete Product Orders
                    </h1>
                </div>
            </div>

            @if(Session::has('product-order-delete-message'))
                <div class="alert alert-success">
                    {{ Session::get('product-order-delete-message') }}
                </div>
            @endif
            <div class="content">
                <div class="clearfix"></div>
                    <table class="table table-hover table-striped product-order-table" id="po-table">
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Date</th>
                                <th>Product</th>
                                <th>Supplier</th>
                                <th>Total Qty Ordered</th>
                                <th>Unreceived</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($incomplate_product_orders as $product_order_detail)
                                <tr>
                                    <td>{{ $product_order_detail->order_id }}</td>
                                    <td>{{ Carbon\Carbon::parse($product_order_detail->order_date)->format('Y/m/d') }}</td>
                                    <td>{{ $product_order_detail->name }}</td>
                                    <td>{{ $product_order_detail->supplier_name }}</td>
                                    <td>{{ $product_order_detail->qty }}</td>
                                    <td>{{ $product_order_detail->unreceived_qty }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

