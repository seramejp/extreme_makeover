@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="glyphicon glyphicon-edit"></span> Edit Product Order Delivery</h1>
            </section>
            <div class="panel panel-default">
                    <div class="panel-body">
                    {{ Form::open(array('url' => 'product-orders/edit-delivery/' . $product_delivery->id . '/update')) }}
                        <div class="product-order-container">
                              <div class="form-group col-md-12"  id="transaction_date">
                                  <label for="date" class="col-md-3 control-label">Date</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="delivery_date" type="text" id="date"  value="{{ date('Y/m/d', strtotime($product_delivery->received_date)) }}" required></div>
                              </div>
                              <div class="form-group col-md-12" id="delivery_date">
                                  <label for="recieved_by" class="col-md-3 control-label">Received By</label>
                                  <div class="col-md-6"><input class="form-control" required="1" name="received_by" type="text" id="text"  value="{{ $product_delivery->received_by }}" required></div>
                              </div>
                              <div class="form-group col-md-12">
                                  <label for="delivered" class="control-label col-md-3">{{ $product_delivery->product_name }} Delivered</label>
                                  <div class="col-md-6"><input class="form-control qty-field" name="delivered" type="number" value="{{ $product_delivery->received_qty }}" required></div>
                              </div>
                              <div class="col-md-12 product-order-cta">
                                  <a href="{{ url('/') }}/product-orders/order/{{ $product_delivery->order_id }}" class="btn btn-default">Cancel</a>
                                  <input class="btn btn-default" type="submit" value="Save">
                              </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

@endsection
