<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Duration</th>
            {{--<th>Type</th>--}}
            <th>Brand</th>
            <th>Price</th>
            <th>Qty</th>
            <th>Service</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $products)
        <tr>
            <td><a href="{!! route('products.show', [$products->id]) !!}" title="View product">{!! $products->name !!}</a></td>
            <td>{!! $products->duration !!}</td>
            {{--<td>{!! $products->type !!}</td>--}}
            <td>{!! $products->brand !!}</td>
            <td>{!! $products->price !!}</td>
            <td>{!! $products->qty !!}</td>
            <td>{!! $products->service !!}</td>
            <td class="table-cta-data">
                <a href="{!! route('products.edit', [$products->id]) !!}" class="table-edit-cta" title="Edit product"><span class="glyphicon glyphicon-edit"></span></a>
                {!! Form::open(['route' => ['products.destroy', $products->id], 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>