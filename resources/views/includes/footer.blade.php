<!-- jQuery -->
<script src="<?=url('/')?>/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?=url('/')?>/js/bootstrap.min.js"></script>

{{--<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>--}}
<script src="<?=url('/')?>/js/datatables/datatables.min.js"></script>
<script src="<?=url('/')?>/js/datatables/datatables.buttons.min.js"></script>
<script src="<?=url('/')?>/js/datatables/buttons.print.min.js"></script>
<script src="<?=url('/')?>/js/datatables/buttons.html5.min.js"></script>
<script src="<?=url('/')?>/js/datatables/buttons.bootstrap.min.js"></script>
<script src="<?=url('/')?>/js/datatables/select.min.js"></script>

<!-- Custom JS -->
<script src="{{ URL::asset('js/custom.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>