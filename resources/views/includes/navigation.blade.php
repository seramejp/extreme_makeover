        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=url('/')?>"><img src="<?=url('/')?>/img/logo.jpg"/></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ \Auth::user()->name }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?=url('/')?>/account"><span class="fa fa-user"></span> Account</a>
                        </li>
                        <!-- <li>
                            <a href="<?=url('/')?>/notifications"><span class="fa fa-exclamation-circle"></span> Notifications</a>
                        </li> -->
                        <li class="divider"></li>
                        <li>
                            <a href="<?=url('/')?>/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse" id="navigation">

                <ul class="nav navbar-nav side-nav">
                    <li class="{{ Request::path() == '/' ? 'active' : '' }}">
                        <a href="<?=url('/')?>/"><span class="fa fa-bars"></span> Transactions</a>
                        <ul style="display: block;">
                            <li class="{{ Request::is('quotes*') ? 'active' : '' }}">
                                <a href="{!! route('quotes.index') !!}"><span class="fa fa-shopping-cart"></span> Temporary Service Records</a>
                            </li>
                            <li class="{{ Request::path() == 'reports/notifications' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/reports/notifications"><span class="fa fa-clock-o"></span> Pending</a>
                            </li>
                            <li class="{{ Request::path() == 'afterSales' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/afterSales"><span class="fa fa-check"></span> Done</a>
                            </li>
                            <li class="{{ Request::path() == 'asfTransactions' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/asfTransactions"><span class="fa fa-cogs"></span> ASF</a>
                            </li>
                            <li class="{{ Request::path() == 'transaction/index' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/transaction/index"><span class="fa fa-upload"></span> Import</a>
                            </li>
                            <li class="{{ Request::is('payments*') ? 'active' : '' }}">
                                <a href="{!! route('payments.index') !!}"><span class="fa fa-credit-card"></span> Payments</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('notifications/*') ? 'active' : '' }}">
                '       <a href="<?=url('/')?>/notifications/walk-in/pending"><span class="glyphicon glyphicon-exclamation-sign"></span> Nofications</a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
                        <ul>
                            <li class="{{ Request::is('notifications/walk-in/*') ? 'active' : '' }}">
                                <a href="<?=url('/')?>/notifications/walk-in/pending"><span class="fa fa-user"></span> Walk-in</a>
                            </li>
                            <li class="{{ Request::is('notifications/corporate/*') ? 'active' : '' }}">
                                <a href="<?=url('/')?>/notifications/corporate/pending"><span class="fa fa-user"></span> Corporate</a>
                            </li>
                            <li class="{{ Request::is('notifications/birthdays/*') ? 'active' : '' }}">
                                <a href="<?=url('/')?>/notifications/birthdays/current"><span class="glyphicon glyphicon-gift"></span> Birthdays</a>
                            </li>
                            <li class="{{ Request::is('notifications/refresh-notification') ? 'active' : '' }}">
                                <a href="<?=url('/')?>/notifications/refresh-notification"><span class="fa fa-refresh"></span> Refresh</a>
                            </li>
                        </ul>
                    </li>                    
                    <li class="{{ Request::path() == 'reports' ? 'active' : '' }}">
                        <a href="<?=url('/')?>/reports"><span class="fa fa-bar-chart"></span> Reports</a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>

                        <ul>
                            <li class="{{ Request::path() == 'accountsReceivable' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/accountsReceivable"><span class="fa fa-calendar"></span>Account Receivables</a>
                            </li>
                            <li class="{{ Request::path() == 'priceHistories' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/priceHistories"><span class="fa fa-calendar-o"></span> Price History</a>
                            </li>
                            <li class="{{ Request::path() == 'qtyHistories' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/qtyHistories"><span class="fa fa-calendar"></span> Stock History</a>
                            </li>
                            <li class="{{ Request::path() == 'accountsPayable' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/accountsPayable"><span class="fa fa-calendar-o"></span> Account Payables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ Request::path() == 'products' ? 'active' : '' }}">
                        <a href="<?=url('/')?>/products"><span class="fa fa-list"></span> Products</a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
                        <ul>

                            <li class="{{ Request::path() == 'brands' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/brands"><span class="glyphicon glyphicon-tag"></span> Brands</a>
                            </li>
                            <!--li class="{{ Request::path() == 'types' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/types"><span class="glyphicon glyphicon-tags"></span> Types</a>
                            </li-->
                            <li class="{{ Request::is('services*') ? 'active' : '' }}">
                                <a href="{!! route('services.index') !!}"><span class="fa fa-edit"></span> Services</a>
                            </li>

                        </ul>
                    </li>
                    <li class="{{ Request::path() == 'customers' ? 'active' : '' }}">
                        <a href="<?=url('/')?>/customers"><span class="fa fa-users"></span> Customers</a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
                        <ul>
                            <li class="{{ Request::path() == 'walkins' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/walkins"><span class="fa fa-user"></span> Walk-in</a>
                            </li>
                            <li class="{{ Request::path() == 'corporates' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/corporates"><span class="fa fa-user"></span> Corporate</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
                        <a href="{!! route('suppliers.index') !!}"><span class="fa fa-archive" aria-hidden="true"></span> <span>Suppliers</span></a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
                        <ul>
                            <li class="{{ Request::path() == 'product-orders' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/product-orders"><span class="glyphicon glyphicon-tag"></span> Orders</a>
                            </li>
                            <li class="{{ Request::path() == 'product-orders/incomplete' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/product-orders/incomplete"><span class="glyphicon glyphicon-tag"></span> Incomplete Orders</a>
                            </li>
                            <li class="{{ Request::path() == 'product-orders/paid' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/product-orders/paid"><span class="glyphicon glyphicon-tag"></span> Paid Orders</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{ Request::path() == 'users' ? 'active' : '' }}">
                        <a href="<?=url('/')?>/users"><span class="fa fa-user"></span> Users</a>
                        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
                        <ul>
                            <li class="{{ Request::path() == 'personnels' ? 'active' : '' }}">
                                <a href="<?=url('/')?>/personnels"><span class="fa fa-user"></span> Personnel</a>
                            </li>
                            <li class="{{ Request::is('roles*') ? 'active' : '' }}">
                                <a href="{!! route('roles.index') !!}"><span class="fa  fa-users"></span> Roles</a>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>
