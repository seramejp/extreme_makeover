<ul class="nav navbar-nav side-nav">
    <li class="{{ Request::path() == '/' ? 'active' : '' }}">
        <a href="<?=url('/')?>/"><span class="fa fa-bars"></span> Transactions</a>
        <ul style="display: block;">
            <li class="{{ Request::path() == 'transaction/index' ? 'active' : '' }}">
                <a href="<?=url('/')?>/transaction/index"><span class="fa fa-upload"></span> Import</a>
            </li>
        </ul>
    </li>
    <li class="{{ Request::path() == 'products' ? 'active' : '' }}">
        <a href="<?=url('/')?>/products"><span class="fa fa-list"></span> Products</a>
        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
        <ul>

            <li class="{{ Request::path() == 'brands' ? 'active' : '' }}">
                <a href="<?=url('/')?>/brands"><span class="glyphicon glyphicon-tag"></span> Brands</a>
            </li>
            <!--li class="{{ Request::path() == 'types' ? 'active' : '' }}">
                <a href="<?=url('/')?>/types"><span class="glyphicon glyphicon-tags"></span> Types</a>
            </li-->
            <li class="{{ Request::is('services*') ? 'active' : '' }}">
                <a href="{!! route('services.index') !!}"><span class="fa fa-edit"></span> Services</a>
            </li>

        </ul>
    </li>
    <li class="{{ Request::path() == 'customers' ? 'active' : '' }}">
        <a href="<?=url('/')?>/customers"><span class="fa fa-users"></span> Customers</a>
        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
        <ul>
            <li class="{{ Request::path() == 'walkins' ? 'active' : '' }}">
                <a href="<?=url('/')?>/walkins"><span class="fa fa-user"></span> Walk-in</a>
            </li>
            <li class="{{ Request::path() == 'corporates' ? 'active' : '' }}">
                <a href="<?=url('/')?>/corporates"><span class="fa fa-user"></span> Corporate</a>
            </li>
        </ul>
    </li>
</ul>
