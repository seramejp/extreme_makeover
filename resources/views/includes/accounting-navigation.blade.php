<ul class="nav navbar-nav side-nav">
    <li class="{{ Request::path() == '/' ? 'active' : '' }}">
        <a href="<?=url('/')?>/"><span class="fa fa-bars"></span> Transactions</a>
        <ul style="display: block;">
            <li class="{{ Request::path() == 'transaction/index' ? 'active' : '' }}">
                <a href="<?=url('/')?>/transaction/index"><span class="fa fa-upload"></span> Import</a>
            </li>
            <li class="{{ Request::is('payments*') ? 'active' : '' }}">
                <a href="{!! route('payments.index') !!}"><span class="fa fa-credit-card"></span> Payments</a>
            </li>
        </ul>
    </li>           
    <li class="{{ Request::path() == 'reports' ? 'active' : '' }}">
        <a href="<?=url('/')?>/reports"><span class="fa fa-bar-chart"></span> Reports</a>
        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>

        <ul>
            <li class="{{ Request::path() == 'accountsReceivable' ? 'active' : '' }}">
                <a href="<?=url('/')?>/accountsReceivable"><span class="fa fa-calendar"></span>Account Receivables</a>
            </li>
            <li class="{{ Request::path() == 'priceHistories' ? 'active' : '' }}">
                <a href="<?=url('/')?>/priceHistories"><span class="fa fa-calendar-o"></span> Price History</a>
            </li>
            <li class="{{ Request::path() == 'qtyHistories' ? 'active' : '' }}">
                <a href="<?=url('/')?>/qtyHistories"><span class="fa fa-calendar"></span> Stock History</a>
            </li>
            <li class="{{ Request::path() == 'accountsPayable' ? 'active' : '' }}">
                <a href="<?=url('/')?>/accountsPayable"><span class="fa fa-calendar-o"></span> Account Payables</a>
            </li>
        </ul>
    </li>     
    <li class="{{ Request::path() == 'products' ? 'active' : '' }}">
        <a href="<?=url('/')?>/products"><span class="fa fa-list"></span> Products</a>
        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
        <ul>
            <li class="{{ Request::path() == 'brands' ? 'active' : '' }}">
                <a href="<?=url('/')?>/brands"><span class="glyphicon glyphicon-tag"></span> Brands</a>
            </li>
            <li class="{{ Request::path() == 'types' ? 'active' : '' }}">
                <a href="<?=url('/')?>/types"><span class="glyphicon glyphicon-tags"></span> Types</a>
            </li>
            <li class="{{ Request::is('services*') ? 'active' : '' }}">
                <a href="{!! route('services.index') !!}"><span class="fa fa-edit"></span> Services</></a>
            </li>
        </ul>
    </li>
    <li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
        <a href="{!! route('suppliers.index') !!}"><span class="fa fa-archive" aria-hidden="true"></span> <span>Suppliers</span></a>
        <a href="#" class="glyphicon glyphicon-chevron-down drop-down-cta"></a>
        <ul>
            <li class="{{ Request::path() == 'product-orders' ? 'active' : '' }}">
                <a href="<?=url('/')?>/product-orders"><span class="glyphicon glyphicon-tag"></span> Orders</a>
            </li>
            <li class="{{ Request::path() == 'product-orders/incomplete' ? 'active' : '' }}">
                <a href="<?=url('/')?>/product-orders/incomplete"><span class="glyphicon glyphicon-tag"></span> Incomplete Orders</a>
            </li>
            <li class="{{ Request::path() == 'product-orders/paid' ? 'active' : '' }}">
                <a href="<?=url('/')?>/product-orders/paid"><span class="glyphicon glyphicon-tag"></span> Paid Orders</a>
            </li>
        </ul>
    </li>
</ul>