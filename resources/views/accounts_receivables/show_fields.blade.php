<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $accountsReceivable->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('Name', 'Name:') !!}
    <p>{!! $accountsReceivable->Name !!}</p>
</div>

<!-- Unit Field -->
<div class="form-group">
    {!! Form::label('Unit', 'Unit:') !!}
    <p>{!! $accountsReceivable->Unit !!}</p>
</div>

<!-- Plate No Field -->
<div class="form-group">
    {!! Form::label('Plate_No', 'Plate No:') !!}
    <p>{!! $accountsReceivable->Plate_No !!}</p>
</div>

<!-- Last Change Oil Field -->
<div class="form-group">
    {!! Form::label('last_change_oil', 'Last Change Oil:') !!}
    <p>{!! $accountsReceivable->last_change_oil !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $accountsReceivable->type !!}</p>
</div>

<!-- Grand Total Field -->
<div class="form-group">
    {!! Form::label('grand_total', 'Grand Total:') !!}
    <p>{!! $accountsReceivable->grand_total !!}</p>
</div>

<!-- Paid Field -->
<div class="form-group">
    {!! Form::label('paid', 'Paid:') !!}
    <p>{!! $accountsReceivable->paid !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $accountsReceivable->balance !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $accountsReceivable->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $accountsReceivable->updated_at !!}</p>
</div>

