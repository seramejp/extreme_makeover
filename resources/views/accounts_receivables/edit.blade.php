@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Accounts Receivable
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($accountsReceivable, ['route' => ['accountsReceivables.update', $accountsReceivable->id], 'method' => 'patch']) !!}

                        @include('accounts_receivables.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection