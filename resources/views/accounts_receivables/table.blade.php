<table class="table table-responsive" id="accountsReceivables-table">
    <thead>
        <th>Name</th>
        <th>Unit</th>
        <th>Plate No</th>
        <!-- <th>Last Change Oil</th> -->
        <th>Transaction Date</th>
        <th>Type</th>
        <th>Grand Total</th>
        <th>Paid</th>
        <th>Balance</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($accountsReceivables as $accountsReceivable)
        <tr>
            <td>{!! $accountsReceivable->Name !!}</td>
            <td>{!! $accountsReceivable->Unit !!}</td>
            <td>{!! $accountsReceivable->Plate_No !!}</td>
            <td>{!! $accountsReceivable->date !!}</td>
            <td>{!! $accountsReceivable->type !!}</td>
            <td>{!! $accountsReceivable->grand_total !!}</td>
            <td>{!! $accountsReceivable->paid !!}</td>
            <td>{!! $accountsReceivable->balance !!}</td>
            <td>
                {!! Form::open(['route' => ['accountsReceivables.destroy', $accountsReceivable->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('accountsReceivables.show', [$accountsReceivable->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('accountsReceivables.edit', [$accountsReceivable->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>