@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="glyphicon glyphicon-calendar"></span> Accounts Receivables
                    </h1>
                <!-- <a href="{{-- route('accountsReceivable.create') --}}" class="cta-link"><span class="fa fa-plus-circle"></span> Add Account</a> -->
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">

                    <table class="table table-hover table-striped" id="ar-table">
                        <thead>

                        <tr>
                            <th>Name</th>
                            <th>Vehicle</th>
                            <th>Last Change Oil</th>
                            <th>Type</th>
                            <th>Grand Total</th>
                            <th>Total Paid</th>
                            <th>Balance</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($accountsReceivables as $ar)
                            @if((floatval($ar->total) - floatval($ar->amount)) > 0) {{-- balance = total-amount; balance > 0 --}}
                            <tr>
                                <td>{{ $ar->name }}</td>
                                <td>{{ $ar->make . "(" . $ar->plate . "), " . $ar->color }}</td>
                                <td>{{ $ar->transdate }}</td>
                                <td>{{ ($ar->type == "1" ? "Corporate" : "Walk-in") }}</td>
                                <td>{{ number_format($ar->total,2) }}</td>
                                <td>{{ number_format($ar->amount, 2) }}</td>
                                <td>{{ number_format(intval($ar->total) - intval($ar->amount), 2) }}</td>
                                <td></td>
                            </tr>
                            @endif
                        @endforeach

                        </tbody>

                        <tfoot>
                        <tr>
                            <th  colspan="4" style="font-size: 16px;">TOTAL</th>
                            <th  colspan="3" style="text-align:right; font-size: 16px;">Total:</th>
                            <th></th>
                        </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@stop