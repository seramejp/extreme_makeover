<table class="table table-hover table-striped" id="pricehistory-table">
    <thead>
        <th>Product</th>
        <th>Price</th>
        <th>Date Updated</th>
        <th></th>
    </thead>
    <tbody>
        @foreach($priceHistories as $priceHistory)
            <tr>
                <td>{{ $priceHistory->name }}</td>
                <td>{{ $priceHistory->price }}</td>
                <td>{{ Carbon\Carbon::parse($priceHistory->created_at)->format('Y/m/d') }}</td>
                <td></td>
            </tr>
        @endforeach
    </tbody>
</table>