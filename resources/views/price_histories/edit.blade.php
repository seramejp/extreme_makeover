@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Price History
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($priceHistory, ['route' => ['priceHistories.update', $priceHistory->id], 'method' => 'patch']) !!}

                        @include('price_histories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection