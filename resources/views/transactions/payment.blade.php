<!-- Payment Information -->
{{--@include('transactions/services')--}}
{{--@include('transactions/products')--}}
<!-- Total Field -->
<div class="form-group col-md-6">
    {!! Form::label('subtotal', 'Subtotal', ['class' => 'col-md-4 control-label']) !!}
    {{--//pass subtotal from service to here--}}
    <div class="col-md-6">{!! Form::number('subtotal', "", ['class' => 'form-control total-amount', 'required' => true]) !!}</div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('discount', 'Discount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('discount', "0", ['class' => 'form-control discount-amount', 'onkeyup' => 'return recalculate()', 'required' => true]) !!}</div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('total', 'Total Amount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('total', "", ['class' => 'form-control grandtotal-amount']) !!}</div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('rendered', 'Initial Amount Paid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('rendered', "0", ['class' => 'form-control paid-amount', 'onkeyup' => 'return recalculate()', 'required' => true]) !!}</div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('balance', 'Balance', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('balance', "0", ['class' => 'form-control balance-amount']) !!}</div>
</div>

<script>
    function recalculate()
    {

        /*var productsubtotal = parseFloat($("input[name = 'subtotals[]']").val());
        var servicesubtotal = parseFloat($("input[name = 'service_subtotals[]']").val());
        var subtotal    = productsubtotal + servicesubtotal;*/
        var discount    = parseFloat($("#discount").val());
        var total       = parseFloat($("#subtotal").val()) - parseFloat(discount);
        var rendered    = parseFloat($("#rendered").val());
        var balance     = total - rendered;

        /*$('#subtotal').val(subtotal) ;
        $("#discount").val(discount);*/
        $("#total").val(total);
        /*$("#rendered").val(rendered);*/
        $("#balance").val(balance);
        console.log("+");
        return false;

    }
</script>
