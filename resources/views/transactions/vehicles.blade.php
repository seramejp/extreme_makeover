@foreach($vehicles as $vehicle)
    <option value="{{ $vehicle->id }}">{{ $vehicle->make }} ({{ $vehicle->plate }}, {{ ucwords(strtolower(($vehicle->color))) }})</option>
@endforeach