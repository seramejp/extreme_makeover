@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="fa fa-list"></span> Transactions
                    </h1>
                </div>
            </div>
            {{--<a href="{!! route('transactions.create') !!}" class="cta-link"><span class="fa fa-plus-circle"></span> Create New Transaction</a>--}}
            <div class="cta-link">
                <a href="{{ url('/transaction/create/walkin') }}"><span class="fa fa-plus-circle"></span> Create New - Walk-in</a> &nbsp; | &nbsp;
                <a href="{!! url('/transaction/create/corporate') !!}"><span class="fa fa-plus-circle"></span> Create New - Corporate</a> &nbsp; | &nbsp;
                <a href="{!! url('/transaction/create/quote') !!}"><span class="fa fa-plus-circle"></span> Create Quotation</a>
            </div>
            <div class="content">
                <div class="clearfix"></div>

                @include('flash::message')

                @if(Input::get('ok') == '1')
                    <div class="alert alert-success">
                        Transactions saved successfully.
                    </div>
                @endif

                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                        @include('transactions.table')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

