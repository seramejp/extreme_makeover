@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-pencil-square-o"></span> Edit Transaction</h1>
            </section>

            @include('adminlte-templates::common.errors')
            {!! Form::model($transactions, ['route' => ['transactions.update', $transactions->id], 'class' => 'form-horizontal', 'method' => 'patch']) !!}


            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>
                <div class="panel-body">
                    @include('transactions.edit_fields')
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Product Information</div>
                <div class="panel-body">
                    @include('transactions.edit_products')
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Labor Information</div>
                <div class="panel-body">
                    @include('transactions.edit_services')
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Payment Information</div>
                <div class="panel-body">
                    @include('transactions.edit_payments')
                </div>
            </div>

            <!-- Submit Field -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-1">
                    <a href="{!! route('transactions.index') !!}" class="btn btn-default">Cancel</a>
                    {!! Form::submit('Save', ['class' => 'btn btn-default']) !!}
                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>


    <script>
        document.addEventListener("DOMContentLoaded", function(event) { 
            function checkProductSelectAndRemovePast(tryThisValue, removeThisValue){
                let theOptions = $("body .transaction-services #serviceProdsSelect");

                if (theOptions.find("option").length > 0) {
                    theOptions.find("option[value="+removeThisValue+"]").remove();
                }

                theOptions.find("option").each(function(index, el) {
                    if ($(el).val() == tryThisValue) {
                        return false;
                    }
                });
                return true;
            }
    
            $.getJSON("{{url("/json/serviceProds.json")}}", function(data){
                let selected = $("body .transaction-services #serviceProdsSelect option:first").val();
                if (selected != null) {
                    let product = data.filter(v => v.id == selected);    
                    $(".transaction-services #serviceTypeInput").val(product[0].servicename);
                    $(".transaction-services #serviceTypeInputHidden").val(product[0].service_id);
                    $(".transaction-products .initial-product input.price-field").val(product[0].price);
                }
            });

            // Every change of product
            $("#serviceProdsSelect").on('change', function(e) {
                e.preventDefault();
                let theElement = $(this);

                $.getJSON("{{url("/json/serviceProds.json")}}", function(data){
                    let selected = theElement.find("option:selected").val();
                    let productname = data.filter(function(e){return e.id == selected});
                    theElement.parent().parent().find("input[id='serviceTypeInput']").val(productname[0].servicename);
                    theElement.parent().parent().find("input[id='serviceTypeInputHidden']").val(productname[0].service_id);
                });
            });

            var theSelectOrig;
            $("body .transaction-products").on('focus', 'select:not(:disabled).selectformcontrol', function(event) {
                theSelectOrig = $(this);
                theSelectOrig.data('previous', theSelectOrig.val());
            }).on('change', 'select:not(:disabled).selectformcontrol', function() {
                let theSelectedProductVal = theSelectOrig.find("option:selected").val();
                let theSelectedProductText = theSelectOrig.find("option:selected").text();
                /*console.log(theSelectOrig.parent().parent().find("input.price-field"));*/

                $.getJSON("{{url("/json/serviceProds.json")}}", function(data){
                    if (theSelectedProductVal != null) {
                        let product = data.filter(v => v.id == theSelectedProductVal);    
                        theSelectOrig.parent().parent().find("input.price-field").val(product[0].price).trigger('keyup');
                    }
                });

                if (checkProductSelectAndRemovePast(theSelectedProductVal, theSelectOrig.data("previous"))){
                    $("body .transaction-services #serviceProdsSelect").append(new Option(theSelectedProductText, theSelectedProductVal));    
                    $("body .transaction-services #serviceProdsSelect").trigger('change');
                }
                theSelectOrig.data('previous', theSelectedProductVal);
            });
        });
    </script>
@endsection
