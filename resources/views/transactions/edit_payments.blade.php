<!-- Payment Information -->
{{--@include('transactions/services')--}}
{{--@include('transactions/products')--}}
<!-- Total Field -->
<div class="form-group col-md-6">
    {!! Form::label('subtotal', 'Subtotal', ['class' => 'col-md-4 control-label']) !!}
    {{--//pass subtotal from service to here--}}
    <div class="col-md-6">
        <input type="number" class="form-control total-amount" required="true" id="subtotal" name="subtotal" value="{{ ($transactions->subtotal) }}">
    </div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('discount', 'Discount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" id="discount" name="discount" class="form-control discount-amount" onkeyup="return recalculate();" required="true" value="{{ ($transactions->discount) }}">
    </div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('total', 'Total Amount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" id="total"  name="total" class="form-control grandtotal-amount" value="{{ ($transactions->total) }}">
    </div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('rendered', 'Initial Amount Paid', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" id="rendered" name="rendered" class="form-control paid-amount" onkeyup="return recalculate();" required="true" value="{{ ($transactions->rendered) }}">
    </div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('balance', 'Balance', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" id="balance" name="balance" class="form-control balance-amount" value="{{ is_null($totalPaid) ? (is_null($transactions->balance) ? "0" : $transactions->balance) : floatval($transactions->total)-floatval($totalPaid->amount) }}">
    </div>
</div>

<script>
    function recalculate()
    {
        /*var productsubtotal = parseFloat($("input[name = 'subtotals[]']").val());
        var servicesubtotal = parseFloat($("input[name = 'service_subtotals[]']").val());
        var subtotal    = productsubtotal + servicesubtotal;*/
        var discount    = parseFloat($("#discount").val());
        var total       = parseFloat($("#subtotal").val()) - parseFloat(discount);
        var rendered    = parseFloat($("#rendered").val());
        var balance     = total - rendered;

        /*$('#subtotal').val(subtotal) ;
        $("#discount").val(discount);*/
        $("#total").val(total);
        /*$("#rendered").val(rendered);*/
        $("#balance").val(balance);
        console.log("+");
        return false;

    }
</script>
