<table class="table table-hover table-striped transactions-table" id="transactions-table">
    <thead>
        <tr>
            <th>Date</th>
            <th>Customer</th>
            <th>Charge Invoice</th>
            <th>Vehicle</th>
            <th>Personnel</th>
            <th>Detail</th>
            <th>Odo Reading</th>
            <th>Subtotal</th>
            <th>Km Run</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transactions)
        <tr>
            <td>{{ Carbon\Carbon::parse($transactions->date)->format('Y/m/d') }}</td>
            <td><a href="<?=url('/')?>/transaction/transaction/{{ $transactions->id }}">{!! $transactions->customer_name !!}</a></td>
            <td>{{ $transactions->charge_invoice or "" }}</td>
            <td>{!! $transactions->vehicle_name !!}</td>
            <td>{!! $transactions->personnel_name !!}</td>
            <td>{!! $transactions->detail_plate !!} ({!! $transactions->detail_color !!})</td>
            <td>{!! $transactions->odo_reading !!}</td>
            <td>{!! $transactions->subtotal !!}</td>
            <td>{!! $transactions->km_run !!}</td>
            <td class="table-cta-data">
                <a href="{!! route('transactions.edit', [$transactions->id]) !!}" class="table-edit-cta" title="Edit Transaction"><i class="glyphicon glyphicon-edit"></i></a> |
                <a href="<?=url('/')?>/transaction/printTransaction/{{ $transactions->id }}" class="table-edit-cta" title="Print Transaction" target="_blank"><i class="fa fa-print"></i></a>
                |
                {!! Form::open(['action' => array('TransactionsController@delete', $transactions->id), 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>