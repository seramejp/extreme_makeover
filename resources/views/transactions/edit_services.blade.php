<?php ?>
<div class="form-group cold-md-12 transaction-services">

    <div class="" style="background-color: #f5f5f5; border: 1px solid #e3e3e3; padding: 20px;">
        <div class="row">
            <div class="col-md-4">
                <select name="" id="serviceProdsSelect" class="form-control selectcontrol">
                    @if(count($transactionServices) > 0)
                        @foreach($transactionServices as $transServ)
                            <option value="{{$transServ->product_id or "0"}}">{{$transServ->product_name or ""}}</option>
                        @endforeach
                    @else
                        @foreach($transactionProducts as $transProd)
                            <option value="{{$transProd->product_id or "0"}}">{{ $prods->where("id", $transProd->product_id)->first()->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>


            <div class="col-md-offset-1 col-md-4">
                <input type="text" name="" id="serviceTypeInput" class="form-control" readonly>
                <input type="text" name="" id="serviceTypeInputHidden" class="form-control hidden" readonly >
            </div>

            
            <div class="col-md-3">
                <button class="btn btn-success service-add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
            </div>
        </div>
    </div>


    <div class="service-copy copy-service-intial hide">
            <div class="service-row container-fluid">

                <!-- Service Id Field --> <!-- -->
                <div class="col-md-2">
                    <label for="" class="control-label">Type of Service</label>
                    <input type="text" class="form-control service-type-id hidden" name="service_ids[]" value="0">
                    <input type="text" class="form-control service-type" name="" readonly style="background-color: white; border: none; box-shadow: none;">
                </div>

                <!-- Product Used Field -->
                <div class="form-group col-md-3">
                    <label for="" class="control-label">Product Used</label>
                    <input type="text" class="form-control service-product-id hidden" name="service_product_ids[]" value="0">
                    <input type="text" class="form-control service-product" readonly style="background-color: white; border: none; box-shadow: none;" value="0">
                </div>

                <!-- Qty Consumed Field -->
                <div class="col-md-1">
                    <label for="" class="control-label">Qty</label>
                    <input type="text" class="form-control" name="service_qtys[]" value="0">
                </div>

                <!-- Unit Field -->
                <div class="col-md-1">
                    <label for="" class="control-label">Unit</label>
                    <input type="text" class="form-control" name="service_units[]" value="liters">
                </div>

                <!-- Subtotal Field -->
                <div class="col-md-2">
                    <label for="" class="control-label">Subtotal</label>
                    <input type="number" class="form-control sub-total-field" onkeyup="javascript:return calculate_sub_total();" name="service_subtotals[]" value="0">
                </div>

                <!-- Defails Field -->
                <div class="col-md-2">
                    <label for="" class="control-label">Details</label>
                    <textarea class="form-control" name="service_details[]"></textarea>
                </div>


                <!-- Unit Field -->
                <div class="transaction-form-cta">
                    <div class="">
                        <button class="btn btn-danger service-remove" type="button"><i class="glyphicon glyphicon-remove" onclick="return calculate_sub_total()"></i></button>
                    </div>
                </div>
            </div>
    </div>



    @foreach($transactionServices as $transServ)
        <div class="service-row container-fluid">

            <!-- Service Id Field --> <!-- -->
            <div class="col-md-2">
                <label for="" class="control-label">Type of Service</label>
                <input type="text" class="form-control service-type-id hidden" name="service_ids[]" value="{{ $transServ->service_id }}">
                <input type="text" class="form-control service-type" name="" readonly style="background-color: white; border: none; box-shadow: none;" value="{{ $transServ->servicename }}">
            </div>

            <!-- Product Used Field -->
            <div class="form-group col-md-3">
                <label for="" class="control-label">Product Used</label>
                <input type="text" class="form-control service-product-id hidden" name="service_product_ids[]" value="{{ $transServ->product_id }}">
                <input type="text" class="form-control service-product" readonly style="background-color: white; border: none; box-shadow: none;" value="{{ $transServ->product_name }}">
            </div>

            <!-- Qty Consumed Field -->
            <div class="col-md-1">
                <label for="" class="control-label">Qty</label>
                <input type="text" class="form-control" name="service_qtys[]" value="{{ $transServ->qty or "1" }}">
            </div>

            <!-- Unit Field -->
            <div class="col-md-1">
                <label for="" class="control-label">Unit</label>
                <input type="text" class="form-control" name="service_units[]" value="{{ $transServ->unit or "liters" }}">
            </div>

            <!-- Subtotal Field -->
            <div class="col-md-2">
                <label for="" class="control-label">Subtotal</label>
                <input type="number" class="form-control sub-total-field" onkeyup="javascript:return calculate_sub_total();" name="service_subtotals[]" value="{{ $transServ->subtotal or "0" }}">
            </div>

            <!-- Defails Field -->
            <div class="col-md-2">
                <label for="" class="control-label">Details</label>
                <textarea class="form-control" name="service_details[]">{{ $transServ->details or "" }}</textarea>
            </div>


            <!-- Unit Field -->
            <div class="transaction-form-cta">
                <div class="">
                    <button class="btn btn-danger service-remove" type="button"><i class="glyphicon glyphicon-remove" onclick="return calculate_sub_total()"></i></button>
                </div>
            </div>
        </div>
    @endforeach
</div>
<script>
    function calculate_sub_total()
    {
        var total_amount_services = 0;
        var total_amount = 0;
        var total_amount = 0;
        var discount_amount = 0;
        var paid_amount = 0;

        $('.transaction-services .service-row .sub-total-field').each(function () {
            total_amount_services = parseFloat(total_amount_services) +  parseFloat($(this).val());
        });

        $('.transaction-products .sub-total-field').each(function () {
            total_amount = parseFloat(total_amount) +  parseFloat($(this).val());
        });

        var final_total = 0;

        final_total = parseFloat(total_amount) + parseFloat(total_amount_services);

        $('#subtotal').val(final_total);
        $("#discount").val(discount_amount);
        $("#total").val(final_total - discount_amount);
        $("#rendered").val(paid_amount);
        $("#balance").val((final_total - discount_amount) - parseFloat(paid_amount));
        
    }
</script>