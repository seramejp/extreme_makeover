
<div class="form-group cold-md-12 transaction-products">
    
    @foreach($transactionProducts as $transProds)
        <div class="product-row container-fluid {{-- if first, add class initial-product--}}">

            <!-- Product Id Field -->
            <div class="col-md-3">
                <label for="" class="control-label">Product</label>
                <select name="product_ids[]" id="" class="form-control selectformcontrol">
                    @foreach($serviceProds as $products)
                        <option value="{{$products->id}}" data-serviceid="{{$products->service_id}}" {{ ($transProds->product_id == $products->id) ? "selected" : "" }}>{{$products->name}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Qty Field -->
            <div class="col-md-1">
                {!! Form::label('qty', 'Qty', ['class' => 'control-label']) !!}
                <input type="number" name="qtys[]" class="form-control qty-field" value="{{$transProds->qty or "0"}}">
            </div>

            <!-- Price Field -->
            <div class="col-md-2">
                {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                <input type="number" name="prices[]" step="0.01" class="form-control price-field" value="{{ $transProds->price or "0.00"}}">
            </div>

            <!-- Subtotal Field -->
            <div class="col-md-2">
                {!! Form::label('subtotal', 'Subtotal', ['class' => 'control-label']) !!}
                <input type="number" name="subtotals[]" step="0.01" class="form-control sub-total-field" value="{{ $transProds->subtotal or "0.00"}}" required="true" onkeyup="return recalculate();">
            </div>

            <!-- Notes Field -->
            <div class="col-md-2">
                {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
                <textarea name="notes[]" id="" class="form-control">{{$transProds->notes or ""}}</textarea>
            </div>

            <!-- add / minus -->
            <div class="col-md-1">
                <div class="transaction-form-cta">
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                </div>
            </div>
        </div>
    @endforeach
    <div class="copy hide">
        <div class="product-row container-fluid">
            <!-- Product Id Field -->
            <div class="col-md-3">
                <label for="" class="control-label">Product</label>
                <select name="product_ids[]" id="" class="form-control selectformcontrol" disabled="disabled">
                    @foreach($serviceProds as $products)
                        <option value="{{$products->id}}" data-serviceid="{{$products->service_id}}">{{$products->name}}</option>
                    @endforeach
                </select>
            </div>

            <!-- Qty Field -->
            <div class="col-md-1">
                {!! Form::label('qty', 'Qty', ['class' => 'control-label']) !!}
                {!! Form::number('qtys[]', 1, ['class' => 'form-control qty-field', 'disabled']) !!}
            </div>

            <!-- Price Field -->
            <div class="col-md-2">
                {!! Form::label('price', 'Price', ['class' => 'control-label']) !!}
                {!! Form::number('prices[]', 0, ['class' => 'form-control price-field', 'disabled']) !!}
            </div>

            <!-- Subtotal Field -->
            <div class="col-md-2">
                {!! Form::label('subtotal', 'Subtotal', ['class' => 'control-label']) !!}
                {!! Form::number('subtotals[]', 0, ['class' => 'form-control sub-total-field', 'disabled',  'onkeyup' => 'return calculate_sub_total()', 'required' => true]) !!}
            </div>

             <!-- Notes Field -->
            <div class="col-md-2">
                {!! Form::label('notes', 'Notes', ['class' => 'control-label']) !!}
                {!! Form::textarea('notes[]', null, ['class' => 'form-control', 'disabled']) !!}
            </div>

            <!-- add / minus -->
            <div class="col-md-2">
                <div class="transaction-form-cta">
                    <div class="col-md-6">
                        <button class="btn btn-danger remove" type="button" onclick="return calculate_sub_total()"><i class="glyphicon glyphicon-remove"></i></button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-success clone-add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                    </div>  
                </div>
            </div>

           

        </div>
    </div>
</div>

<script>
    function calculate_sub_total()
    {
        var total_amount_services = 0;
        var total_amount = 0;
        var total_amount = 0;
        var discount_amount = 0;
        var paid_amount = 0;

        $('.transaction-services .service-row .sub-total-field').each(function () {
            total_amount_services = parseFloat(total_amount_services) +  parseFloat($(this).val());
        });

        $('.transaction-products .sub-total-field').each(function () {
            total_amount = parseFloat(total_amount) +  parseFloat($(this).val());
        });

        var final_total = 0;

        final_total = parseFloat(total_amount) + parseFloat(total_amount_services);

        $('#subtotal').val(final_total);
        $("#discount").val(discount_amount);
        $("#total").val(final_total - discount_amount);
        $("#rendered").val(paid_amount);
        $("#balance").val((final_total - discount_amount) - parseFloat(paid_amount));
        
    }
</script>


