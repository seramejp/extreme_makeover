<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transactions->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $transactions->date !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $transactions->customer_id !!}</p>
</div>

<!-- Vehicle Id Field -->
<div class="form-group">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    <p>{!! $transactions->vehicle_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $transactions->product_id !!}</p>
</div>

<!-- Detail Id Field -->
<div class="form-group">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    <p>{!! $transactions->detail_id !!}</p>
</div>

<!-- Odo Reading Field -->
<div class="form-group">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    <p>{!! $transactions->odo_reading !!}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{!! $transactions->subtotal !!}</p>
</div>

<!-- Km Run Field -->
<div class="form-group">
    {!! Form::label('km_run', 'Km Run:') !!}
    <p>{!! $transactions->km_run !!}</p>
</div>

<!-- Is Done Field -->
<div class="form-group">
    {!! Form::label('is_done', 'Is Done:') !!}
    <p>{!! $transactions->is_done !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $transactions->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $transactions->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $transactions->deleted_at !!}</p>
</div>

