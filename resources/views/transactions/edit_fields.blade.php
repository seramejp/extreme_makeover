<!-- Date Field -->
<div class="form-group col-md-6" id="transaction_date">
    {!! Form::label('date', 'Date', ['class' => 'col-md-4 control-label']) !!}
    {{--<div class="col-md-6">
        <input class="form-control" name="date" type="text" id="date" required>
    </div>--}}
    <div class="col-md-6">
        <input type="text" class="form-control" required="true" value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $transactions->date)->format('Y-m-d') }}" id="date" name="date">
    </div>
</div>

<div class="form-group col-md-12">
</div>

<!-- Odo Reading Field -->
<div class="form-group col-md-6">
    {!! Form::label('odo_reading', 'Odo Reading', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" class="form-control" required="true" value="{{ $transactions->odo_reading or "0"}}" id="odo_reading" name="odo_reading">
    </div>
</div>

<!-- Km Run Field -->
<div class="form-group col-md-6">
    {!! Form::label('km_run', 'Km Run', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="number" class="form-control" required="true" value="{{ $transactions->km_run or "0" }}" id="km_run" name="km_run">
    </div>
</div>

<!-- Customer Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('customer_id', 'Customer', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="customer_id" id="" class="form-control" required="true">
            <option value="{{ $transactions->customer_id or $transactions->id}}">{{ $transactions->customer_name  or "Something Wrong"}}</option>
        </select>
    </div>
</div>

<!-- Vehicle Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('detail_id', 'Vehicle', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="detail_id" id="" class="form-control" required="true">
            <option value="{{$transactions->detail_id}}">{{ $transactions->details or "Something Wrong" }}</option>
        </select>
    </div>
</div>

<!-- Odo Reading Field -->
<div class="form-group col-md-6">
    {!! Form::label('personnel_id', 'Service Personnel', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select name="personnel_id" id="" class="form-control" required="true">
            @if($transactions->personnel_id == 0)
                <option value="0" selected>---</option>
            @else
                <option value="0">---</option>
            @endif

            @foreach($personnels as $tao)
                    <option value="{{ $tao->id}}" {{ ($transactions->personnel_id == $tao->id ? "selected" : "") }}>{{$tao->name}}</option>
            @endforeach
        </select>
    </div>
</div>


<!-- Vehicle Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('charge_invoice', 'Charge Invoice', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="text" value="{{$transactions->charge_invoice}}" class="form-control" id="charge_invoice" name="charge_invoice">
    </div>
</div>



<script>
    
</script>