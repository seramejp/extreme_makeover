<?php ?>
<div class="form-group cold-md-12 transaction-services">

    <div class="" style="background-color: #f5f5f5; border: 1px solid #e3e3e3; padding: 20px;">
        <div class="row">
            <div class="col-md-4">
                <select name="" id="serviceProdsSelect" class="form-control selectcontrol">
                    
                </select>
            </div>


            <div class="col-md-offset-1 col-md-4">
                <input type="text" name="" id="serviceTypeInput" class="form-control" readonly>
                <input type="text" name="" id="serviceTypeInputHidden" class="form-control hidden" readonly>
            </div>

            
            <div class="col-md-3">
                <button class="btn btn-success service-add-more" type="button"><i class="glyphicon glyphicon-plus"></i></button>
            </div>
        </div>
    </div>


    <div class="service-copy copy-service-intial hide">
        <div class="service-row container-fluid">

            <!-- Service Id Field --> <!-- -->
            <div class="col-md-2">
                <label for="" class="control-label">Type of Service</label>
                <input type="text" class="form-control service-type-id hidden" name="service_ids[]" value="0">
                <input type="text" class="form-control service-type" name="" readonly style="background-color: white; border: none; box-shadow: none;">
            </div>

            <!-- Product Used Field -->
            <div class="form-group col-md-3">
                <label for="" class="control-label">Product Used</label>
                <input type="text" class="form-control service-product-id hidden" name="service_product_ids[]" value="0">
                <input type="text" class="form-control service-product" readonly style="background-color: white; border: none; box-shadow: none;">
            </div>

            <!-- Qty Consumed Field -->
            <div class="col-md-1">
                <label for="" class="control-label">Qty</label>
                <input type="text" class="form-control" name="service_qtys[]" value="0">
            </div>

            <!-- Unit Field -->
            <div class="col-md-1">
                <label for="" class="control-label">Unit</label>
                <input type="text" class="form-control" name="service_units[]" value="liters">
            </div>

            <!-- Subtotal Field -->
            <div class="col-md-2">
                <label for="" class="control-label">Subtotal</label>
                <input type="number" class="form-control sub-total-field" onkeyup="javascript:return calculate_sub_total();" name="service_subtotals[]" value="0">
            </div>

            <!-- Defails Field -->
            <div class="col-md-2">
                <label for="" class="control-label">Details</label>
                <textarea class="form-control" name="service_details[]"></textarea>
            </div>


            <!-- Unit Field -->
            <div class="transaction-form-cta">
                <div class="">
                    <button class="btn btn-danger service-remove" type="button"><i class="glyphicon glyphicon-remove" onclick="return calculate_sub_total()"></i></button>
                </div>
            </div>
        </div>
    
    </div>
</div>

<script>
    function calculate_sub_total()
    {
        var total_amount_services = 0;
        var total_amount = 0;
        var total_amount = 0;
        var discount_amount = 0;
        var paid_amount = 0;

        $('.transaction-services .service-row .sub-total-field').each(function () {
            total_amount_services = parseFloat(total_amount_services) +  parseFloat($(this).val());
        });

        $('.transaction-products .sub-total-field').each(function () {
            total_amount = parseFloat(total_amount) +  parseFloat($(this).val());
        });

        var final_total = 0;

        final_total = parseFloat(total_amount) + parseFloat(total_amount_services);

        $('#subtotal').val(final_total);
        $("#discount").val(discount_amount);
        $("#total").val(final_total - discount_amount);
        $("#rendered").val(paid_amount);
        $("#balance").val((final_total - discount_amount) - parseFloat(paid_amount));
        
    }
</script>