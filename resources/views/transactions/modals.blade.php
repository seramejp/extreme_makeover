<!-- Modal -->

<!-- ------------- -->
<!-- ADD CUSTOMER -->
<!-- ------------- -->

<div class="modal fade" id="mAddCustomer" tabindex="-1" role="dialog" aria-labelledby="mAddCustomer_label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mAddCustomer_label"><span class="glyphicon glyphicon-user"></span> Add Customer</h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label for="">{{ ($type == "- Walk-in" ? "": "Company ")  }} Name:</label>
                            <input type="text" class="form-control" id="ma-name">
                        </div>
                        <div class="form-group">
                            <label for="">Contact #:</label>
                            <input type="text" class="form-control" id="ma-contact">
                        </div>
                        <div class="form-group">
                            <label for="">Address:</label>
                            <input type="text" class="form-control" id="ma-address">
                        </div>
                        <div class="form-group">
                            <label for="">Date of Birth:</label>
                            <input type="date" class="form-control" id="ma-dob">
                        </div>
                        <div class="form-group">
                            <label for="">Vehicle Make / Model:</label>
                            <input type="text" class="form-control" id="ma-make">
                        </div>
                        <div class="form-group">
                            <label for="">Plate Number:</label>
                            <input type="text" class="form-control" id="ma-platenum">
                        </div>
                        <div class="form-group">
                            <label for="">Vehicle Color:</label>
                            <input type="text" class="form-control" id="ma-color">
                            <input type="text" class="form-control hidden" id="ma-iscompany" value="{{ ($type == "- Walk-in" ? 0: 1)  }}">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="ma-btn-save" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            </div>
        </div>
    </div>
</div>


<!-- ------------- -->
<!-- ADD PERSONNEL -->
<!-- ------------- -->
<div class="modal fade" id="mAddPersonnel" tabindex="-1" role="dialog" aria-labelledby="mAddPersonnel_label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mAddPersonnel_label"><span class="glyphicon glyphicon-user"></span> Add New Personnel</h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <meta name="csrf-token2" content="{{ csrf_token() }}" />
                    <div class="col-md-6 col-md-offset-1">
                        <div class="form-group">
                            <label for="">Personnel Name:</label>
                            <input type="text" class="form-control" id="mp-name" autofocus>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="mp-btn-savep" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            </div>
        </div>
    </div>
</div>



<!-- ------------- -->
<!-- ADD PRODUCT -->
<!-- ------------- -->

<div class="modal fade" id="mAddProduct" tabindex="-1" role="dialog" aria-labelledby="mAddProduct_label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mAddProduct_label"><span class="glyphicon glyphicon-user"></span> Add New Product</h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                        <meta name="csrf-token" content="{{ csrf_token() }}" />
                        
                        <div class="form-group">
                            <label for="">Name:</label>
                            <input type="text" class="form-control" id="map-name">
                        </div>

                        <div class="form-group">
                            <label for="">Duration:</label>
                            <input type="text" class="form-control" id="map-duration">
                        </div>

                        <div class="form-group">
                            <label for="">Brand:</label>
                            <select id="map-brand" class="form-control">
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Price:</label>
                            <input type="text" class="form-control" id="map-price">
                        </div>

                        <div class="form-group">
                            <label for="">Quantity:</label>
                            <input type="text" class="form-control" id="map-quantity">
                        </div>

                        <div class="form-group">
                            <label for="">Service:</label>
                            <select id="map-service" class="form-control">
                                @foreach($services as $service)
                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="map-save" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
            </div>
        </div>
    </div>
</div>