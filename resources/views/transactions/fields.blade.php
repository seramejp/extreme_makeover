<!-- Date Field -->
<div class="form-group col-md-6" id="transaction_date">
    {!! Form::label('date', 'Date', ['class' => 'col-md-4 control-label']) !!}
    {{--<div class="col-md-6">
        <input class="form-control" name="date" type="text" id="date" required>
    </div>--}}
    <div class="col-md-6">{!! Form::text('date', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<div class="form-group col-md-12">
</div>

<!-- Odo Reading Field -->
<div class="form-group col-md-6">
    {!! Form::label('odo_reading', 'Odo Reading', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('odo_reading', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Km Run Field -->
<div class="form-group col-md-6">
    {!! Form::label('km_run', 'Km Run (months)', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::number('km_run', null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Customer Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('customer_id', 'Customer', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('customer_id', $customers, null, ['class' => 'form-control', 'onchange' => 'return getVehiclesByCustomer();', 'required' => true]) !!}</div>
    <button class="btn btn-primary" id="btnAddCustomer" data-toggle="modal" data-target="#mAddCustomer" onclick="return false;"><span class="glyphicon glyphicon-plus"></span></button>
</div>

<!-- Vehicle Id Field -->
<div class="form-group col-md-6">
    {!! Form::label('detail_id', 'Vehicle', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('detail_id', $vehicles, null, ['class' => 'form-control', 'required' => true]) !!}</div>
</div>

<!-- Odo Reading Field -->
<div class="form-group col-md-6">
    {!! Form::label('personnel_id', 'Service Personnel', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::select('personnel_id', $personnels, null, ['class' => 'form-control', 'required' => true]) !!}</div>
    <button class="btn btn-primary" id="btnAddPersonnel" data-toggle="modal" data-target="#mAddPersonnel" onclick="return false;"><span class="glyphicon glyphicon-plus"></span></button>
</div>

<!-- Is Quote Field -->
<div class="form-group col-md-6 hidden">
    {!! Form::label('is_quote', 'Temporary Service Record', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::hidden('is_quote', false) !!}
        {!! Form::checkbox('is_quote', '1', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-md-6">
    {!! Form::label('is_quote', 'Temporary', ['class' => 'col-md-4 control-label hidden ']) !!}
    <div class="col-md-6">
        {!! Form::hidden('create_quotation', false) !!}
    </div>
</div>
    
@if($type == "- Corporate")
    <div class="form-group col-md-6" id="div_chargeinvoice">
        {!! Form::label('', 'Charge Invoice', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <input type="text" class="form-control" required="true" id="charge_invoice" name="charge_invoice">
        </div>
    </div>
@endif

@include ("transactions.modals")

<script>
    function getVehiclesByCustomer()
    {
        $("#detail_id").html("<option>Please wait...</option>");
        $.ajax({
            url:    "{{ url('/transaction/getVehiclesByCustomer/') }}" + "/" + $("#customer_id").val(),
            success: function(data)
            {
                $("#detail_id").html(data);
            }
        });
    }

</script>

