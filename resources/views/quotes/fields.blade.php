<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Vehicle Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    {!! Form::number('vehicle_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Detail Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    {!! Form::number('detail_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Odo Reading Field -->
<div class="form-group col-sm-6">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    {!! Form::number('odo_reading', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtotal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    {!! Form::number('subtotal', null, ['class' => 'form-control']) !!}
</div>

<!-- Km Run Field -->
<div class="form-group col-sm-6">
    {!! Form::label('km_run', 'Km Run:') !!}
    {!! Form::number('km_run', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Quote Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_quote', 'Is Quote:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_quote', false) !!}
        {!! Form::checkbox('is_quote', '1', null) !!} 1
    </label>
</div>

<!-- Qty Differential Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty_differential', 'Qty Differential:') !!}
    {!! Form::text('qty_differential', null, ['class' => 'form-control']) !!}
</div>

<!-- Qty Transmission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty_transmission', 'Qty Transmission:') !!}
    {!! Form::text('qty_transmission', null, ['class' => 'form-control']) !!}
</div>

<!-- Qty Engine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('qty_engine', 'Qty Engine:') !!}
    {!! Form::text('qty_engine', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Done Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_done', 'Is Done:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_done', false) !!}
        {!! Form::checkbox('is_done', '1', null) !!} 1
    </label>
</div>

<!-- Is Asf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_asf', 'Is Asf:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_asf', false) !!}
        {!! Form::checkbox('is_asf', '1', null) !!} 1
    </label>
</div>

<!-- Personnel Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('personnel_id', 'Personnel Id:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('personnel_id', false) !!}
        {!! Form::checkbox('personnel_id', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('quotes.index') !!}" class="btn btn-default">Cancel</a>
</div>
