<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $quotes->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $quotes->date !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $quotes->customer_id !!}</p>
</div>

<!-- Vehicle Id Field -->
<div class="form-group">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    <p>{!! $quotes->vehicle_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $quotes->product_id !!}</p>
</div>

<!-- Detail Id Field -->
<div class="form-group">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    <p>{!! $quotes->detail_id !!}</p>
</div>

<!-- Odo Reading Field -->
<div class="form-group">
    {!! Form::label('odo_reading', 'Odo Reading:') !!}
    <p>{!! $quotes->odo_reading !!}</p>
</div>

<!-- Subtotal Field -->
<div class="form-group">
    {!! Form::label('subtotal', 'Subtotal:') !!}
    <p>{!! $quotes->subtotal !!}</p>
</div>

<!-- Km Run Field -->
<div class="form-group">
    {!! Form::label('km_run', 'Km Run:') !!}
    <p>{!! $quotes->km_run !!}</p>
</div>

<!-- Is Quote Field -->
<div class="form-group">
    {!! Form::label('is_quote', 'Is Quote:') !!}
    <p>{!! $quotes->is_quote !!}</p>
</div>

<!-- Qty Differential Field -->
<div class="form-group">
    {!! Form::label('qty_differential', 'Qty Differential:') !!}
    <p>{!! $quotes->qty_differential !!}</p>
</div>

<!-- Qty Transmission Field -->
<div class="form-group">
    {!! Form::label('qty_transmission', 'Qty Transmission:') !!}
    <p>{!! $quotes->qty_transmission !!}</p>
</div>

<!-- Qty Engine Field -->
<div class="form-group">
    {!! Form::label('qty_engine', 'Qty Engine:') !!}
    <p>{!! $quotes->qty_engine !!}</p>
</div>

<!-- Is Done Field -->
<div class="form-group">
    {!! Form::label('is_done', 'Is Done:') !!}
    <p>{!! $quotes->is_done !!}</p>
</div>

<!-- Is Asf Field -->
<div class="form-group">
    {!! Form::label('is_asf', 'Is Asf:') !!}
    <p>{!! $quotes->is_asf !!}</p>
</div>

<!-- Personnel Id Field -->
<div class="form-group">
    {!! Form::label('personnel_id', 'Personnel Id:') !!}
    <p>{!! $quotes->personnel_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $quotes->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $quotes->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $quotes->deleted_at !!}</p>
</div>

