<table class="table table-hover table-striped transactions-table" id="transactions-table">
    <thead>
    <tr>
        <th>Date</th>
        <th>Customer</th>
        <th>Vehicle</th>
        <th>Personnel</th>
        <th>Detail</th>
        <th>Odo Reading</th>
        <th>Subtotal</th>
        <th>Km Run</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transactions)
        <tr id="quote-{!! $transactions->id !!}">
            <td>{{ Carbon\Carbon::parse($transactions->date)->format('Y/m/d') }}</td>
            <td><a href="<?=url('/')?>/transaction/transaction/{{ $transactions->id }}">{!! $transactions->customer_name !!}</a></td>
            <td>{!! $transactions->vehicle_name !!}</td>
            <td>{!! $transactions->personnel_name !!}</td>
            <td>{!! $transactions->detail_plate !!} ({!! $transactions->detail_color !!})</td>
            <td>{!! $transactions->odo_reading !!}</td>
            <td>{!! $transactions->subtotal !!}</td>
            <td>{!! $transactions->km_run !!}</td>
            <td class="table-cta-data">
                <a href="#" onclick="return approve({!! $transactions->id !!});"><span class="fa fa-thumbs-up"></span></a> &nbsp; |&nbsp;
                <a href="#" onclick="return disapprove({!! $transactions->id !!});"><span class="fa fa-thumbs-down"></span></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<script>
    function approve(id)
    {
        var con = confirm("Please confirm transaction approval.");
        if (con)
        {
            var url = "<?=url('/')?>/quotes/approve/" + id + "/";
            $.ajax({url: url, success: function(){
                $("#quote-" + id).fadeOut(300);
                $("#quote-" + id).hide();
                window.location.replace("<?=url('/')?>/transactions/"+id+"/edit");

            }});
            return false;
        }
    }

    function disapprove(id)
    {
        var con = confirm("Please confirm transaction disapproval.");
        if (con)
        {
            var url = "<?=url('/')?>/quotes/disapprove/" + id + "/";
            $.ajax({url: url, success: function(){
                $("#quote-" + id).fadeOut(300);
                $("#quote-" + id).hide();

            }});
        }
    }
</script>