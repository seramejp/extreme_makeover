<div class="data-full-info">
    <h1 class="page-header">{!! $corporates->name !!}</h1><!-- Id Field -->
    <div class="panel panel-default">
        <div class="panel-heading">Customer information</div>
        <div class="panel-body">
            <!-- Id Field -->
            <div class="form-group col-md-6">
                {!! Form::label('id', 'Id:') !!}
                <p>{!! $corporates->id !!}</p>
            </div>

            <!-- Contact Field -->
            <div class="form-group col-md-6">
                {!! Form::label('contact', 'Contact:') !!}
                <p>{!! $corporates->contact !!}</p>
            </div>

            <!-- Company Field -->
            <div class="form-group col-md-6">
                {!! Form::label('company', 'Company:') !!}
                <p>{!! $corporates->company !!}</p>
            </div>

            <!-- Is Company Field -->
            <div class="form-group col-md-6">
                {!! Form::label('is_company', 'Is Company:') !!}
                <p>{!! $corporates->is_company !!}</p>
            </div>

              @if (!empty($corporates->address))
              <!-- Address  Field -->
              <div class="form-group col-md-6">
                  {!! Form::label('address', 'Address:') !!}
                  <p>{!! $corporates->address !!}</p>
              </div>
              @endif
            
            <!-- Created At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('created_at', 'Created At:') !!}
                <p>{!! $corporates->created_at !!}</p>
            </div>

            <!-- Updated At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('updated_at', 'Updated At:') !!}
                <p>{!! $corporates->updated_at !!}</p>
            </div>

            <!-- Deleted At Field -->
            <div class="form-group col-md-6">
                {!! Form::label('deleted_at', 'Deleted At:') !!}
                <p>{!! $corporates->deleted_at !!}</p>
            </div>
      </div>
    </div>
</div>

<div class="data-full-info">
    <div class="panel panel-default">
        <div class="panel-heading">Vehicles Owned</div>
        <div class="panel-body">
          <ul>
          @foreach($vehicles as $vehicle)
              <li>{{ $vehicle->make }}</li>
          @endforeach
          </ul>
      </div>
    </div>
</div>

<div class="data-full-info">
    <div class="panel panel-default">
        <div class="panel-heading">Transactions</div>
        <div class="panel-body">
          <table class="table table-hover table-striped" id="reports-table">
              <thead>
                  <tr>
                      <th>Date</th>
                      <th>Vehicle</th>
                      <th>Product</th>
                      <th>Detail</th>
                      <th>Odo Reading</th>
                      <th>Subtotal</th>
                      <th>Km Run</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  @foreach($transactions as $transaction)
                  <tr>
                      <td>{{ Carbon\Carbon::parse($transaction->date)->format('Y/m/d') }}</td>
                      <td><a href="<?=url('/')?>/transaction/transaction/{{ $transaction->id }}">{!! $transaction->vehicle_name !!}</a></td>
                      <td>{!! $transaction->product_name !!}</td>
                      <td>{!! $transaction->detail_plate !!} ({!! $transaction->detail_color !!})</td>
                      <td>{!! $transaction->odo_reading !!}</td>
                      <td>{!! $transaction->subtotal !!}</td>
                      <td>{!! $transaction->km_run !!}</td>
                  </tr>
                  @endforeach
              </tbody>
          </table>
      </div>
    </div>
</div>
