<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <th>Company</th>
        <th>Contact</th>
        <th>Address</th>
        <th>Vehicle</th>
        <th>Plate Num</th>
        <th>Action</th>
    </thead>
    <tbody>
    @foreach($corporates as $corporates)
        <tr>
            <td><a href="{!! route('customers.show', [$corporates->id]) !!}">{!! $corporates->company !!}</a></td>
            <td>{!! $corporates->contact !!}</td>
            <td>{!! $corporates->address !!}</td>
            <td>{{ $corporates->make . " , " . $corporates->color }}</td>
            <td>{{ $corporates->plate }}</td>
            <td class="table-cta-data">
              <a href="{!! route('corporates.edit', [$corporates->id]) !!}" class="table-edit-cta"><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::open(['route' => ['corporates.destroy', $corporates->id], 'method' => 'delete']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>