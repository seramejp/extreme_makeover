<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('name', null, ['class' => 'form-control']) !!}</div>
</div>


<!-- Contact Field -->
<div class="form-group">
    {!! Form::label('contact', 'Contact:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('contact', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('company', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Is Company Field -->
{!! Form::hidden('is_company', 1, ['class' => 'form-control']) !!}

<!-- Address Field -->
<div class="form-group address-field">
    {!! Form::label('address', 'Address:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">{!! Form::text('address', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <a href="{!! route('corporates.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::submit('Save Customer Info', ['class' => 'btn btn-default']) !!}
    </div>
</div>