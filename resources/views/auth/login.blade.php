@extends('layouts.default')
@section('content')
    <div id="wrapper" class="homepage">
        <div class="col-xs-4 login-wrapper">
        	    <div class="form-wrap">
                     <img src="img/logo.jpg">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="login-form" >
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group">
                                <button type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block">
                                    Login
                                </button>
                        </div>
                    </form>
        	    </div>
        </div>
    </div>
    <!-- /#wrapper -->
@endsection
