@extends('layouts.admin')
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="fa fa-list"></span> Services
                    </h1>
                </div>
            </div>

            <div class="content">
                <div class="clearfix"></div>

                @include('flash::message')

                <div class="clearfix"></div>

                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row" style="padding-left: 20px">
                            @include('services.show_fields')
                            <a href="{!! route('services.index') !!}" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
