<!-- Modal -->

<!-- ------------- -->
<!-- TRANSACTION HISTORY -->
<!-- ------------- -->

<div class="modal fade" id="mServiceRecord" tabindex="-1" role="dialog" aria-labelledby="mServiceRecord_label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mServiceRecord_label"><span class="glyphicon glyphicon-user"></span> Service Record</h4>
            </div>
            
            <div class="modal-body">
                <meta name="csrf-token2" content="{{ csrf_token() }}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default" id="customer_transhistory">
                            <div class="panel-body">   
                              <table class="table table-hover" id="customer-transaction-table">
                                  <thead>
                                      <tr>
                                          <th>Date</th>
                                          <th>Vehicle</th>
                                          <th>Plate #</th>
                                          <th>Odo Reading</th>
                                          <th></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      @foreach($transactions as $transaction)
                                      <tr onclick="loadTransactionHistory({{$transaction->id}});">
                                          <td>{{ Carbon\Carbon::parse($transaction->date)->format('Y/m/d') }}</td>
                                          <td><a href="{{ url('/')}}/transaction/transaction/{{ $transaction->id }}">{!! $transaction->vehicle_name !!}</a></td>
                                          <td>{!! $transaction->detail_plate !!} ({!! $transaction->detail_color !!})</td>
                                          <td>{!! $transaction->odo_reading !!}</td>
                                          <td><a href="{{ url('/')}}/transaction/printTransaction/{{ $transaction->id }}" class="table-edit-cta" title="Print Transaction" target="_blank"><i class="fa fa-print"></i></a></td>
                                      </tr>
                                      @endforeach
                                  </tbody>
                              </table>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default" id="customer_transhistory_productinformation">
                            <div class="panel-heading">Product Information</div>
                            <div class="panel-body">   
                                <table class="table table-hover" id="customer-productinformation-table">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="panel panel-default" id="customer_transhistory_laborinformation">
                            <div class="panel-heading">Labor Information</div>
                            <div class="panel-body">
                                <table class="table table-hover" id="customer-laborinformation-table">
                                    <thead>
                                        <tr>
                                            <th>Service</th>
                                            <th>Qty</th>
                                            <th>Unit</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="panel panel-default" id="customer_transhistory">
                            <div class="panel-heading">Payment Information
                                <span class="pull-right" id="spanTotalPaid">Total: Php 0.00</span>
                            </div>
                            <div class="panel-body">
                                <table class="table table-hover" id="customer-paymentinformation-table">
                                    <thead>
                                        <tr>
                                            <th>Payment Date</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>