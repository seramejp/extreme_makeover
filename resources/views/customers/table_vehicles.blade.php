<table class="table table-hover" id="vehicles-table">
    <thead>
        <tr>
            @if($customers->is_company == '1')
                <th>Company</th>
            @endif
                <th>Vehicle Make</th>
                <th>Plate Number</th>
                <th>{{ $customers->is_company == '1' ? "Company" : "Customer" }} Contact #</th>
                <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($vehicles as $customerVehicles)
        <tr style="cursor: pointer;">

            @if($customers->is_company == '1')
                <td data-toggle="modal" data-target="#mServiceRecord">{{ $customers->company }}</td>
            @endif
            <td data-toggle="modal" data-target="#mServiceRecord">{!! $customerVehicles->make !!}</td>
            <td data-toggle="modal" data-target="#mServiceRecord">{!! $customerVehicles->plate !!}</td>
            <td data-toggle="modal" data-target="#mServiceRecord">{{ $customers->contact }}</td>
            <td>
                {!! Form::open(['route' => ['customerVehicles.destroy', $customerVehicles->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('customerVehicles.edit', [$customerVehicles->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<a class="btn btn-primary" href="{!! route('customerVehicles.create') !!}/{!! $customers->id !!}">Add New</a>

@include('customers.modals')