@extends('layouts.admin')

@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Customer</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in customer information</div>
                    <div class="panel-body">
                            {!! Form::open(['route' => 'customers.store', 'class' => 'form-horizontal customer-form']) !!}

                            @include('customers.fields')

                            {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
