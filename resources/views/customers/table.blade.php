<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Contact</th>
            <th>Company</th>
            <th>Type</th>
            <th>Birthday</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $customers)
        <tr>
            <td><a href="{!! route('customers.show', [$customers->id]) !!}">{!! $customers->name !!}</a></td>
            <td>{!! $customers->contact !!}</td>
            <td>{!! $customers->company !!}</td>
            <td>{{ $customers->is_company == "1" ? "Corporate" : "Walk-in" }}</td>
            <td>
              @if ($customers->birthday != '0000-00-00')
                  {{ date('m/d/Y', strtotime($customers->birthday)) }}
              @endif
            </td>
            <td class="table-cta-data">
                <a href="{!! route('customers.edit', [$customers->id]) !!}" class="table-edit-cta"><span class="glyphicon glyphicon-edit"></span></a>
                {!! Form::open(['route' => ['customers.destroy', $customers->id], 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                    {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>