<div class="data-full-info">
    <h1 class="page-header">{!! $customers->name !!}</h1><!-- Id Field -->


    <a href="{!! route('customers.index') !!}" class="btn btn-default">Back</a>
    <a href="{!! url('/customers/' . $customers->id . '/edit') !!}" class="btn btn-default">Edit Customer</a>

    <a href="" class="btn btn-danger pull-right" data-toggle='modal' data-target='#modalConfirmDeleteCustomer'>Delete</a>

    <br><br>
    <div class="panel panel-default">
        <div class="panel-heading">Customer information</div>
        <div class="panel-body">

            <!-- Company Field -->
            <div class="form-group col-md-6">
                {!! Form::label('name', 'Name:') !!}
                <p>{!! $customers->name !!}</p>
            </div>

          <!-- Contact Field -->
          <div class="form-group col-md-6">
              {!! Form::label('contact', 'Contact:') !!}
              <p>{!! $customers->contact !!}</p>
          </div>

          <!-- Company Field -->
          <div class="form-group col-md-6">
              {!! Form::label('company', 'Company:') !!}
              <p>{!! $customers->company !!}</p>
          </div>

          <!-- Is Company Field -->
          <div class="form-group col-md-6">
              {!! Form::label('is_company', 'Customer Type:') !!}
              <p>{!! $customers->is_company ? 'Corporate' : 'Walk-in' !!}</p>
          </div>

          @if ($customers->birthday != '0000-00-00')
          <!-- Birthday Field -->
          <div class="form-group col-md-6">
              {!! Form::label('birthday', 'Birthday:') !!}
              <p>{{ date('m/d/Y', strtotime($customers->birthday)) }}</p>
          </div>
          @endif

          @if (!empty($customers->address))
          <!-- Address  Field -->
          <div class="form-group col-md-6">
              {!! Form::label('address', 'Address:') !!}
              <p>{!! $customers->address !!}</p>
          </div>
          @endif

      </div>
    </div>
</div>

<div class="data-full-info">
    <div class="panel panel-default">
        <div class="panel-heading">Vehicles Owned</div>
        <div class="panel-body">
          @include('customers.table_vehicles')
          <table class="table table-hover hidden">
              <thead>
                  <tr>
                      <th>Make</th>
                      <th>Plate Number</th>
                      <th>Color</th>
                      <th>Contact</th>
                  </tr>
              </thead>
              <tbody>
          @foreach($vehicles as $vehicle)
              <tr>
                  <td>{{ $vehicle->make }}</td>
                  <td>{{ $vehicle->plate }}</td>
                  <td>{{ $vehicle->color }}</td>
                  <td>{{ $customers->contact }}</td>
              </tr>
          @endforeach
              </tbody>
          </table>
      </div>
    </div>
</div>


<!-- Modal -->
    <div class="modal fade" id="modalConfirmDeleteCustomer" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true" style="position: fixed; top: 20%;">
        
        <div class="modal-dialog">

            <!-- Modal content-->
            
                <div class="modal-content">
                    <div class="modal-header" style="background-color: black;">
                        <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                        <h4 class="modal-title text-white"><span class="text-danger"><i class="ti-alert"></i></span> Confirm <span class="text-danger"><strong>delete</strong></span> customer.</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                            
                                
                                    <div class="form-group">
                                        <label for="" class="">Are you sure you want to delete this customer?</label>
                                    </div>

                                    
                                
                            </div>
                           
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a id="btnDeleteCustomer" href="{{ url('/customers/delete/' . $customers->id) }}" role="button" class="btn btn-danger btn-outline">Yes, I confirm</a>
                        <button type="button" class="btn btn-default btn-outline" data-dismiss="modal" id="cancelDeleteTransaction">Cancel</button>
                    </div>

                </div>
           
        </div>
    </div>