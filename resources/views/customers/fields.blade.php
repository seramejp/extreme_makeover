<!-- Customer Type -->
<div class="form-group">
    {!! Form::label('is_company', 'Customer Type:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">{!! Form::select('is_company', [0 => 'Walk-In', 1 => 'Corporate'], null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}</div>
</div>

<!-- Contact Field -->
<div class="form-group">
    {!! Form::label('contact', 'Contact:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('contact', null, ['class' => 'form-control', 'required' => 'required']) !!}</div>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">{!! Form::text('company', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Birthday Field -->
<div class="form-group" id="transaction_date">
    {!! Form::label('birthday', 'Date of birth:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        @if (!empty($customers->birthday))
            <input class="form-control" name="birthday" type="text" id="date" value="@if ($customers->birthday != '0000-00-00'){{ date('Y/m/d', strtotime($customers->birthday)) }}@endif">
        @else
            <input class="form-control" name="birthday" type="text" id="date">
        @endif
    </div>
</div>

<!-- Address Field -->
<div class="form-group address-field">
    {!! Form::label('address', 'Address:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">{!! Form::text('address', null, ['class' => 'form-control']) !!}</div>
</div>



<!-- Submit Field -->
<div class="form-group">
    <div class="col-md-8 col-md-offset-4">
        <a href="{!! route('customers.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::submit('Save Customer Info', ['class' => 'btn btn-default']) !!}
    </div>
</div>