@extends('layouts.admin')
@section('content')
    <div id="page-wrapper" class="payments-page">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Payment</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in payment information</div>
                <div class="panel-body">
                    @include('adminlte-templates::common.errors')
                    {!! Form::open(['route' => 'payments.store']) !!}

                    @include('payments.create_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

