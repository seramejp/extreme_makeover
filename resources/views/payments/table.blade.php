<table class="table table-responsive" id="payments-table">
    <thead>
        <th>Transaction Date</th>
        <th>Customer</th>
        <th>Amount</th>
        <th>Payment Date</th>
    </thead>
    <tbody>
    @foreach($payments as $payment)
        <tr>
            <td><a href="{{ url('/transaction/transaction/' . $payment->transaction_id)  }}">{{ Carbon\Carbon::parse($payment->transaction_date)->format('Y/m/d') }}</a></td>
            <td><a href="{{ url('/customers/' . $payment->customer_id) }}">{!! $payment->customer_name !!}</a></td>
            <td>P {{ number_format($payment->amount, 2) }}</td>
            <td>{{ Carbon\Carbon::parse($payment->payment_date)->format('Y/m/d') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>