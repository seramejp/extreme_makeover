<!-- Transaction Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('transaction_id', 'Plate Number:') !!}
    <select name="transaction_id" id="transaction_id" class="form-control select-plate">
        @foreach($transactions as $trans)
            <option value="{{$trans->id}}" data-customerid="{{$trans->customer_id}}" data-customername="{{$trans->customer_name}}">{{ strtoupper($trans->vehicle_detail) }}</option>
        @endforeach
    </select>
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer:') !!}
    <select name="customer_id" id="customer_id" class="form-control select-customer">
    </select>
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    <input type="number" id="amount" name="amount" class="form-control">
</div>

<!-- Payment Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_date', 'Payment Date:') !!}
    {!! Form::date('payment_date', Carbon\Carbon::today()->toDateString(), ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('payments.index') !!}" class="btn btn-default">Cancel</a>
</div>
