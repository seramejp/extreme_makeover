@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="glyphicon glyphicon-calendar"></span> Account Payables
                    </h1>
                    <!-- <a href="{{-- route('accountsReceivable.create') --}}" class="cta-link"><span class="fa fa-plus-circle"></span> Add Account</a> -->
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">

                    <table class="table table-hover table-striped" id="ap-table">
                        <thead>
                            <tr>
                                <th>Supplier</th>
                                <th>PO Date</th>
                                <th>Total Purchased</th>
                                <th>Total Paid</th>
                                <th>Balance</th>
                                <th></th>
                            </tr>  
                        </thead>
                        <tbody>
                            @foreach($accountsPayables as $ap)
                                <tr>
                                    <td>{{ $ap->supplier . ", " . $ap->address }}</td>
                                    <td>{{ $ap->order_date }}</td>
                                    <td>{{ number_format($ap->grand_total, 2) }}</td>
                                    <td>{{ number_format($ap->paidamount, 2) }}</td>
                                    <td>{{ number_format(intval($ap->grand_total) - intval($ap->paidamount), 2) }}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </tbody>

                        <tfoot>
                            <tr>
                                <th  colspan="4" style="font-size: 16px;">TOTAL BALANCE</th>
                                <th  colspan="1" style="text-align:right; font-size: 16px;"></th>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@stop