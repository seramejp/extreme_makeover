<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 2/8/2017
 * Time: 4:55 PM
 */
?>
@extends('layouts.admin')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
      <div class="data-full-info">
          <h1 class="page-header">{{ $transaction->name }}</h1><!-- Id Field -->

            <a href="{{ $back }}" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
            <a href="<?=url('/')?>/transactions/{{ $transaction->transaction_id }}/edit" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> Edit Transaction</a>
            <a href="<?=url('/')?>/payment/createFromTransaction/{{ $transaction->transaction_id }}" class="btn btn-default"><span class="glyphicon glyphicon-credit-card"></span> Add Payment</a>
            
            <a href="" class="btn btn-danger pull-right" data-toggle='modal' data-target='#modalConfirmDeleteTransaction' data-transid="{{ $transaction->transaction_id }}">Delete</a>

          <br> <br>
          <div class="panel panel-default">
              <div class="panel-heading">Transaction information</div>
              <div class="panel-body">
                <div class="form-group col-md-6">
                    {!! Form::label('contact', 'Contact Number:') !!}
                    <p>{{ $transaction->contact }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('make', 'Vehicle Unit:') !!}
                    <p>{{ $transaction->make }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('color', 'Color:') !!}
                    <p>{{ $transaction->color }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('plate', 'Plate Number:') !!}
                    <p>{{ $transaction->plate }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('date', 'Last Change Oil:') !!}
                    <p>{{ Carbon\Carbon::parse($transaction->date)->format('Y/m/d') }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('km_run', 'Km Run:') !!}
                    <p>{{ $transaction->km_run }} months</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('odo_reading', 'ODO Reading:') !!}
                    <p>{{ $transaction->odo_reading }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('personnel_name', 'Personnel:') !!}
                    <p>{{ $transaction->personnel_name }}</p>
                </div>

                  <div class="form-group col-md-6">
                      {!! Form::label('charge_invoice', 'Charge Invoice:') !!}
                      <p>{{ $transaction->charge_invoice }}</p>
                  </div>

            </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Product information</div>
              <div class="panel-body">
                  <table class="table table-hover table-responsive">
                      <thead>
                      <tr>
                          <th>Product</th>
                          <th>Qty</th>
                          <th>Price</th>
                          <th>Subtotal</th>
                          <th>Notes</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($products as $product)
                          <tr>
                              <td>{{ $product->name }}</td>
                              <td>{{ $product->transaction_qty }}</td>
                              <td>P {{ number_format($product->product_price, 2) }}</td>
                              <td>P {{ number_format($product->subtotal, 2) }}</td>
                              <td>{{ $product->notes or "" }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Labor Information</div>
              <div class="panel-body">
                  <table class="table table-hover table-responsive">
                      <thead>
                      <tr>
                          <th>Service</th>
                          <th>Details</th>
                          <th>Product Used</th>
                          <th>Qty</th>
                          <th>Subtotal</th>
                          <th>Details</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($services as $service)
                          <tr>
                              <td>{{ $service->service }}</td>
                              <td>{{ $service->details }}</td>
                              <td>{{ $service->product }}</td>
                              <td>{{ $service->qty }} {{ $service->unit }}</td>
                              <td>{{ $service->subtotal }}</td>
                              <td>{{ $service->details or "" }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Payment Information</div>
              <div class="panel-body row">
                  <div class="col-md-2">
                      Subtotal: <b>P {{ number_format($transaction->subtotal, 2) }}</b>
                  </div>
                  <div class="col-md-2">
                      Discount: <b>P {{ number_format($transaction->discount, 2) }}</b>
                  </div>
                  <div class="col-md-2">
                      Total: <b>P {{ number_format($transaction->total, 2) }}</b>
                  </div>
                  <div class="col-md-2">
                      Rendered: <b>P {{ number_format($transaction->rendered, 2) }}</b>
                  </div>
                  <div class="col-md-2">
                      Total Paid: <b>P {{ (is_null($totalPaid) ? "0.00" : number_format($totalPaid->amount, 2)) }}</b>
                  </div>
                  <div class="col-md-2">
                      Balance: <b>P {{ number_format( is_null($totalPaid) ? (is_null($transaction->balance) ? "0" : $transaction->balance) : floatval($transaction->total)-floatval($totalPaid->amount),2 ) }}</b>
                  </div>
              </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Payment History</div>
              <div class="panel-body row">
                  @include('payments.table')
              </div>
          </div>


      </div>
      
    </div>



    <!-- Modal -->
    <div class="modal fade" id="modalConfirmDeleteTransaction" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true" style="position: fixed; top: 20%;">
        
        <div class="modal-dialog">

            <!-- Modal content-->
            
                <div class="modal-content">
                    <div class="modal-header" style="background-color: black;">
                        <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                        <h4 class="modal-title text-white"><span class="text-danger"><i class="ti-alert"></i></span> Confirm <span class="text-danger"><strong>delete</strong></span> transaction.</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-10 col-lg-offset-1">
                            
                                
                                    <div class="form-group">
                                        <label for="" class="">Are you sure you want to delete this transaction?</label>
                                    </div>

                                    
                                
                            </div>
                           
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <a id="btnDeleteTransaction" href="{{ url('/transactions/delete/' . $transaction->transaction_id) }}" role="button" class="btn btn-danger btn-outline">Yes, I confirm</a>
                        <button type="button" class="btn btn-default btn-outline" data-dismiss="modal" id="cancelDeleteTransaction">Cancel</button>
                    </div>

                </div>
           
        </div>
    </div>

</div>


@endsection