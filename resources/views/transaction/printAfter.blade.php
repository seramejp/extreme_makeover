<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 2/8/2017
 * Time: 4:55 PM
 */
?>
@extends('layouts.admin')
@section('content')

<div id="page-wrapper">

    <div class="extreme-printinfo">
        <p><img src="<?=url('/')?>/img/logo.jpg"/></p>
        <h4>EXTREME MAKEOVER</h4>
        <p class="no-margin">Auto Supply Sales and Service Center</p>
        <p class="no-margin">Tel. No. : (082) 221-6210/300-1408</p>
        <p class="no-margin">Email: extreme_totalquartzcenter@yahoo.com.ph</p>
    </div>

    <div class="container-fluid">
      <div class="data-full-info">
          <h1 class="page-header">{{ $transaction->name }}</h1><!-- Id Field -->
          <div class="panel panel-default">
              <div class="panel-heading">Transaction information</div>
              <div class="panel-body">
                <div class="form-group col-md-6">
                    {!! Form::label('contact', 'Contact Number:') !!}
                    <p>{{ $transaction->contact }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('make', 'Vehicle Unit:') !!}
                    <p>{{ $transaction->make }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('color', 'Color:') !!}
                    <p>{{ $transaction->color }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('plate', 'Plate Number:') !!}
                    <p>{{ $transaction->plate }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('date', 'Transaction Date:') !!}
                    <p>{{ Carbon\Carbon::parse($transaction->date)->format('Y/m/d') }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('km_run', 'Km Run:') !!}
                    <p>{{ $transaction->km_run }} months</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('modelyear', 'Model, Year:') !!}
                    <p>{{ $transaction->modelyear }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('personnel_name', 'Personnel:') !!}
                    <p>{{ $transaction->personnel_name }}</p>
                </div>
                <div class="form-group col-md-6">
                    {!! Form::label('charge_invoice', 'Charge Invoice:') !!}
                    <p>{{ $transaction->charge_invoice }}</p>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Product information</div>
              <div class="panel-body">
                  <table class="table table-hover table-responsive">
                      <thead>
                      <tr>
                          <th>Product</th>
                          <th>Qty</th>
                          <th>Price</th>
                          <th>Subtotal</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($products as $product)
                          <tr>
                              <td>{{ $product->name }}</td>
                              <td>{{ $product->transaction_qty }}</td>
                              <td>P {{ number_format($product->product_price, 2) }}</td>
                              <td>P {{ number_format($product->subtotal, 2) }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Services Availed</div>
              <div class="panel-body">
                  <table class="table table-hover table-responsive">
                      <thead>
                      <tr>
                          <th>Service</th>
                          <th>Details</th>
                          <th>Product Used</th>
                          <th>Qty</th>
                          <th>Subtotal</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($services as $service)
                          <tr>
                              <td>{{ $service->service }}</td>
                              <td>{{ $service->details }}</td>
                              <td>{{ $service->product }}</td>
                              <td>{{ $service->qty }} {{ $service->unit }}</td>
                              <td>P {{ number_format($service->subtotal, 2) }}</td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>

          <div class="panel panel-default">
              <div class="panel-heading">Payment Information</div>
              <div class="panel-body">
                  Sub-Total: <b>P {{ number_format($transaction->subtotal, 2) }}</b>
                  <br>
                  Discount: <b>P {{ number_format($transaction->discount, 2) }}</b>
                  <br>
                  Total Amount to be Paid: <b>P {{ number_format($transaction->total, 2) }}</b>
                  <br>
                  Initial Payment: <b>P {{ number_format($transaction->rendered, 2) }}</b>
                  <br>
                  Balance: <b>P {{ number_format($transaction->balance, 2) }}</b>
                  
              </div>
          </div>

      </div>
    </div>

</div>

<script>
    window.onload = function()
    {
        window.print();
        setTimeout(window.location.href = "{{ url('/') }}/transactions?ok=1", 0);
    };
</script>

@endsection