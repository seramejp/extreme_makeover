<?php
/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 2/8/2017
 * Time: 4:55 PM
 */
?>
@extends('layouts.admin')
@section('content')

    <div id="page-wrapper">
    <div class="container-fluid">

        <div>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">
                        <span class="fa fa-upload"></span> Import
                    </h2>
                </div>
            </div>

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="alert alert-info">
                Notes: <br /> <br />
                For proper importing, download this <a href="<?=url('/')?>/import_sample.csv" target="_blank">sample import template file</a>. <br />
                Also include the CSV headers to prevent the first line from skipping.
            </div>

            <div class="div-import-transaction container-fluid">
                <form id="form-import-transaction" method="post" action="<?=url('/')?>/transaction/import" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="file" name="import" id="import-file" required />
                        </div>
                    </div>
                    <div class="row">
                        <br />
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-default"><span class="fa fa-upload"></span> Import</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection