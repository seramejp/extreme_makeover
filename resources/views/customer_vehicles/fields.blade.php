<!-- Customer Id Field -->

<div class="form-group col-sm-6">
    {!! Form::label('make', 'Make:') !!}
    {!! Form::text('make', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('plate', 'Plate Number:') !!}
    {!! Form::text('plate', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('color', 'Color:') !!}
    {!! Form::text('color', null, ['class' => 'form-control']) !!}
</div>

@if(isset($customer_id))
    <div class="form-group col-sm-6 hidden">
        {!! Form::label('customer_id', 'Customer Id:') !!}
        {!! Form::number('customer_id', $customer_id, ['class' => 'form-control']) !!}
    </div>
@else
    <div class="form-group col-sm-6 hidden">
        {!! Form::label('customer_id', 'Customer Id:') !!}
        {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Vehicle Id Field -->
<div class="form-group col-sm-6 hidden">
    {!! Form::label('vehicle_id', 'Vehicle Id:') !!}
    {!! Form::number('vehicle_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Detail Id Field -->
<div class="form-group col-sm-6 hidden">
    {!! Form::label('detail_id', 'Detail Id:') !!}
    {!! Form::number('detail_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::hidden('customer_url', URL::previous()) !!}
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
</div>
