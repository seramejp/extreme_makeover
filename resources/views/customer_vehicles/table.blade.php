<table class="table table-responsive" id="customerVehicles-table">
    <thead>
        <th>Customer Id</th>
        <th>Vehicle Id</th>
        <th>Detail Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($customerVehicles as $customerVehicles)
        <tr>
            <td>{!! $customerVehicles->customer_id !!}</td>
            <td>{!! $customerVehicles->vehicle_id !!}</td>
            <td>{!! $customerVehicles->detail_id !!}</td>
            <td>
                {!! Form::open(['route' => ['customerVehicles.destroy', $customerVehicles->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('customerVehicles.show', [$customerVehicles->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('customerVehicles.edit', [$customerVehicles->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>