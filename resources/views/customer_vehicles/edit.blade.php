@extends('layouts.admin')
@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-pencil-square-o"></span> Edit Customer Vehicle</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::model($customerVehicles, ['route' => ['customerVehicles.update', $customerVehicles->id], 'method' => 'patch']) !!}

                    @include('customer_vehicles.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection