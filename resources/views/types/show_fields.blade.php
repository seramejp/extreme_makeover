<div class="data-full-info">
    <h1 class="page-header">{!! $types->name !!}</h1><!-- Id Field -->
    <div class="panel panel-default">
        <div class="panel-heading">Type information</div>
        <div class="panel-body">
          <!-- Id Field -->
          <div class="form-group col-md-6">
              {!! Form::label('id', 'Id:') !!}
              <p>{!! $types->id !!}</p>
          </div>

          <!-- Created At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('created_at', 'Created At:') !!}
              <p>{!! $types->created_at !!}</p>
          </div>

          <!-- Updated At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('updated_at', 'Updated At:') !!}
              <p>{!! $types->updated_at !!}</p>
          </div>

          <!-- Deleted At Field -->
          <div class="form-group col-md-6">
              {!! Form::label('deleted_at', 'Deleted At:') !!}
              <p>{!! $types->deleted_at !!}</p>
          </div>
      </div>
    </div>
</div>
