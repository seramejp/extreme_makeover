@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-pencil-square-o"></span> Edit Type</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in product information</div>
                <div class="panel-body">
                    @include('adminlte-templates::common.errors')
                    {!! Form::model($types, ['route' => ['types.update', $types->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}

                    @include('types.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection