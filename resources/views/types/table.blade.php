<table class="table table-hover table-striped" id="types-table">
    <thead>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($types as $types)
        <tr>
            <td><a href="{!! route('types.show', [$types->id]) !!}">{!! $types->name !!}</a></td>
            <td class="table-cta-data">
                <a href="{!! route('types.edit', [$types->id]) !!}" class='table-edit-cta'><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::open(['route' => ['types.destroy', $types->id], 'method' => 'delete']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>