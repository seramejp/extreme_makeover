@extends('layouts.default')
@section('content')
    <div id="wrapper" class="homepage">
        <div class="col-xs-4 login-wrapper">
        	    <div class="form-wrap">
                     <img src="<?=url('/')?>/img/logo.jpg">
                    <form role="form" action="report" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="email" class="sr-only">username</label>
                            <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Password</label>
                            <input type="password" name="key" id="key" class="form-control" placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <span class="character-checkbox" onclick="showPassword()"></span>
                            <span class="label">Show password</span>
                        </div>
                        <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Log in">
                    </form> 
                    <hr>
        	    </div>
        </div>
    </div>
    <!-- /#wrapper -->
@stop