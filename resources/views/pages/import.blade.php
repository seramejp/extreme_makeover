@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
    <div class="container-fluid">

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span class="fa fa-upload"></span> Import
                </h1>
            </div>
        </div>

        <form id="form-import-transactions" method="post" action="<?=url('/')?>/transactions/import" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-lg-12">
                    <input type="file" name="import" id="import-file" required />
                </div>
            </div>
            <div class="row">
                <br />
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default"><span>Import</span></button>
                </div>
            </div>
        </form>

    </div>

</div>
@stop