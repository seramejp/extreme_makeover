<table class="table table-hover personnels-table">
    <thead>
        <th>Name</th>
        <th></th>
    </thead>
    <tbody>
    @foreach($personnels as $personnel)
        <tr>
            <td><a href="{!! route('personnels.show', [$personnel->id]) !!}" title="View Personnel">{!! $personnel->name !!}</a></td>
            <td class="table-cta-data">
                <a href="{!! route('personnels.edit', [$personnel->id]) !!}" class="table-edit-cta" title="Edit product" ><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::open(['route' => ['personnels.destroy', $personnel->id], 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>