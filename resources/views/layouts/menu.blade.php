<li class="{{ Request::is('brands*') ? 'active' : '' }}">
    <a href="{!! route('brands.index') !!}"><i class="fa fa-edit"></i><span>Brands</span></a>
</li>

<li class="{{ Request::is('types*') ? 'active' : '' }}">
    <a href="{!! route('types.index') !!}"><i class="fa fa-edit"></i><span>Types</span></a>
</li>

<li class="{{ Request::is('walkins*') ? 'active' : '' }}">
    <a href="{!! route('walkins.index') !!}"><i class="fa fa-edit"></i><span>Walkins</span></a>
</li>

<li class="{{ Request::is('corporates*') ? 'active' : '' }}">
    <a href="{!! route('corporates.index') !!}"><i class="fa fa-edit"></i><span>Corporates</span></a>
</li>


<li class="{{ Request::is('transactions*') ? 'active' : '' }}">
    <a href="{!! route('transactions.index') !!}"><i class="fa fa-edit"></i><span>Transactions</span></a>
</li>

<li class="{{ Request::is('afterSales*') ? 'active' : '' }}">
    <a href="{!! route('afterSales.index') !!}"><i class="fa fa-edit"></i><span>AfterSales</span></a>
</li>

<li class="{{ Request::is('afterSales*') ? 'active' : '' }}">
    <a href="{!! route('afterSales.index') !!}"><i class="fa fa-edit"></i><span>AfterSales</span></a>
</li>

<li class="{{ Request::is('priceHistories*') ? 'active' : '' }}">
    <a href="{!! route('priceHistories.index') !!}"><i class="fa fa-edit"></i><span>PriceHistories</span></a>
</li>

<li class="{{ Request::is('qtyHistories*') ? 'active' : '' }}">
    <a href="{!! route('qtyHistories.index') !!}"><i class="fa fa-edit"></i><span>QtyHistories</span></a>
</li>

<li class="{{ Request::is('asfTransactions*') ? 'active' : '' }}">
    <a href="{!! route('asfTransactions.index') !!}"><i class="fa fa-edit"></i><span>AsfTransactions</span></a>
</li>

<li class="{{ Request::is('personnels*') ? 'active' : '' }}">
    <a href="{!! route('personnels.index') !!}"><i class="fa fa-edit"></i><span>Personnels</span></a>
</li>
<li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
    <a href="{!! route('suppliers.index') !!}"><i class="fa fa-edit"></i><span>Suppliers</span></a>
</li>


<li class="{{ Request::is('customerVehicles*') ? 'active' : '' }}">
    <a href="{!! route('customerVehicles.index') !!}"><i class="fa fa-edit"></i><span>CustomerVehicles</span></a>
</li>

<li class="{{ Request::is('quotes*') ? 'active' : '' }}">
    <a href="{!! route('quotes.index') !!}"><i class="fa fa-edit"></i><span>Quotes</span></a>
</li>

<li class="{{ Request::is('payments*') ? 'active' : '' }}">
    <a href="{!! route('payments.index') !!}"><i class="fa fa-edit"></i><span>Payments</span></a>
</li>

<li class="{{ Request::is('roles*') ? 'active' : '' }}">
    <a href="{!! route('roles.index') !!}"><i class="fa fa-edit"></i><span>Roles</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

