<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        * {
            font-family: Tahoma, Helvetica, sans-serif;
        }
    </style>
    @include('includes.head')
</head>
    <body>
     <div id="wrapper">
        @include('includes.navigation')
        @yield('content')
     </div>
        @include('includes.footer')      
    </body>
</html>