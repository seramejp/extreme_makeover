<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <th>Name</th>
        <th>Address</th>
        <th></th>
    </thead>
    <tbody>
    @foreach($suppliers as $supplier)
        <tr>
            <td>{!! $supplier->name !!}</td>
            <td>{!! $supplier->address !!}</td>
            <td class="table-cta-data">
                <a href="{!! route('suppliers.edit', [$supplier->id]) !!}" class="table-edit-cta" title="Edit supplier"><span class="glyphicon glyphicon-edit"></span></a>
                {!! Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'delete', 'style' => 'display: inline-block;']) !!}
                {!! Form::button('<span class="glyphicon glyphicon-trash "></span>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>            
        </tr>
    @endforeach
    </tbody>
</table>