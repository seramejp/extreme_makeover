@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-pencil-square-o"></span> Edit Supplier</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in supplier information</div>
                    <div class="panel-body">
                        @include('adminlte-templates::common.errors')
                        {!! Form::model($supplier, ['route' => ['suppliers.update', $supplier->id], 'method' => 'patch', 'class' => 'form-horizontal']) !!}

                        @include('suppliers.fields')

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection