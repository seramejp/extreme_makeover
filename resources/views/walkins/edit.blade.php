@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-pencil-square-o"></span> Edit Customer Information</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in customer information</div>
                <div class="panel-body">
                    @include('adminlte-templates::common.errors')
                    {!! Form::model($walkins, ['route' => ['walkins.update', $walkins->id], 'method' => 'patch', 'class' => 'form-horizontal customer-form']) !!}

                    @include('walkins.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection