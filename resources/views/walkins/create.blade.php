@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1 class="page-header"><span class="fa fa-plus-circle"></span> Add Walk-in Customer</h1>
            </section>
            <div class="panel panel-default">
                <div class="panel-heading">Fill in customer information</div>
                <div class="panel-body">
                    @include('adminlte-templates::common.errors')
                    {!! Form::open(['route' => 'walkins.store', 'class' => 'form-horizontal']) !!}

                    @include('walkins.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection
