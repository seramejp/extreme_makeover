<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('name', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Contact Field -->
<div class="form-group">
    {!! Form::label('contact', 'Contact:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('contact', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">{!! Form::text('company', null, ['class' => 'form-control']) !!}</div>
</div>

<!-- Is Company Field -->
{!! Form::hidden('is_company', 0, ['class' => 'form-control']) !!}

<!-- Birthday Field -->
<div class="form-group" id="transaction_date">
    {!! Form::label('birthday', 'Date of birth:', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-8">
        @if (!empty($walkins->birthday))
            <input class="form-control" name="birthday" type="text" id="date" value="@if ($walkins->birthday != '0000-00-00'){{ date('Y/m/d', strtotime($walkins->birthday)) }}@endif" required>
        @else
            <input class="form-control" name="birthday" type="text" id="date" required>
        @endif
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <a href="{!! route('walkins.index') !!}" class="btn btn-default">Cancel</a>
        {!! Form::submit('Save Customer Info', ['class' => 'btn btn-default']) !!}
    </div>
</div>