@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <span class="glyphicon glyphicon-user"></span> Walk-in Customers
                    </h1>
                </div>
            </div>
            <div class="content">
                <div class="clearfix"></div>

                @include('flash::message')

                <div class="clearfix"></div>
                <div class="box box-primary">
                    <div class="box-body">
                        @include('walkins.table')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

