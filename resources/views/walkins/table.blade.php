<table class="table table-hover table-striped" id="reports-table">
    <thead>
        <tr>
            <th>Vehicle</th>
            <th>Plate Num</th>
            <th>Name</th>
            <th>Contact</th>
            <th>Birthday</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($walkins as $walkins)
        <tr>
            <td>{{ $walkins->make . " , " . $walkins->color }}</td>
            <td>{{ $walkins->plate }}</td>
            <td><a href="{!! route('customers.show', [$walkins->id]) !!}">{!! $walkins->name !!}</a></td>
            <td>{!! $walkins->contact !!}</td>
            <td>
              @if ($walkins->birthday != '0000-00-00')
                  {{ date('m/d/Y', strtotime($walkins->birthday)) }}
              @endif
            </td>
            <td class="table-cta-data">
                <a href="{!! route('walkins.edit', [$walkins->id]) !!}" class="table-edit-cta"><i class="glyphicon glyphicon-edit"></i></a>
                {!! Form::open(['route' => ['walkins.destroy', $walkins->id], 'method' => 'delete']) !!}
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger table-delete-cta', 'onclick' => "return confirm('Are you sure?')"]) !!}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>