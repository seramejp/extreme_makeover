<table class="table table-responsive" id="reports-table">
    <thead>
        <tr>
            <th>Product</th>
            <th>Current QTY</th>
            <th>Added/Deducted QTY</th>
            <th>Note</th>
            <th>Date Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach($qtyHistories as $qtyHistory)
        <tr>
            <td>{!! $qtyHistory->name !!}</td>
            <td>{!! $qtyHistory->qty !!}</td>
            <td>
                @if($qtyHistory->note == 'Product added')
                    + {!! $qtyHistory->adjusted !!}
                @else
                    - {!! $qtyHistory->adjusted !!}
                @endif
            </td>
            <td>{!! $qtyHistory->note !!}</td>
            <td>{{ Carbon\Carbon::parse($qtyHistory->created_at)->format('Y/m/d') }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>