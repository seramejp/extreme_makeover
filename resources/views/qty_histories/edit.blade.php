@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Qty History
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($qtyHistory, ['route' => ['qtyHistories.update', $qtyHistory->id], 'method' => 'patch']) !!}

                        @include('qty_histories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection