@extends('layouts.admin')
@section('content') 
<div id="notifications">
  <div id="page-wrapper">
      <div class="container-fluid">
          <!-- Page Heading -->
          <div class="row">
              <div class="col-lg-12">
                  <h1 class="page-header">
                    <span class="glyphicon glyphicon-exclamation-sign"></span> Notifications
                  </h1>
              </div>
          </div>

          <div class="alert alert-info">
              You have {{ $count }} oil change requests. <a href="<?=url('/')?>/reports/notifications">View List in Reports View</a>
          </div>

          <!-- /.row -->
              <ul class="nav nav-tabs">
                  <li><a data-toggle="tab" href="#walk-in">Walk-in Customers</a></li>
                  <li class="active"><a data-toggle="tab" href="#corporate">Corporate</a></li>
                  <li><a data-toggle="tab" href="#walkindone">Done - Walk-in Customers</a></li>
                  <li><a data-toggle="tab" href="#corporatedone">Done - Corporate</a></li>
              </ul>
              <div class="tab-content">
                  <div id="walk-in" class="tab-pane fade">
                      <br />
                      <ul class="list-group">
                          @foreach ($walkIns as $index => $item)
                              <li class="list-group-item {{ $item->is_done == '1' ? 'done' : '' }}">
                                  {{ Carbon\Carbon::parse($item->date)->format('Y/m/d') }} - {{ $item->name }}
                                  <div class="pull-right">
                                      <a href="<?=url('/')?>/transaction/transaction/{{ $item->transaction_id }}/walkin" class="btn btn-success"><span class="glyphicon glyphicon-zoom-in"></span> View</a>
                                      @if ($item->is_done != '1')
                                          <a href="<?=url('/')?>/transaction/transaction/done/{{ $item->transaction_id }}/walkin" class="btn btn-warning"><span class="glyphicon glyphicon-ok"></span> Done</a>
                                      @endif
                                  </div>
                              </li>
                          @endforeach
                      </ul>
                  </div>
                  <div id="corporate" class="tab-pane fade in active">
                      <br />
                      <ul class="list-group">
                          @foreach ($corporates as $index => $item)
                              <li class="list-group-item {{ $item->is_done == '1' ? 'done' : '' }}">
                                  {{ Carbon\Carbon::parse($item->date)->format('Y/m/d') }} - {{ $item->name }}
                                  <div class="pull-right">
                                      <a href="<?=url('/')?>/transaction/transaction/{{ $item->transaction_id }}/corporate" class="btn btn-success"><span class="glyphicon glyphicon-zoom-in"></span> View</a>
                                      @if ($item->is_done != '1')
                                          <a href="<?=url('/')?>/transaction/transaction/done/{{ $item->transaction_id }}/corporate" class="btn btn-warning"><span class="glyphicon glyphicon-ok"></span> Done</a>
                                      @endif
                                  </div>
                              </li>
                          @endforeach
                      </ul>
                  </div>
                  <div id="walkindone" class="tab-pane fade">
                      <br />
                      <ul class="list-group">
                          @foreach ($walkInsD as $index => $item)
                              <li id="asf{{ $item->transaction_id }}" class="list-group-item {{ $item->is_done == '1' ? 'done' : '' }}">
                                  {{ Carbon\Carbon::parse($item->date)->format('Y/m/d') }} - {{ $item->name }}
                                  <div class="pull-right">
                                      <a href="<?=url('/')?>/transaction/transaction/{{ $item->transaction_id }}/walkin" class="btn btn-success"><span class="glyphicon glyphicon-zoom-in"></span> View</a>
                                      @if ($item->is_done == '1')
                                          <a href="#" class="btn btn-warning" onclick="return markAsfDone({{ $item->transaction_id }});"><span class="glyphicon glyphicon-ok"></span> ASF</a>
                                      @endif
                                  </div>
                              </li>
                          @endforeach
                      </ul>
                  </div>
                  <div id="corporatedone" class="tab-pane fade">
                      <br />
                      <ul class="list-group">
                          @foreach ($corporatesD as $index => $item)
                              <li id="asf{{ $item->transaction_id }}" class="list-group-item {{ $item->is_done == '1' ? 'done' : '' }}">
                                  {{ Carbon\Carbon::parse($item->date)->format('Y/m/d') }} - {{ $item->name }}
                                  <div class="pull-right">
                                      <a href="<?=url('/')?>/transaction/transaction/{{ $item->transaction_id }}/corporate" class="btn btn-success"><span class="glyphicon glyphicon-zoom-in"></span> View</a>
                                      @if ($item->is_done == '1')
                                          <a href="#" class="btn btn-warning" onclick="return markAsfDone({{ $item->transaction_id }});"><span class="glyphicon glyphicon-ok"></span> ASF</a>
                                      @endif
                                  </div>
                              </li>
                          @endforeach
                      </ul>
                  </div>

              </div>
          <!-- /.row -->
      </div>  
  </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

    <script>
        function markAsfDone(transactionId) {
            var url = "<?=url('/')?>/transaction/asf/" + transactionId + "/";
            $.ajax({url: url, success: function(){
                $("#asf" + transactionId).fadeOut(300);
                //$("#asf" + transactionId).hide();
            }});
            return false;
        }
    </script>

@stop