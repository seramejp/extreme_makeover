@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <!-- Page Heading -->
       <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                  <span class="glyphicon glyphicon-user"></span> Birthday Nothification
              </h1>
          </div>
       </div>
       <div class="row notifications birthdays">
          <div class="col-md-12">
              <table class="table table-hover notification-table">
                  <thead>
                      <th>Date</th>
                      <th>Info</th>
                      <th>Contact</th>
                  </thead>
                  <tbody>
                      @foreach($birthday_users as $user)
                          <tr>
                              <td>{{ date('M d, Y', strtotime($user->date)) }}</td>
                              <td>Today is <a href="{{ url('/') }}/walkins/{{ $user->id }}">{{ $user->name }}'s</a> birthday</td>
                              <td>{{ $user->contact }}</td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
          </div>
       </div>
    </div>
</div>
@stop
