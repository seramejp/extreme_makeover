@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <!-- Page Heading -->
       <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                  <span class="glyphicon glyphicon-user"></span> Walk-In Notifications
              </h1>
          </div>
       </div>
       <div class="row notifications">

          @if(Session::has('notiification-mark-asf-message'))
              <div class="alert alert-success">
                    {{ Session::get('notiification-mark-asf-message') }}
              </div>
            @endif    
       
          <div class="col-md-12">
              <ul class="nav nav-tabs nav-justified">
                <li><a href="{{ url('/') }}/notifications/walk-in/pending">Pending</a></li>
                <li class="active"><a href="{{ url('/') }}/notifications/walk-in/done">Done</a></li>
                <li><a href="{{ url('/') }}/notifications/walk-in/service">Service</a></li>
              </ul>
              <div class="tab-content">
                  <table class="table table-hover notification-table">
                      <thead>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Unit</th>
                        <th>Plate No</th>
                        <th></th>
                      </thead>
                      <tbody>
                          @foreach($walkin_customers as $customer)
                              <tr>
                                  <td class="notification-date">{{ date('m/d/Y', strtotime($customer->date)) }}</td>
                                  <td class="customer-name"><a href="{{ url('/') }}/transaction/transaction/{{ $customer->reference_id }}">{{ $customer->name }}</a></td>
                                  <td>{{ $customer->contact }}</td>
                                  <td>{{ $customer->make }}</td>
                                  <td>{{ $customer->plate }}</td>
                                  <td class="cta-column"><a href="{{ url('/') }}/transaction/update/{{ $customer->reference_id }}/asf/walk-in" class="btn mark-cta"><span class="glyphicon glyphicon-ok"></span> Move to ASF</a></td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
       </div>
    </div>
</div>
@stop
