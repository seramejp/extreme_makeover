@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <!-- Page Heading -->
       <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                  <span class="fa fa-refresh"></span> Refresh All Notifications
              </h1>
          </div>
       </div>
       <div class="row">

       @if(Session::has('notification-update-message'))
          <div class="alert alert-success">
                {{ Session::get('notification-update-message') }}
          </div>
       @endif

       <div class="col-md-12">
            {{ Form::open(array('url' => 'refresh-notification-record')) }}
              <p>If you need to force update all notification record, click Refresh <i>All Notification Records button</i></p>
              {{ Form::submit('Refresh All Notification Records') }}
            {{ Form::close() }}
          </div>
       </div>
    </div>
</div>
@stop
