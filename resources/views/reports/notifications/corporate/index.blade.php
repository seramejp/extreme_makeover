@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <!-- Page Heading -->
       <div class="row">
          <div class="col-md-12">
              <h1 class="page-header">
                  <span class="glyphicon glyphicon-user"></span> Corporate Notifications
              </h1>
          </div>
       </div>
       <div class="row notifications">
       
          @if(Session::has('notiification-mark-done-message'))
              <div class="alert alert-success">
                    {{ Session::get('notiification-mark-done-message') }}
              </div>
            @endif      
      
          <div class="col-md-12">
              <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="{{ url('/') }}/notifications/corporate/pending">Pending</a></li>
                <li><a href="{{ url('/') }}/notifications/corporate/done">Done</a></li>
                <li><a href="{{ url('/') }}/notifications/corporate/service">Service</a></li>
              </ul>
              <div class="tab-content">
                  <table class="table table-hover notification-table">
                      <thead>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Unit</th>
                        <th>Plate No</th>
                        <th></th>
                      </thead>
                      <tbody>
                          @foreach($corporate_customers as $customer)
                              <tr>
                                  <td class="notification-date">{{ date('m/d/Y', strtotime($customer->date)) }}</td>
                                  <td class="customer-name"><a href="{{ url('/') }}/transaction/transaction/{{ $customer->reference_id }}">{{ $customer->name }}</a></td>
                                  <td>{{ $customer->contact }}</td>
                                  <td>{{ $customer->make }}</td>
                                  <td>{{ $customer->plate }}</td>
                                  <td class="cta-column"><a href="{{ url('/') }}/transaction/update/{{ $customer->reference_id }}/done/corporate" class="btn mark-cta"><span class="glyphicon glyphicon-ok"></span> Mark as done</a></td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
       </div>
    </div>
</div>
@stop
