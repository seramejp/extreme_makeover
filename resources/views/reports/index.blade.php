@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <div class="alert alert-info">
           You have pending oil change requests. <a href="{{ url('/notifications/walk-in/pending/') }}" class="alert-link">Go to notifications.</a>
       </div>

       <!-- Page Heading -->
       <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
          <span class="glyphicon glyphicon-list-alt"></span> Reports
      </h1>
        </div>
       </div>
       <!-- /.row -->
       <div class="row">
           <div class="col-lg-12">
              <form class="form-inline report-list-filter" id="reports_page_form" onsubmit="return reports_show_report();">
                  <div class="form-group">
                      <label class="control-label" for="start-date">Report Type</label>
                      <select id="report_type" name="report_type" class="form-control">
                          <option value="transactions">Transactions</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label class="control-label" for="start-date">Start Date</label>
                      <input type="text" class="form-control" id="start-date" placeholder="Start Date">
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="end-date">End Date</label>
                    <input type="text" class="form-control" id="end-date" placeholder="End Date">
                  </div>
                  <div class="form-group">
                      <label class="control-label" for="customer-type">Type</label>
                      <select class="form-control" id="customer-type">
                        <option value="all">All</option>
                        <option value="walkin">Walk-In</option>
                        <option value="corporate">Corporate</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label class="control-label" for="transaction-status">Status</label>
                      <select class="form-control" id="transaction-status">
                        <option>All</option>
                        <option>Done</option>
                      </select>
                  </div>
                  <button type="submit" class="btn btn-default">Filter</button>
                </form>

          </div>
       </div>

       <div id="reports_page_result">
       </div>
       <!-- /.row -->
   </div>

    <script>
        function reports_show_report()
        {
            $("#reports_page_result").html("<div>Please wait...</div>");

            var report = $('#report_type').val();
            var start = $('#start-date').val();
            var end = $('#end-date').val();
            var type = $('#customer-type').val();
            var status = $('#transaction-status').val();
            var url = "<?=url('/')?>/reports/filter/?report="+ report +"&start="+ start +"&end="+ end +"&type="+ type +"&status="+ status +"";

            $("#reports_page_result").load(url, function() {
                $("#reports_page_result tr td:last-child").hide();
            });

            return false;
        }
    </script>
   <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
@stop