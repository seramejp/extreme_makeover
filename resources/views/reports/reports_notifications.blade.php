@extends('layouts.admin')
@section('content') 
<div id="page-wrapper">
   <div class="container-fluid">

       <!-- Page Heading -->
       <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">
          <span class="glyphicon glyphicon-time"></span> Pending Oil Changes
      </h1>
              @if(Session::has('notification-pending-to-done-message'))
                      <div class="alert alert-success">
                            {{ Session::get('notification-pending-to-done-message') }}
                      </div>
              @endif
        </div>
       </div>
       <!-- /.row -->
       <div class="row">
           <div class="col-lg-12">
                <table class="table table-hover table-striped notification-pending-table" id="reports-table">
                  <thead>
                      <tr>
                          <th>Type</th>
                          <th>Name</th>
                          <th>CONTACT NO.</th>
                          <th>UNIT</th>
                          <th>PLATE NO.</th>
                          <th>PERSONNEL</th>
                          <th>LAST CHANGE OIL</th>
                          <th>Km. / Run</th>
                          <th>ODO Meter</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody>
                  @foreach( $transactions as $item )
                     @if ($item->is_done != '1')
                        <tr id="pending{{ $item->transaction_id }}">
                            <td>{{ $item->is_company == "1" ? "Corporate" : "Walk-in" }}</td>
                            <td><a href="<?=url('/')?>/transaction/transaction/{{ $item->transaction_id }}">{{ $item->name }}</a></td>
                            <td>{{ $item->contact }}</td>
                            <td>{{ $item->make }}</td>
                            <td>{{ $item->plate }}</td>
                            <td>{{ $item->personnel_name }}</td>
                            <td>{{ Carbon\Carbon::parse($item->date)->format('Y/m/d') }}</td>
                            <td>{{ $item->km_run }} months</td>
                            <td>{{ $item->odo_reading }}</td>
                            <td class="table-cta-data">
                                <a href="<?=url('/')?>/transactions/{{ $item->transaction_id }}/edit" class="table-edit-cta"><span class="glyphicon glyphicon-edit"></span></a>
                                    <a href="#" onclick="return markasdone({{ $item->transaction_id }});" class="table-edit-cta"><span class="glyphicon glyphicon-ok"></span></a>
                            </td>
                        </tr>
                      @endif
                  @endforeach
                   </tbody>
                </table>
          </div>
       </div>
       <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>
  function markasdone(id)
    {
        var con = confirm("Please confirm transaction approval.");
        if (con)
        {
            var url = "<?=url('/')?>/transaction/markAsDone/" + id + "/";
            $.ajax({url: url, success: function(){
                $("#pending" + id).fadeOut(300);
                $("#pending" + id).hide();

            }});
            return false;
        }
    }
</script>
@stop