<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('transaction_products')) {
            Schema::create('transaction_products', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('transaction_id')->unsignedInteger();
                $table->integer('product_id')->unsignedInteger();
                $table->integer('qty');
                $table->string('unit');
                $table->double('price');
                $table->double('subtotal');
                $table->timestamps();
                $table->timestamp('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
