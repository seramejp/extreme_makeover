<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountsreceivablesTableAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts_receivables', function (Blueprint $table) {
            $table->string('Name')->nullable()->change();
            $table->string('Unit')->nullable()->change();
            $table->string('Plate_No')->nullable()->change();
            $table->string('last_change_oil')->nullable()->change();
            $table->string('type')->nullable()->change();
            $table->string('grand_total')->nullable()->change();
            $table->string('paid')->nullable()->change();
            $table->string('balance')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
