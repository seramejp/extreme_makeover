<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSupplierorderdeliveriesAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_order_deliveries', function (Blueprint $table) {
            $table->integer('order_id')->nullable()->change();
            $table->integer('product_id')->nullable()->change();
            $table->integer('received_qty')->nullable()->change();
            $table->string('received_by')->nullable()->change();
            $table->date('received_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
