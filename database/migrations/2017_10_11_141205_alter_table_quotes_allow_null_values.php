<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableQuotesAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->dateTime('date')->nullable()->change();
            $table->integer('customer_id')->unsignedInteger()->nullable()->change();
            $table->integer('vehicle_id')->unsignedInteger()->nullable()->change();
            $table->integer('product_id')->unsignedInteger()->nullable()->change();
            $table->integer('detail_id')->unsignedInteger()->nullable()->change();
            $table->string('odo_reading')->nullable()->change();
            $table->integer('km_run')->nullable()->change();
            $table->integer('is_quote')->nullable()->change();
            $table->integer('qty_differential')->nullable()->change();
            $table->integer('qty_transmission')->nullable()->change();
            $table->integer('qty_engine')->nullable()->change();
            $table->integer('is_done')->nullable()->change();
            $table->integer('is_asf')->nullable()->change();
            $table->integer('personnel_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
