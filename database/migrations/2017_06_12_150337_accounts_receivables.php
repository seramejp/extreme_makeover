<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountsReceivables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('sales_payment_history', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->double('amount');
            $table->date('date_paid');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('sales_payment_history');
    }
}
