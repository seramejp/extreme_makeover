<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNotificationrecordAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification_record', function (Blueprint $table) {
            $table->date('date')->nullable()->change();
            $table->string('name')->nullable()->change();
            $table->string('summary')->nullable()->change();
            $table->integer('type_id')->nullable()->change();
            $table->integer('reference_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
