<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductPriceAndCustomerVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // products
        if(!Schema::hasColumn('products', 'price')) {
            Schema::table('products', function($table) {
                $table->float('price')->after('brand_id');
            });
        }

        // customer vehicles
        if (!Schema::hasTable('customer_vehicles')) {
            Schema::create('customer_vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('customer_id')->unsignedInteger();
                $table->integer('vehicle_id')->unsignedInteger();
                $table->timestamps();
                $table->timestamp('deleted_at');
            });
        }
        if (!Schema::hasTable('price_history')) {
            Schema::create('price_history', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsignedInteger();
                $table->float('price');
                $table->timestamps();
                $table->timestamp('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
