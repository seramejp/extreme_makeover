<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTableAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('duration')->nullable()->change();
            $table->string('odo_reading')->nullable()->change();
            $table->integer('type_id')->unsignedInteger()->nullable()->change();
            $table->integer('brand_id')->unsignedInteger()->nullable()->change();
            $table->float('price')->nullable()->change();
            $table->integer('qty')->nullable()->change();
            $table->integer('service_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
