<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPaymenthistoryTableAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_history', function(Blueprint $table)
        {
            $table->integer('transaction_id')->unsigned()->nullable()->change();
            $table->integer('customer_id')->unsigned()->nullable()->change();
            $table->date('payment_date')->nullable()->change();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
