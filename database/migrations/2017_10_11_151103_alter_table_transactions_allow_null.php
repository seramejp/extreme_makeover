<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTransactionsAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dateTime('date')->nullable()->change();
            $table->integer('customer_id')->unsignedInteger()->nullable()->change();
            $table->integer('vehicle_id')->unsignedInteger()->nullable()->change();
            $table->integer('product_id')->unsignedInteger()->nullable()->change();
            $table->integer('detail_id')->unsignedInteger()->nullable()->change();
            $table->string('odo_reading')->nullable()->change();
            $table->integer('km_run')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
