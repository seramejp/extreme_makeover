<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateaccountsReceivablesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_receivables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name');
            $table->string('Unit');
            $table->string('Plate_No');
            $table->string('last_change_oil');
            $table->string('type');
            $table->string('grand_total');
            $table->string('paid');
            $table->string('balance');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts_receivables');
    }
}
