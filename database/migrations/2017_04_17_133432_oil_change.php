<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OilChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // add qty_engine
        if(!Schema::hasColumn('transactions', 'qty_engine')) {
            Schema::table('transactions', function($table) {
                $table->string('qty_engine')->after('is_quote');
            });
        }
        // add qty_transmission
        if(!Schema::hasColumn('transactions', 'qty_transmission')) {
            Schema::table('transactions', function($table) {
                $table->string('qty_transmission')->after('is_quote');
            });
        }
        // add qty_differential
        if(!Schema::hasColumn('transactions', 'qty_differential')) {
            Schema::table('transactions', function($table) {
                $table->string('qty_differential')->after('is_quote');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
