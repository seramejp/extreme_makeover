<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductQty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('products', 'qty')) {
            Schema::table('products', function($table) {
                $table->integer('qty')->after('price');
            });
        }

        if (!Schema::hasTable('qty_history')) {
            Schema::create('qty_history', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsignedInteger();
                $table->integer('qty');
                $table->timestamps();
                $table->timestamp('deleted_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
