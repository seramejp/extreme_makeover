<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisapprovedTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->integer('customer_id')->unsignedInteger();
            $table->integer('vehicle_id')->unsignedInteger();
            $table->integer('product_id')->unsignedInteger();
            $table->integer('detail_id')->unsignedInteger();
            $table->string('odo_reading');
            $table->double('subtotal');
            $table->integer('km_run');
            $table->integer('is_quote');
            $table->integer('qty_differential');
            $table->integer('qty_transmission');
            $table->integer('qty_engine');
            $table->integer('is_done');
            $table->integer('is_asf');
            $table->integer('personnel_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('quotes');
    }
}
