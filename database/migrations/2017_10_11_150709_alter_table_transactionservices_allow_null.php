<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTransactionservicesAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_services', function(Blueprint $table)
            {
                $table->integer('transaction_id')->unsigned()->nullable()->change();
                $table->integer('service_id')->unsigned()->nullable()->change();
                $table->string('details')->nullable()->nullable()->change();
                $table->integer('product_id')->unsigned()->nullable()->change();
             
                $table->string('unit')->nullable()->change();
             
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
