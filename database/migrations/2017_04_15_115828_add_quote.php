<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // add quote
        if(!Schema::hasColumn('transactions', 'is_quote')) {
            Schema::table('transactions', function($table) {
                $table->boolean('is_quote')->after('km_run');
            });
        }
        // add personnel
        if(!Schema::hasColumn('transactions', 'personnel_id')) {
            Schema::table('transactions', function($table) {
                $table->boolean('personnel_id')->after('is_asf');
            });
        }

        // add odo_reading
        if(!Schema::hasColumn('transaction_products', 'odo_reading')) {
            Schema::table('transaction_products', function($table) {
                $table->string('odo_reading')->after('product_id');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
