<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletedAtFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // brands
        Schema::table('brands', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // customers
        Schema::table('customers', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // details
        Schema::table('details', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // products
        Schema::table('products', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // transaction_statuses
        Schema::table('transaction_statuses', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // transactions
        Schema::table('transactions', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // types
        Schema::table('types', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // users
        Schema::table('users', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
        // vehicles
        Schema::table('vehicles', function($table) {
            $table->timestamp('deleted_at')->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
