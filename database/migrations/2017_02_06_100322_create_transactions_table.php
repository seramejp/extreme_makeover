<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->integer('customer_id')->unsignedInteger();
            $table->integer('vehicle_id')->unsignedInteger();
            $table->integer('product_id')->unsignedInteger();
            $table->integer('detail_id')->unsignedInteger();
            $table->string('odo_reading');
            $table->double('subtotal');
            $table->integer('km_run');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
