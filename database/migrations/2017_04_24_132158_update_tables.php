<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // update customers table
        if (!Schema::hasColumn('customers', 'birthday'))
        {
            Schema::table('customers', function(Blueprint $table)
            {
                $table->date('birthday')->after('company');
            });
        }

        if (!Schema::hasColumn('customers', 'address'))
        {
            Schema::table('customers', function(Blueprint $table)
            {
                $table->text('address')->after('company');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
