<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSupplierorderdetailsAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_order_details', function (Blueprint $table) {
            $table->integer('order_id')->nullable()->change();
            $table->integer('product_id')->nullable()->change();
            $table->integer('qty')->nullable()->change();
            $table->decimal('price')->nullable()->change();
            $table->decimal('sub_total')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}