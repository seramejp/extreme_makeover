<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCustomervehiclesTableAllowNullValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_vehicles', function (Blueprint $table) {
                $table->integer('customer_id')->unsignedInteger()->nullable()->change();
                $table->integer('vehicle_id')->unsignedInteger()->nullable()->change();
                $table->integer('detail_id')->unsigned()->nullable()->change();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
