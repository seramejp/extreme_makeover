<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create new table: services
        if (!Schema::hasTable('services'))
        {
            Schema::create('services', function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
                $table->softDeletes();
            });
        }

        // create new table: transaction_services
        if (!Schema::hasTable('transaction_services'))
        {
            Schema::create('transaction_services', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('transaction_id')->unsigned();
                $table->integer('service_id')->unsigned();
                $table->string('details')->nullable();
                $table->integer('product_id')->unsigned();
                $table->double('qty');
                $table->string('unit');
                $table->softDeletes();
            });
        }

        // update transaction_products
        if (!Schema::hasColumn('transaction_products', 'notes'))
        {
            Schema::table('transaction_products', function(Blueprint $table)
            {
                $table->text('notes')->after('subtotal');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('services');
    }
}
