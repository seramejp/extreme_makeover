<?php

use Illuminate\Database\Seeder;

class NotificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notification_type')->insert([
            ['name' => 'walkin'],
            ['name' => 'corporate'],
            ['name' => 'walkin done'],
            ['name' => 'corporate done'],
            ['name' => 'walkin service'],
            ['name' => 'corporate service'],
            ['name' => 'birthday'],
        ]);
    }
}
