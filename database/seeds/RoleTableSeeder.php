<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Administrator'; // optional
        $admin->save();        
        
        $staff = new Role();
        $staff->name = 'staff';
        $staff->display_name = 'Staff'; // optional
        $staff->save();

        // role admin role to default user.     
        $user = User::where('id', '=', '1')->first();
        $user->attachRole($admin);
    }
}
