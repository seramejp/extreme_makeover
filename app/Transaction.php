<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    //
    use SoftDeletes;
    protected $table = 'transactions';
    protected $primaryKey  = 'id';

	protected $dates = ['deleted_at'];

    protected $guarded = array();
}
