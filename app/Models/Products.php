<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Products
 * @package App\Models
 * @version February 27, 2017, 2:26 pm UTC
 */
class Products extends Model
{
    use SoftDeletes;

    public $table = 'products';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'duration',
        'type_id',
        'brand_id',
        'price',
        'qty',
        'odo_reading',
        'service_id'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'duration' => 'string',
        'type_id' => 'integer',
        'brand_id' => 'integer',
        'price' => 'double',
        'qty' => 'integer',
        'odo_reading' => 'string',
        'service_id' => 'integer'
    ];

    
}
