<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QtyHistory
 * @package App\Models
 * @version March 22, 2017, 10:26 am UTC
 */
class QtyHistory extends Model
{
    use SoftDeletes;

    public $table = 'qty_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'qty',
        'adjusted'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'product_id'    => 'integer',
        'qty'           => 'integer',
        'adjusted'      => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
