<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AsfTransactions
 * @package App\Models
 * @version March 26, 2017, 2:17 pm UTC
 */
class AsfTransactions extends Model
{
    use SoftDeletes;

    public $table = 'transactions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'date',
        'customer_id',
        'vehicle_id',
        'product_id',
        'detail_id',
        'odo_reading',
        'subtotal',
        'km_run',
        'is_done',
        'is_asf'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'vehicle_id' => 'integer',
        'product_id' => 'integer',
        'detail_id' => 'integer',
        'odo_reading' => 'string',
        'subtotal' => 'float',
        'km_run' => 'integer',
        'is_done' => 'boolean',
        'is_asf' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
