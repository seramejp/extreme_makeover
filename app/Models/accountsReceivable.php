<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AccountsReceivable
 * @package App\Models
 * @version July 12, 2017, 1:10 pm UTC
 */
class AccountsReceivable extends Model
{
    use SoftDeletes;

    public $table = 'accounts_receivables';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'Name',
        'Unit',
        'Plate_No',
        'last_change_oil',
        'type',
        'grand_total',
        'paid',
        'balance'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Name' => 'string',
        'Unit' => 'string',
        'Plate_No' => 'string',
        'last_change_oil' => 'string',
        'type' => 'string',
        'grand_total' => 'string',
        'paid' => 'string',
        'balance' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
