<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Quotes
 * @package App\Models
 * @version May 24, 2017, 10:43 am UTC
 */
class Quotes extends Model
{
    use SoftDeletes;

    public $table = 'quotes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'date',
        'customer_id',
        'vehicle_id',
        'product_id',
        'detail_id',
        'odo_reading',
        'subtotal',
        'km_run',
        'is_quote',
        'qty_differential',
        'qty_transmission',
        'qty_engine',
        'is_done',
        'is_asf',
        'personnel_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'vehicle_id' => 'integer',
        'product_id' => 'integer',
        'detail_id' => 'integer',
        'odo_reading' => 'string',
        'subtotal' => 'float',
        'km_run' => 'integer',
        'is_quote' => 'boolean',
        'qty_differential' => 'string',
        'qty_transmission' => 'string',
        'qty_engine' => 'string',
        'is_done' => 'boolean',
        'is_asf' => 'boolean',
        'personnel_id' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
