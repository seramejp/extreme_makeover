<?php

namespace App\Repositories;

use App\Models\PriceHistory;
use InfyOm\Generator\Common\BaseRepository;

class PriceHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PriceHistory::class;
    }
}
