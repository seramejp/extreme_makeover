<?php

namespace App\Repositories;

use App\Models\AfterSales;
use InfyOm\Generator\Common\BaseRepository;

class AfterSalesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'customer_id',
        'vehicle_id',
        'product_id',
        'detail_id',
        'odo_reading',
        'subtotal',
        'km_run',
        'is_done'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AfterSales::class;
    }
}
