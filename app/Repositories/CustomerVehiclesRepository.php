<?php

namespace App\Repositories;

use App\Models\CustomerVehicles;
use InfyOm\Generator\Common\BaseRepository;

class CustomerVehiclesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'vehicle_id',
        'detail_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerVehicles::class;
    }
}
