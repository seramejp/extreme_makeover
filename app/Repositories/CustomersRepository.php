<?php

namespace App\Repositories;

use App\Models\Customers;
use InfyOm\Generator\Common\BaseRepository;

class CustomersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'contact',
        'company',
        'is_company',
        'birthday',
        'address'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customers::class;
    }
}
