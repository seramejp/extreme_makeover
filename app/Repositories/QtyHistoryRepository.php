<?php

namespace App\Repositories;

use App\Models\QtyHistory;
use InfyOm\Generator\Common\BaseRepository;

class QtyHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'qty'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return QtyHistory::class;
    }
}
