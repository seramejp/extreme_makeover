<?php

namespace App\Repositories;

use App\Models\accountsReceivable;
use InfyOm\Generator\Common\BaseRepository;

class accountsReceivableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'Name',
        'Unit',
        'Plate_No',
        'last_change_oil',
        'type',
        'grand_total',
        'paid',
        'balance'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return accountsReceivable::class;
    }
}
