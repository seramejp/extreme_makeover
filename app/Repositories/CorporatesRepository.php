<?php

namespace App\Repositories;

use App\Models\Corporates;
use InfyOm\Generator\Common\BaseRepository;

class CorporatesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'contact',
        'company',
        'is_company',
        'address'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Corporates::class;
    }
}
