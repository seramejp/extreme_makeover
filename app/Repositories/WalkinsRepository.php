<?php

namespace App\Repositories;

use App\Models\Walkins;
use InfyOm\Generator\Common\BaseRepository;

class WalkinsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'contact',
        'company',
        'is_company',
        'birthday',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Walkins::class;
    }
}
