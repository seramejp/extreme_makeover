<?php

namespace App\Repositories;

use App\Models\Personnel;
use InfyOm\Generator\Common\BaseRepository;

class PersonnelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Personnel::class;
    }
}
