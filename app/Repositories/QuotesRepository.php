<?php

namespace App\Repositories;

use App\Models\Quotes;
use InfyOm\Generator\Common\BaseRepository;

class QuotesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'customer_id',
        'vehicle_id',
        'product_id',
        'detail_id',
        'odo_reading',
        'subtotal',
        'km_run',
        'is_quote',
        'qty_differential',
        'qty_transmission',
        'qty_engine',
        'is_done',
        'is_asf',
        'personnel_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Quotes::class;
    }
}
