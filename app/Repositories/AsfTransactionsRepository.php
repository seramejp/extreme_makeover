<?php

namespace App\Repositories;

use App\Models\AsfTransactions;
use InfyOm\Generator\Common\BaseRepository;

class AsfTransactionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'customer_id',
        'vehicle_id',
        'product_id',
        'detail_id',
        'odo_reading',
        'subtotal',
        'km_run',
        'is_done',
        'is_asf'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AsfTransactions::class;
    }
}
