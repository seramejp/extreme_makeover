<?php
namespace App\Http\Controllers;
use App\Http\Requests\CreateAccountsReceivableRequest;
use App\Http\Requests\UpdateAccountsReceivableRequest;
use App\Repositories\AccountsReceivableRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class AccountsPayableController extends AppBaseController
{
    /**
     * Display a listing of the AccountsReceivable.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /*$this->accountsReceivableRepository->pushCriteria(new RequestCriteria($request));
        $accountsReceivables = $this->accountsReceivableRepository->all();*/

        $accountsPayables = DB::table('suppliers as s')
            ->select(DB::raw('s.name as supplier, s.address, so.order_date, so.grand_total, sum(sop.payment_amount) as paidamount'))
            ->join('supplier_orders as so', 'so.supplier_id', '=', 's.id')
            ->join('supplier_order_payments as sop', 'sop.order_id', '=', 'so.id')
            ->groupBy('so.id')
            ->orderBy('so.order_date')
            ->get();

        return view('accounts_payables.index')
            ->with('accountsPayables', $accountsPayables);
    }
}
