<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Models\Customers;
use App\Models\Transactions;
use App\Repositories\PaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class PaymentController extends AppBaseController
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Payment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $payments = DB::table("payment_history as p")
        ->select("p.id", "t.date as transaction_date", "c.name as customer_name", "p.amount", "p.payment_date", "p.transaction_id", "t.customer_id")
        ->leftJoin("transactions as t", "t.id", "p.transaction_id")
        ->leftJoin("customers as c", "c.id", "p.customer_id")
        ->get();

        return view('payments.index')->with('payments', $payments);
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    /*public function create()
    {
        $transactions = DB::table('transactions as t')
          ->select('t.id', 't.date', 'v.make as vehicle_name', 'd.plate as detail_plate', 'd.color as detail_color')
          ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
          ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
          ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
          ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
          ->where('t.is_quote', 0)
          ->get();
        $transactionsArray = array();
        foreach ($transactions as $t)
        {
            $transactionsArray[$t->id] = $t->vehicle_name . ' (' . $t->detail_plate . ') ' . $t->detail_color;
        }

        $customers = Customers::pluck('name', 'id');
        return view('payments.create')->with("customers", $customers)->with("transactions", $transactionsArray);
    }*/

    public function create(){
        $transactions = DB::table('transactions as t')
          ->select('t.id', 't.date', 't.total', 'ph.amount', 't.customer_id', 'c.name as customer_name', DB::raw('concat(v.make, " (", d.plate, ") ", d.color) as vehicle_detail'))
          ->join('customers as c', 'c.id', '=', 't.customer_id')
          ->join('vehicles as v', 'v.id', '=', 't.vehicle_id')
          ->join('details as d', 'd.id', '=', 't.detail_id')
          ->leftJoin('payment_history as ph', 'ph.transaction_id', 't.id')
          ->where("t.total", ">", 0)
          ->where("t.total", ">", "sum(ph.amount)")
          ->where('t.is_quote', 0)
          ->groupBy('t.id')
          ->orderBy('vehicle_detail')
          ->get();

        $customers = Customers::select('name', 'id')->get();
        return view('payments.create', compact('customers', 'transactions'));
    }




    /*public function createFromTransaction($id)
    {
        $transactions = DB::table('transactions as t')
                          ->select('t.id', 't.date', 'c.id as customer_id', 'v.make as vehicle_name', 'd.plate as detail_plate', 'd.color as detail_color')
                          ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
                          ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
                          ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
                          ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
                          ->where('t.id', $id)
                          ->get();
        $transactionsArray = array();
        $customerId = 0;
        foreach ($transactions as $t)
        {
            $transactionsArray[$t->id] = $t->vehicle_name . ' (' . $t->detail_plate . ') ' . $t->detail_color;
            $customerId = $t->customer_id;
        }

        $customers = Customers::where('id', $customerId)->pluck('name', 'id');
        return view('payments.create')->with("customers", $customers)->with("transactions", $transactionsArray);
    }*/


    public function createFromTransaction($id)
    {
        $transactions = DB::table('transactions as t')
          ->select('t.id', 't.date', 't.total', 'ph.amount', 't.customer_id', 'c.name as customer_name', DB::raw('concat(v.make, " (", d.plate, ") ", d.color) as vehicle_detail'))
          ->join('customers as c', 'c.id', '=', 't.customer_id')
          ->join('vehicles as v', 'v.id', '=', 't.vehicle_id')
          ->join('details as d', 'd.id', '=', 't.detail_id')
          ->leftJoin('payment_history as ph', 'ph.transaction_id', 't.id')
          ->where("t.total", ">", 0)
          ->where("t.total", ">", "sum(ph.amount)")
          ->where('t.is_quote', 0)
          ->where('t.id', $id)
          ->groupBy('t.id')
          ->orderBy('vehicle_detail')
          ->get();

        $customers = Customers::select('name', 'id')->get();
        return view('payments.create')->with("customers", $customers)->with("transactions", $transactions);
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $input = $request->all();
        
        $payment = $this->paymentRepository->create($input);

        Flash::success('Payment saved successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.show')->with('payment', $payment);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.edit')->with('payment', $payment);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $payment = $this->paymentRepository->update($request->all(), $id);

        Flash::success('Payment updated successfully.');

        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $this->paymentRepository->delete($id);

        Flash::success('Payment deleted successfully.');

        return redirect(route('payments.index'));
    }
}
