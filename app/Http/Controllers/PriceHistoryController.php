<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePriceHistoryRequest;
use App\Http\Requests\UpdatePriceHistoryRequest;
use App\Repositories\PriceHistoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PriceHistoryController extends AppBaseController
{
    /** @var  PriceHistoryRepository */
    private $priceHistoryRepository;

    public function __construct(PriceHistoryRepository $priceHistoryRepo)
    {
        $this->priceHistoryRepository = $priceHistoryRepo;
    }

    /**
     * Display a listing of the PriceHistory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $priceHistories = DB::table('price_history as ph')
            ->select('p.name', 'ph.price', 'ph.created_at')
            ->leftJoin('products as p', 'p.id', '=', 'ph.product_id')
            ->where("ph.price", ">", 0)
            ->get();

        return view('price_histories.index')
            ->with('priceHistories', $priceHistories);
    }

    /**
     * Show the form for creating a new PriceHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('price_histories.create');
    }

    /**
     * Store a newly created PriceHistory in storage.
     *
     * @param CreatePriceHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreatePriceHistoryRequest $request)
    {
        $input = $request->all();

        $priceHistory = $this->priceHistoryRepository->create($input);

        Flash::success('Price History saved successfully.');

        return redirect(route('priceHistories.index'));
    }

    /**
     * Display the specified PriceHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $priceHistory = $this->priceHistoryRepository->findWithoutFail($id);

        if (empty($priceHistory)) {
            Flash::error('Price History not found');

            return redirect(route('priceHistories.index'));
        }

        return view('price_histories.show')->with('priceHistory', $priceHistory);
    }

    /**
     * Show the form for editing the specified PriceHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $priceHistory = $this->priceHistoryRepository->findWithoutFail($id);

        if (empty($priceHistory)) {
            Flash::error('Price History not found');

            return redirect(route('priceHistories.index'));
        }

        return view('price_histories.edit')->with('priceHistory', $priceHistory);
    }

    /**
     * Update the specified PriceHistory in storage.
     *
     * @param  int              $id
     * @param UpdatePriceHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriceHistoryRequest $request)
    {
        $priceHistory = $this->priceHistoryRepository->findWithoutFail($id);

        if (empty($priceHistory)) {
            Flash::error('Price History not found');

            return redirect(route('priceHistories.index'));
        }

        $priceHistory = $this->priceHistoryRepository->update($request->all(), $id);

        Flash::success('Price History updated successfully.');

        return redirect(route('priceHistories.index'));
    }

    /**
     * Remove the specified PriceHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $priceHistory = $this->priceHistoryRepository->findWithoutFail($id);

        if (empty($priceHistory)) {
            Flash::error('Price History not found');

            return redirect(route('priceHistories.index'));
        }

        $this->priceHistoryRepository->delete($id);

        Flash::success('Price History deleted successfully.');

        return redirect(route('priceHistories.index'));
    }
}
