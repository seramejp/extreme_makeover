<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAsfTransactionsRequest;
use App\Http\Requests\UpdateAsfTransactionsRequest;
use App\Repositories\AsfTransactionsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class AsfTransactionsController extends AppBaseController
{
    /** @var  AsfTransactionsRepository */
    private $asfTransactionsRepository;

    public function __construct(AsfTransactionsRepository $asfTransactionsRepo)
    {
        $this->asfTransactionsRepository = $asfTransactionsRepo;
    }

    /**
     * Display a listing of the AsfTransactions.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $asfTransactions = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
                'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company',
                'v.make as vehicle_name', 'p.name as personnel_name', 'd.plate as detail_plate',
                'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.is_done', '=', '1')
            ->where('t.is_asf', '=', '1')
            ->groupBy('t.odo_reading')
            ->get();

        return view('asf_transactions.index')
            ->with('asfTransactions', $asfTransactions);
    }

    /**
     * Show the form for creating a new AsfTransactions.
     *
     * @return Response
     */
    public function create()
    {
        return view('asf_transactions.create');
    }

    /**
     * Store a newly created AsfTransactions in storage.
     *
     * @param CreateAsfTransactionsRequest $request
     *
     * @return Response
     */
    public function store(CreateAsfTransactionsRequest $request)
    {
        $input = $request->all();

        $asfTransactions = $this->asfTransactionsRepository->create($input);

        Flash::success('Asf Transactions saved successfully.');

        return redirect(route('asfTransactions.index'));
    }

    /**
     * Display the specified AsfTransactions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $asfTransactions = $this->asfTransactionsRepository->findWithoutFail($id);

        if (empty($asfTransactions)) {
            Flash::error('Asf Transactions not found');

            return redirect(route('asfTransactions.index'));
        }

        return view('asf_transactions.show')->with('asfTransactions', $asfTransactions);
    }

    /**
     * Show the form for editing the specified AsfTransactions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $asfTransactions = $this->asfTransactionsRepository->findWithoutFail($id);

        if (empty($asfTransactions)) {
            Flash::error('Asf Transactions not found');

            return redirect(route('asfTransactions.index'));
        }

        return view('asf_transactions.edit')->with('asfTransactions', $asfTransactions);
    }

    /**
     * Update the specified AsfTransactions in storage.
     *
     * @param  int              $id
     * @param UpdateAsfTransactionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAsfTransactionsRequest $request)
    {
        $asfTransactions = $this->asfTransactionsRepository->findWithoutFail($id);

        if (empty($asfTransactions)) {
            Flash::error('Asf Transactions not found');

            return redirect(route('asfTransactions.index'));
        }

        $asfTransactions = $this->asfTransactionsRepository->update($request->all(), $id);

        Flash::success('Asf Transactions updated successfully.');

        return redirect(route('asfTransactions.index'));
    }

    /**
     * Remove the specified AsfTransactions from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $asfTransactions = $this->asfTransactionsRepository->findWithoutFail($id);

        if (empty($asfTransactions)) {
            Flash::error('Asf Transactions not found');

            return redirect(route('asfTransactions.index'));
        }

        $this->asfTransactionsRepository->delete($id);

        Flash::success('Asf Transactions deleted successfully.');

        return redirect(route('asfTransactions.index'));
    }
}
