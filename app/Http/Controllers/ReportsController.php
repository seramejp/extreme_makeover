<?php

namespace App\Http\Controllers;

use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\DAYOFYEAR;
use Session;

class ReportsController extends Controller
{
     /**
     * UserController constructor
     */
    public function __construct() {
        $this->middleware('auth');
    } 
  
    /**
     * @description Serves as the report page's main view.
     * Should include all for this month's transactions
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $top10 = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.date', 'desc')
            ->groupBy('t.odo_reading')
            ->get();
        return view('reports.index')->with('transactions', $top10)->with('status', 'hi');
    }

    /*public function notifications($type = "walk-in") {

        $count = 0;

        $transactions_w = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.date', 'asc')
            ->groupBy('t.odo_reading')
            ->where('c.is_company', '=', '0')
            ->where('t.is_done', '=', '0')
            ->get();

        $walkIns = array();
        foreach ($transactions_w as $item) {
            // get transaction date and set to carbon
            $futureDate = Carbon::parse($item->date)->addMonth($item->km_run);
            $currentDate = Carbon::now();
            if ($futureDate->lt($currentDate)) {
                $walkIns[] = $item;
                $count++;
            }
        }

        $transactions_c = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.date', 'asc')
            ->groupBy('t.odo_reading')
            ->where('c.is_company', '=', '1')
            ->where('t.is_done', '=', '0')
            ->get();

        $corporates = array();
        foreach ($transactions_c as $item) {
            // get transaction date and set to carbon
            $futureDate = Carbon::parse($item->date)->addMonth($item->km_run);
            $currentDate = Carbon::now();
            if ($futureDate->lt($currentDate)) {
                $corporates[] = $item;
                $count++;
            }
        }

        // for done transactions
        $countD = 0;
        $transactions_wd = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.date', 'asc')
            ->groupBy('t.odo_reading')
            ->where('c.is_company', '=', '0')
            ->where('t.is_done', '=', '1')
            ->where('t.is_asf', '=', '0')
            ->get();

        $walkInsD = array();
        foreach ($transactions_wd as $item) {
            // get transaction date and set to carbon
            $futureDate = Carbon::parse($item->date)->addMonth($item->km_run);
            $currentDate = Carbon::now();
            if ($futureDate->lt($currentDate)) {
                $walkInsD[] = $item;
                $countD++;
            }
        }

        $transactions_cd = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.date', 'asc')
            ->groupBy('t.odo_reading')
            ->where('c.is_company', '=', '1')
            ->where('t.is_done', '=', '1')
            ->where('t.is_asf', '=', '0')
            ->groupBy('t.odo_reading')
            ->get();

        $corporatesD = array();
        foreach ($transactions_cd as $item) {
            // get transaction date and set to carbon
            $futureDate = Carbon::parse($item->date)->addMonth($item->km_run);
            $currentDate = Carbon::now();
            if ($futureDate->lt($currentDate)) {
                $corporatesD[] = $item;
                $countD++;
            }
        }

        if ($type == "corporate") {
            return view('reports.notifications_corporate')
                ->with('walkIns', $walkIns)
                ->with('corporates', $corporates)
                ->with("count", $count)
                ->with('walkInsD', $walkInsD)
                ->with('corporatesD', $corporatesD)
                ->with("countD", $countD);
        }
        else {
            return view('reports.notifications')
                ->with('walkIns', $walkIns)
                ->with('corporates', $corporates)
                ->with("count", $count)
                ->with('walkInsD', $walkInsD)
                ->with('corporatesD', $corporatesD)
                ->with("countD", $countD);
        }

    }*/
    
    public function notifications($type, $status)
    {

        if ($type == 'walk-in')
        {

            if ($status == 'pending') 
            {
                $walkin_customers = DB::table('notification_record as nr')
                    ->select('nr.reference_id as reference_id', 'nr.date as date', 'c.name as name', 'c.contact as contact',
                        'v.make as make', 'd.plate as plate')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->leftJoin('details as d', 't.detail_id', '=', 'd.id')
                    ->leftJoin('vehicles as v', 'd.vehicle_id', '=', 'v.id')
                    ->where('nr.type_id', '=', '1')
                    ->orderBy('nr.date')
                    ->get();

                return view('reports.notifications.walk-in.index')->with('walkin_customers', $walkin_customers);
            }
            
            if ($status == 'done')
            {
                $walkin_customers = DB::table('notification_record as nr')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->leftJoin('details as d', 't.detail_id', '=', 'd.id')
                    ->leftJoin('vehicles as v', 'd.vehicle_id', '=', 'v.id')
                    ->where('nr.type_id', '=', '3')
                    ->orderBy('nr.date')
                    ->get();

                return view('reports.notifications.walk-in.done')->with('walkin_customers', $walkin_customers);
            }
            
            if ($status == 'service')
            {
                $walkin_customers = DB::table('notification_record as nr')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->where('nr.type_id', '=', '5')
                    ->orderBy('nr.date')                    
                    ->get();

                return view('reports.notifications.walk-in.service')->with('walkin_customers', $walkin_customers);              
            }
        }
        
        if ($type == 'corporate')
        {
            if ($status == 'pending') 
            {
                $corporate_customers = DB::table('notification_record as nr')
                    ->select('nr.reference_id as reference_id', 'nr.date as date', 'c.name as name', 'c.contact as contact',
                        'v.make as make', 'd.plate as plate')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->leftJoin('details as d', 't.detail_id', '=', 'd.id')
                    ->leftJoin('vehicles as v', 'd.vehicle_id', '=', 'v.id')
                    ->where('nr.type_id', '=', '2')
                    ->orderBy('nr.date')
                    ->get();

                return view('reports.notifications.corporate.index')->with('corporate_customers', $corporate_customers);
            }
            
            if ($status == 'done')
            {
                $corporate_customers = DB::table('notification_record as nr')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->leftJoin('details as d', 't.detail_id', '=', 'd.id')
                    ->leftJoin('vehicles as v', 'd.vehicle_id', '=', 'v.id')
                    ->where('nr.type_id', '=', '4')
                    ->orderBy('nr.date')
                    ->get();

                return view('reports.notifications.corporate.done')->with('corporate_customers', $corporate_customers);
            }
            
            if ($status == 'service')
            {
                $corporate_customers = DB::table('notification_record as nr')
                    ->leftJoin('transactions as t', 'nr.reference_id', '=', 't.id')
                    ->leftJoin('customers as c', 't.customer_id', '=', 'c.id')
                    ->where('nr.type_id', '=', '6')
                    ->orderBy('nr.date')
                    ->get();

                return view('reports.notifications.corporate.service')->with('corporate_customers', $corporate_customers);              
            }
        }
        
        if ($type == 'birthdays' && $status == 'current')
        {


            $birthday_users = DB::table('notification_record as nr')
                ->leftJoin('customers as c', 'nr.reference_id', '=', 'c.id')
                ->where('type_id', '=', '7')
                ->get();

            return view('reports.notifications.birthdays.index')
                ->with('birthday_users', $birthday_users);
        }
    }

    public function reports_notifications() {

        $transactions = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
            ->orderBy('t.date', 'asc')
            ->groupBy('t.odo_reading')
            ->get();

        $deadline = array();
        $count = 0;
        foreach ($transactions as $item) {
            // get transaction date and set to carbon
            $futureDate = Carbon::parse($item->date)->addMonth($item->km_run);
            $currentDate = Carbon::now();
            if ($futureDate->lt($currentDate)) {
                $deadline[] = $item;
                $count++;
            }
        }

        return view('reports.reports_notifications')->with('transactions', $deadline)->with("count", $count);
    }

    public function reports_notifications_redirect() {
        return redirect()->action('ReportsController@reports_notifications');
    }

    public function notifications_redirect($type1, $type2) {
        return redirect()->action('ReportsController@notifications', [ 'type' => $type2]);
    }

    public function show_with_filter()
    {
        $report = isset($_GET["report"]) ? $_GET["report"] : null;
        $start = isset($_GET["start"]) ? $_GET["start"] : null;
        $end = isset($_GET["end"]) ? $_GET["end"] : null;
        $type = isset($_GET["type"]) ? $_GET["type"] : null;
        $status = isset($_GET["status"]) ? $_GET["status"] : null;

        $collection = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
                'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company', 'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate', 'd.color as detail_color', 'pe.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('personnels as pe', 'pe.id', '=', 't.personnel_id')
            ->groupBy('t.odo_reading')
            ->whereBetween('t.date', [$start, $end])
            ->get();

        /*switch($type)
        {
            case 'walkin': $collection->where('is_company', '=', '0'); break;
            case 'corporate': $collection->where('is_company', '=', '1'); break;
        }*/
        /*$collection->get();*/

        return view('transactions.table')->with('transactions', $collection);
    }
    
    public function refreshNotification()
    {

        return view('reports.notifications.refresh');
    }
    
    public function updateNotificationRecord()
    {
        // Remove existing notification records
        DB::table('notification_record')->delete();

        // Insert updated notification records        
        $notification_data = array_merge(
            $this->get_birthday_data(),
            $this->get_pending_transactions(),
            $this->get_for_asf_transactions(),
            $this->get_for_service_transactions()
        );

        DB::table('notification_record')->insert($notification_data);
       
        Session::flash('notification-update-message', 'Notifications are now up to date!'); 
        return redirect('notifications/refresh-notification');
    }
    
    public function get_birthday_data()
    {
        $current_month = Carbon::today()->month;
        $current_day = Carbon::today()->day;

        $birthday_users = DB::table('customers')
            ->where(DB::raw('MONTH(birthday)'), '=', $current_month)
            ->get(); 
            
        if (!empty($birthday_users))
        {

            $birthday_data = array();

            foreach ($birthday_users as $user)
            {
                $birthday_data[] = array(
                    'name' => $user->name,
                    'type_id' => '7',
                    'reference_id' => $user->id,
                    'date' => $user->birthday
                );
            }
            
            return $birthday_data;
        }
    }
    
    public function get_pending_transactions()
    {
        $customer_transactions = DB::table('transactions as t')
            ->select('t.id as transaction_id', 't.date as date',
                'c.name as customer_name', 'c.company as company_name',
                'c.is_company as is_company', 't.km_run as km_run')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->where('t.is_done', '=', '0')
            ->where('t.is_asf', '=', '0')
            ->get();

        if (!empty($customer_transactions))
        {
            $customers = array();

            foreach ($customer_transactions as $customer_transactions)
            {
                $futureDate = Carbon::parse($customer_transactions->date)->addMonth($customer_transactions->km_run);
                $currentDate = Carbon::now();

                if ($futureDate->lt($currentDate)) {
                    $customers[] = $customer_transactions;
                }
            }

            $transaction_notification_data = array();

            foreach ($customers as $customer)
            {
                if ($customer->is_company == '0')
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->customer_name,
                        'type_id' => '1',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,

                    );
                }
                else
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->company_name,
                        'type_id' => '2',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,
                    );
                }
            }

            return $transaction_notification_data;
        }
    }

    public function get_for_asf_transactions()
    {
        $customer_for_asf_transactions = DB::table('transactions as t')
            ->select('t.id as transaction_id', 't.date as date',
                'c.name as customer_name', 'c.company as company_name',
                'c.is_company as is_company', 't.km_run as km_run')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->where('t.is_done', '=', '1')
            ->where('t.is_asf', '=', '0')
            ->get();
            
        if (!empty($customer_for_asf_transactions))
        {
            $customers = array();

            foreach ($customer_for_asf_transactions as $customer_transactions)
            {
                $futureDate = Carbon::parse($customer_transactions->date)->addMonth($customer_transactions->km_run);
                $currentDate = Carbon::now();

                if ($futureDate->lt($currentDate)) {
                    $customers[] = $customer_transactions;
                }
            }

            $transaction_notification_data = array();

            foreach ($customers as $customer)
            {
                if ($customer->is_company == '0')
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->customer_name,
                        'type_id' => '3',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,

                    );
                }
                else
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->company_name,
                        'type_id' => '4',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,
                    );
                }
            }

            return $transaction_notification_data;
        }
    }
    
    public function get_for_service_transactions()
    {
        $customer_for_service_transactions = DB::table('transactions as t')
            ->select('t.id as transaction_id', 't.date as date',
                'c.name as customer_name', 'c.company as company_name',
                'c.is_company as is_company', 't.km_run as km_run', 'c.contact as contact')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->where('t.is_done', '=', '0')
            ->where('t.is_asf', '=', '0')
            ->get();
            
        if (!empty($customer_for_service_transactions))
        {
            $customers = array();

            foreach ($customer_for_service_transactions as $customer_transactions)
            {
                $futureDate = Carbon::parse($customer_transactions->date)->addMonth(2);
                $currentDate = Carbon::now();

                if ($futureDate->lt($currentDate)) {
                    $customers[] = $customer_transactions;
                }
            }

            $transaction_notification_data = array();

            foreach ($customers as $customer)
            {
                if ($customer->is_company == '0')
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->customer_name,
                        'type_id' => '5',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,
                    );
                }
                else
                {
                    $transaction_notification_data[] = array(
                        'name' => $customer->company_name,
                        'type_id' => '6',
                        'reference_id' => $customer->transaction_id,
                        'date' => $customer->date,
                    );
                }
            }

            return $transaction_notification_data;
        }
    }
}
