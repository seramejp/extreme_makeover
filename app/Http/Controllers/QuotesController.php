<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQuotesRequest;
use App\Http\Requests\UpdateQuotesRequest;
use App\Models\Quotes;
use App\Models\Transactions;
use App\Repositories\QuotesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class QuotesController extends AppBaseController
{
    /** @var  QuotesRepository */
    private $quotesRepository;

    public function __construct(QuotesRepository $quotesRepo)
    {
        $this->quotesRepository = $quotesRepo;
    }

    /**
     * Display a listing of the Quotes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $transactions = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
                'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company',
                'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate',
                'd.color as detail_color', 'p.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('customer_vehicles as cv', 'cv.detail_id', '=', 't.detail_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
            ->where('t.is_quote', 1)
            ->get();

        return view('quotes.index')
            ->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new Quotes.
     *
     * @return Response
     */
    public function create()
    {
        return view('quotes.create');
    }

    /**
     * Store a newly created Quotes in storage.
     *
     * @param CreateQuotesRequest $request
     *
     * @return Response
     */
    public function store(CreateQuotesRequest $request)
    {
        $input = $request->all();

        $quotes = $this->quotesRepository->create($input);

        Flash::success('Quotes saved successfully.');

        return redirect(route('quotes.index'));
    }

    /**
     * Display the specified Quotes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $quotes = $this->quotesRepository->findWithoutFail($id);

        if (empty($quotes)) {
            Flash::error('Quotes not found');

            return redirect(route('quotes.index'));
        }

        return view('quotes.show')->with('quotes', $quotes);
    }

    /**
     * Show the form for editing the specified Quotes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $quotes = $this->quotesRepository->findWithoutFail($id);

        if (empty($quotes)) {
            Flash::error('Quotes not found');

            return redirect(route('quotes.index'));
        }

        return view('quotes.edit')->with('quotes', $quotes);
    }

    /**
     * Update the specified Quotes in storage.
     *
     * @param  int              $id
     * @param UpdateQuotesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuotesRequest $request)
    {
        $quotes = $this->quotesRepository->findWithoutFail($id);

        if (empty($quotes)) {
            Flash::error('Quotes not found');

            return redirect(route('quotes.index'));
        }

        $quotes = $this->quotesRepository->update($request->all(), $id);

        Flash::success('Quotes updated successfully.');

        return redirect(route('quotes.index'));
    }

    /**
     * Remove the specified Quotes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $quotes = $this->quotesRepository->findWithoutFail($id);

        if (empty($quotes)) {
            Flash::error('Quotes not found');

            return redirect(route('quotes.index'));
        }

        $this->quotesRepository->delete($id);

        Flash::success('Quotes deleted successfully.');

        return redirect(route('quotes.index'));
    }

    public function approveQuote($id)
    {
        DB::table('transactions')->where('id', $id)->update(['is_quote' => 0]);
        return 0;
    }

    public function disapproveQuote($id)
    {
        $transaction = DB::table('transactions')->where('id', $id)->get()->toArray();

        foreach ($transaction as $t)
        {
            $t = get_object_vars($t);
            Quotes::insert($t);
            DB::table('transactions')->where('id', $id)->delete();
        }
        return 0;
    }
}
