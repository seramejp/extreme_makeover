<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductsRequest;
use App\Http\Requests\UpdateProductsRequest;
use App\Models\Services;
use App\Repositories\ProductsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use App\Type;
use App\Brand;
use Carbon\Carbon;
use Input;
use Session;

class ProductsController extends AppBaseController
{
    /** @var  ProductsRepository */
    private $productsRepository;

    public function __construct(ProductsRepository $productsRepo)
    {
        $this->productsRepository = $productsRepo;
    }

    /**
     * Display a listing of the Products.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /*$this->productsRepository->pushCriteria(new RequestCriteria($request));
        $products = $this->productsRepository->all();*/

        $products = DB::table('products as p')
            ->select('p.id', 'p.name', 'p.duration', 'p.created_at', 'p.updated_at', 't.name as type', 'b.name as brand', 'p.price', 'p.qty', 'p.odo_reading', 's.name as service')
            ->leftJoin('types as t', 't.id', '=', 'p.type_id')
            ->leftJoin('brands as b', 'b.id', '=', 'p.brand_id')
            ->leftJoin('services as s', 's.id', '=', 'p.service_id')
            ->get();

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Products.
     *
     * @return Response
     */
    public function create()
    {
        $types = Type::pluck('name', 'id');
        $brands = Brand::pluck('name', 'id');
        $services = Services::pluck('name', 'id');

        return view('products.create')
            ->with('types', $types)
            ->with('brands', $brands)
            ->with('services', $services);
    }

    /**
     * Store a newly created Products in storage.
     *
     * @param CreateProductsRequest $request
     *
     * @return Response
     */
    public function store(CreateProductsRequest $request)
    {
        $input = $request->all();

        $products = $this->productsRepository->create($input);

        Flash::success('Product has been added successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Products.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        // $products = $this->productsRepository->findWithoutFail($id);

        $products = DB::table('products as p')
            ->select('p.*', 'p.duration', 'p.created_at', 'p.updated_at', 't.name as type', 'b.name as brand')
            ->leftJoin('types as t', 't.id', '=', 'p.type_id')
            ->leftJoin('brands as b', 'b.id', '=', 'p.brand_id')
            ->where("p.id", "=", $id)->first();

        if (empty($products)) {
            Flash::error('Products not found');

            return redirect(route('products.index'));
        }

        return view('products.show')
            ->with('products', $products);
    }

    /**
     * Show the form for editing the specified Products.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $products = $this->productsRepository->findWithoutFail($id);

        $products = DB::table('products')->where("id", "=", $id)->first();

        if (empty($products)) {
            Flash::error('Products not found');

            return redirect(route('products.index'));
        }

        $types = Type::pluck('name', 'id');
        $brands = Brand::pluck('name', 'id');
        $services = Services::pluck('name', 'id');

        return view('products.edit')
            ->with('products', $products)
            ->with('types', $types)
            ->with('brands', $brands)
            ->with('services', $services);
    }

    /**
     * Update the specified Products in storage.
     *
     * @param  int              $id
     * @param UpdateProductsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductsRequest $request)
    {
        // $products = $this->productsRepository->findWithoutFail($id);

        $products = DB::table("products")->where("id", "=", $id)->first();

        if (empty($products)) {
            Flash::error('Products not found');

            return redirect(route('products.index'));
        }

        // $products = $this->productsRepository->update($request->all(), $id);
        DB::table('products')->where('id', $id)->update([
            'name'          => $request->name,
            'duration'      => $request->duration,
            'type_id'       => $request->type_id,
            'brand_id'      => $request->brand_id,
            'price'         => $request->price,
            'qty'           => $request->qty,
            'odo_reading'   => $request->odo_reading,
            'service_id'    => $request->service_id
        ]);

        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();
        DB::table('price_history')->insert([
            'product_id'  => $id,
            'price'       => $request->price,
            'created_at'    => $today,
            'updated_at'    => $today
        ]);

        DB::table('qty_history')->insert([
            'product_id'    => $id,
            'qty'           => $request->qty,
            'adjusted'      => $request->qty,
            'created_at'    => $today,
            'updated_at'    => $today
        ]);

        Flash::success('Product: ' . $request->name . ' has been updated successfully.');

        return redirect('/products/' . $products->id);
    }

    /**
     * Remove the specified Products from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // $products = $this->productsRepository->findWithoutFail($id);
        $products = DB::table("products")->where("id", "=", $id)->first();

        if (empty($products)) {
            Flash::error('Products not found');

            return redirect(route('products.index'));
        }

        // $this->productsRepository->delete($id);
        DB::table('products')->where("id", "=", $id)->delete();

        Flash::success('Products deleted successfully.');

        return redirect(route('products.index'));
    }
    
    public function productOrderIndex()
    {     
        $product_orders = DB::table('supplier_orders as so')
          ->select('so.id as order_id', 'so.supplier_id as supplier_id',
              'so.order_date as order_date', 'so.is_cash as is_cash',
              'so.is_paid as is_paid', 'so.grand_total as grand_total', 's.name as name')
          ->leftJoin('suppliers as s', 'so.supplier_id', '=', 's.id')
          ->where('so.is_paid', '=', 0)
          ->orderBy('so.order_date')
          ->get();

        foreach($product_orders as $key => $product_order)
        {
            $total_paid = $this->productOrderPaymentPaid($product_order->order_id);
            $product_orders[$key]->paid = $total_paid;
            $product_orders[$key]->balance = $product_order->grand_total - $total_paid;
        }

        return view('products.product_order_index')->with('product_orders', $product_orders);
    }
    
    public function productOrderIndexPaid()
    {
       $product_orders = DB::table('supplier_orders as so')
          ->select('so.id as order_id', 'so.supplier_id as supplier_id',
              'so.order_date as order_date', 'so.is_cash as is_cash',
              'so.is_paid as is_paid', 'so.grand_total as grand_total', 's.name as name')
          ->leftJoin('suppliers as s', 'so.supplier_id', '=', 's.id')
          ->where('so.is_paid', '=', 1)
          ->orderBy('so.order_date')
          ->get();

        foreach($product_orders as $key => $product_order)
        {
            $total_paid = $this->productOrderPaymentPaid($product_order->order_id);
            $product_orders[$key]->paid = $total_paid;
        }

        return view('products.product_order_index_paid')->with('product_orders', $product_orders);
    }
    
    public function productOrderShow($id)
    {

        $product_order = DB::table('supplier_orders as so')
            ->select('so.id as order_id', 'so.supplier_id as supplier_id',
                'so.order_date as order_date', 'so.is_cash as is_cash',
                'so.is_paid as is_paid', 'so.grand_total as grand_total', 'so.charge_invoice', 's.name as name')
            ->leftJoin('suppliers as s', 'so.supplier_id', '=', 's.id')
            ->orderBy('so.order_date')
            ->where('so.id', '=', $id)
            ->first();
        $product_order->paid = $this->productOrderPaymentPaid($id);
        $product_order->balance = $product_order->grand_total - $product_order->paid;


        $order_id = $product_order->order_id;
        $product_order_details = DB::table('supplier_order_details as sod')
            ->select('p.id as product_id', 'p.name as name', 'sod.price as price', 'sod.qty as qty',
                'sod.sub_total as sub_total')
            ->leftJoin('products as p', 'sod.product_id', '=', 'p.id')
            ->where('sod.order_id', '=', $order_id)
            ->get();

        
        foreach($product_order_details as $product_order_detail)
        {
            $recieved_qty = $this->productOrderProductRecieved($order_id, $product_order_detail->product_id);
            $product_order_detail->received_qty = $recieved_qty;
            $product_order_detail->unreceived_qty = $product_order_detail->qty - $recieved_qty;
        }

       $delivery_logs = DB::table('supplier_order_deliveries as sod')
          ->select('sod.id', 'sod.order_id as order_id', 'sod.received_date as received_date', 'sod.received_qty as received_qty',
              'sod.received_by as received_by', 'p.name as product_name')
          ->leftJoin('products as p', 'sod.product_id', '=', 'p.id')
          ->where('order_id', '=', $order_id)
          ->orderBy('sod.received_date', 'desc')
          ->get();
          
        $payment_logs = DB::table('supplier_order_payments')
            ->where('order_id', '=', $order_id)
            ->get();

        return view('products.product_order_show')
            ->with('product_order_details', $product_order_details)        
            ->with('product_order', $product_order)
            ->with('delivery_logs', $delivery_logs)
            ->with('payment_logs', $payment_logs);
    }

    public function productOrderAdd()
    {
        $suppliers = DB::table('suppliers')
            ->select('id', 'name')
            ->orderBy('name')
            ->get();

        $products = DB::table('products')
            ->select('id', 'name')
            ->orderBy('name')
            ->get();

        return view('products.product_order_add')
            ->with('products', $products)
            ->with('suppliers', $suppliers);
    }
    
    public function productOrderInsert()
    {

        // Insert product order info to database

        $product_order_id = DB::table('supplier_orders')->insertGetId(array(
            'supplier_id' => Input::get('supplier'), 
            'order_date' => Input::get('date'),
            'is_cash' => Input::get('order_type'),
            'grand_total' => Input::get('grand_total'),
            'charge_invoice' => Input::get('charge_invoice')
        )); 

        // Insert product order details

        $product_order_details = array();
        $product_ids = Input::get('product_ids');
        $product_qty = Input::get('qtys');
        $product_price = Input::get('prices');
        $product_subtotal = Input::get('subtotals');
        $row = 0;

        foreach($product_ids as $product_id)
        {
            if (($product_price [$row] > 0) && ($product_subtotal > 0)) 
            {
                $product_order_details[] = array(
                    'order_id' => $product_order_id,
                    'product_id' => $product_id,
                    'qty' => $product_qty[$row],
                    'price' => $product_price[$row],
                    'sub_total' => $product_subtotal[$row],
                );
            }

            $row++;
        }

        DB::table('supplier_order_details')->insert($product_order_details);
        return redirect('product-orders/order/' . $product_order_id);     
        //return redirect('product-orders/add-delivery/' . $product_order_id . '?mode=step');          

    }

    public function productOrderEdit($id)
    {
        $suppliers = DB::table('suppliers')
            ->select('id', 'name')
            ->get();

        $products = DB::table('products')
            ->select('id', 'name')
            ->get();
      
        $product_order = DB::table('supplier_orders')
            ->where('id', '=', $id)
            ->first();
          
        $product_order_details = DB::table('supplier_order_details as sod')
            ->select('p.id as product_id', 'p.name as  name', 'sod.qty as qty', 'sod.price as price', 'sod.sub_total as sub_total')
            ->leftJoin('products as p', 'p.id', '=', 'sod.product_id')
            ->where('sod.order_id', '=', $id)
            ->get();

        return view('products.product_order_edit')
            ->with('order_id', $id)
            ->with('products', $products)
            ->with('suppliers', $suppliers)
            ->with('product_order', $product_order)
            ->with('product_order_details', $product_order_details);
    }
    
    public function productOrderUpdate($id)
    {
        // Update supplier order
        DB::table('supplier_orders')
            ->where('id', $id)
            ->update(array(
                'supplier_id' => Input::get('supplier'), 
                'order_date' => Input::get('date'),
                'is_cash' => Input::get('order_type'),
                'grand_total' => Input::get('grand_total'),
                'charge_invoice' => Input::get('charge_invoice')
            ));
            
        // Remove exisiting products
        DB::table('supplier_order_details')->where('order_id', '=', $id)->delete();

        // Insert product order details
        $product_order_details = array();
        $product_ids = Input::get('product_ids');
        $product_qty = Input::get('qtys');
        $product_price = Input::get('prices');
        $product_subtotal = Input::get('subtotals');
        $row = 0;

        foreach($product_ids as $product_id)
        {
            if (($product_price [$row] > 0) && ($product_subtotal > 0)) 
            {
                $product_order_details[] = array(
                    'order_id' => $id,
                    'product_id' => $product_id,
                    'qty' => $product_qty[$row],
                    'price' => $product_price[$row],
                    'sub_total' => $product_subtotal[$row],
                );
            }

            $row++;
        }

        DB::table('supplier_order_details')->insert($product_order_details);

        Session::flash('product-order-update-message', 'Product order successfully updated!'); 

        return redirect('product-orders/order/' . $id);
    }
    
    public function productOrderDelete($id)
    {
        DB::table('supplier_orders')->where('id', '=', $id)->delete();
        DB::table('supplier_order_details')->where('order_id', '=', $id)->delete();
        DB::table('supplier_order_deliveries')->where('order_id', '=', $id)->delete();
        DB::table('supplier_order_payments')->where('order_id', '=', $id)->delete();
        
        Session::flash('product-order-delete-message', 'Product order successfully deleted!'); 
        
        return redirect('product-orders');
    }
    
    public function productOrderDeliveryAdd($id)
    {
      $product_order_details = DB::table('supplier_order_details as sod')
          ->select('p.name as name', 'sod.order_id', 'sod.price as price', 'sod.qty as qty',
              'sod.sub_total as sub_total', 'sod.product_id as product_id')
          ->leftJoin('products as p', 'sod.product_id', '=', 'p.id')
          ->where('sod.order_id', '=', $id)
          ->get();

        $parameters = (!empty(Input::get('mode'))) ? '?mode=' . Input::get('mode') : '';
          
        return view('products.product_order_delivery_add')
            ->with('order_id', $id)
            ->with('parameters', $parameters)
            ->with('product_order_details', $product_order_details);
    }
    
    public function productOrderDeliveryInsert($id)
    {
        $supplier_order_deliveries = array();
        $received_date = Input::get('delivery_date');
        $received_by = Input::get('received_by');        
        $products = Input::get('product');
        $delivered = Input::get('delivered');
        $row = 0;

        foreach($products as $product)
        {
            if ($delivered [$row] > 0) 
            {

                $supplier_order_deliveries[] = array(
                    'order_id' => $id,
                    'product_id' => $products[$row],
                    'received_qty' => $delivered[$row],
                    'received_by' => $received_by,
                    'received_date' => $received_date
                );

                $this->incrementProductQty($products[$row], $delivered[$row], $received_date);
            }

            $row++;
        }

        DB::table('supplier_order_deliveries')->insert($supplier_order_deliveries);

        Session::flash('product-delivery-add-message', 'Product delivery successfully added!'); 

        if (!empty(Input::get('mode')) && Input::get('mode') == 'step')
        {
            return redirect('product-orders/add-payment/' . $id . '?mode=step');
        }
        else
        {
            return redirect('product-orders/order/' . $id);          
        }
    }
    
    public function productOrderDeliveryEdit($id)
    {
        $product_delivery = DB::table('supplier_order_deliveries as sod')
            ->select('sod.id as id', 'sod.order_id', 'p.id as product_id', 'p.name as product_name',
                'sod.received_by as received_by', 'sod.received_date as received_date',
                'sod.received_qty as received_qty')
            ->leftJoin('products as p', 'p.id', '=', 'sod.product_id')
            ->where('sod.id', '=', $id)
            ->first();

        return view('products.product_order_delivery_edit')
            ->with('product_delivery', $product_delivery);
    }
    
    public function productOrderDeliveryUpdate($id)
    {

        $previous_delivery_record = DB::table('supplier_order_deliveries')
        ->select('received_qty', 'product_id', 'received_date')
        ->where('id', $id)
        ->first();

        if (Input::get('delivered') != $previous_delivery_record->received_qty)
        {

            // Check delivery product update if added or  deducted
            if (Input::get('delivered') > $previous_delivery_record->received_qty)
            {

                $updated_product_qty = Input::get('delivered') - $previous_delivery_record->received_qty;
                $this->incrementProductQty($previous_delivery_record->product_id, $updated_product_qty, $$previous_delivery_record->received_date);
            }
            else 
            {

                $updated_product_qty = $previous_delivery_record->received_qty - Input::get('delivered');
                $this->decrementProductQty($previous_delivery_record->product_id, $updated_product_qty, $previous_delivery_record->received_date);
            }
            
            // Update delivery record
            DB::table('supplier_order_deliveries')
                ->where('id', $id)
                ->update(array(
                    'received_date' => Input::get('delivery_date'),
                    'received_by' => Input::get('received_by'),
                    'received_qty' => Input::get('delivered'),
                ));

        }

        $supplier_order_deliveries = DB::table('supplier_order_deliveries')
            ->where('id', $id)
            ->first();

        Session::flash('product-delivery-update-message', 'Product delivery successfully updated!'); 

        return redirect('product-orders/order/' . $supplier_order_deliveries->order_id);
    }

    public function productOrderProductRecieved($order_id, $product_id)
    {
        $productReceived = 0;
        $productOrders = DB::table('supplier_order_deliveries')
            ->where('order_id', '=', $order_id)
            ->where('product_id', '=', $product_id)
            ->get();

        foreach($productOrders as $productOrder)
        {
              $productReceived += $productOrder->received_qty;
        }

        return $productReceived;
    }

    public function productOrderPaymentAdd($id)
    {

        return view('products.product_order_payment_add')
            ->with('order_id', $id);
    }
    
    public function productOrderPaymentInsert($id)
    {
        $payment[] = array(
            'order_id' => $id,
            'payment_date' => Input::get('payment_date'),
            'payment_amount' => Input::get('payment_amount'),
        );

        DB::table('supplier_order_payments')->insert($payment);

        // Update product order paid status
        $this->UpdateProductOrderPaidStatus($id);

        Session::flash('product-payment-add-message', 'Product order payment successfully added!'); 

        return redirect('product-orders/order/' . $id);          
    }

    public function productOrderPaymentEdit($id)
    {
        $payment_detail = DB::table('supplier_order_payments')
            ->where('id', '=', $id)
            ->first();

        return view('products.product_order_payment_edit')
            ->with('payment_detail', $payment_detail);
    }

    
    public function productOrderPaymentUpdate($id)
    {
        DB::table('supplier_order_payments')
            ->where('id', '=', $id)
            ->update(array(
                'payment_date' => Input::get('payment_date'),
                'payment_amount' => Input::get('payment_amount'),
            ));

        $supplier_order_payment = DB::table('supplier_order_payments')
            ->where('id', '=', $id)
            ->first();

        // Update product order paid status
        $this->UpdateProductOrderPaidStatus($supplier_order_payment->order_id);

        Session::flash('product-payment-update-message', 'Product order payment successfully updated!'); 

        return redirect('product-orders/order/' . $supplier_order_payment->order_id);
    }
    
    public function UpdateProductOrderPaidStatus($id)
    {
        $product_order = DB::table('supplier_orders')
            ->where('id', '=', $id)
            ->first();
        
        $total_paid = $this->productOrderPaymentPaid($id);
        $total_amount = $product_order->grand_total;
        
        $paid_status = ($total_amount == $total_paid) ? 1 : 0;

        DB::table('supplier_orders')
            ->where('id', '=', $id)
            ->update(array(
                'is_paid' => $paid_status
            ));
    }
    
    public function productOrderPaymentPaid($order_id)
    {
        $productPaymentBalance = 0;
        $productOrdersPayments = DB::table('supplier_order_payments')
            ->where('order_id', '=', $order_id)
            ->get();

        foreach($productOrdersPayments as $productOrdersPayment)
        {
              $productPaymentBalance += $productOrdersPayment->payment_amount;
        }

        return $productPaymentBalance;
    }
    
    public function incrementProductQty($id, $qty, $received_date)
    {

        DB::table('products')
            ->where('id', '=', $id)
            ->increment('qty', $qty);

        $updated_product = DB::table('products')
            ->select('qty')
            ->where('id', '=', $id)
            ->first();
        
        $this->qtyHistoryLog($id, $updated_product->qty, $qty, 'add', $received_date);
    }
    
    public function decrementProductQty($id, $qty, $received_date)
    {

        DB::table('products')
            ->where('id', '=', $id)
            ->decrement('qty', $qty);

        $updated_product = DB::table('products')
            ->select('qty')
            ->where('id', '=', $id)
            ->first();

        $this->qtyHistoryLog($id, $updated_product->qty, $qty, 'minus', $received_date);
    }
    
    public function qtyHistoryLog($id, $qty, $adjusted_qty, $action, $received_date )
    {

        if ($action == 'add')
        {
            $note = 'Product added';
        }
        else 
        {
            $note = 'Product deducted';
        }

        if (is_null($received_date)) {
            $received_date = Carbon::now();
        }

        DB::table('qty_history')->insert([
            'product_id' => $id,
            'qty' => $qty,
            'adjusted' => $adjusted_qty,
            'note' => $note,
            'created_at' => $received_date
          ]);
    }

    public function productOrderIndexIncomplete()
    {
        $product_order_details = DB::table('supplier_order_details as sod')
            ->select('p.id as product_id', 'p.name as name', 'sod.price as price', 'sod.qty as qty',
                'sod.sub_total as sub_total', 'sod.order_id as order_id', 's.name as supplier_name',
                'so.order_date as order_date')
            ->leftJoin('products as p', 'sod.product_id', '=', 'p.id')
            ->leftJoin('supplier_orders as so', 'so.id', '=', 'sod.order_id')
            ->leftJoin('suppliers as s', 's.id', '=', 'so.supplier_id')
            ->get();

        $incomplete_orders = array();

        foreach($product_order_details as $product_order_detail)
        {
            $recieved_qty = $this->productOrderProductRecieved($product_order_detail->order_id, $product_order_detail->product_id);
            $product_order_detail->received_qty = $recieved_qty;
            $product_order_detail->unreceived_qty = $product_order_detail->qty - $recieved_qty;

            if ($product_order_detail->unreceived_qty > 0 )
            {
                $incomplete_orders[] = $product_order_detail;
            }
        }

        return view('products.product_order_index_incomplete')->with('incomplate_product_orders', $incomplete_orders);
    }



    public function ajaxadd(Request $request)
    {
        $xx = DB::table('products')->insertGetId([
            'name' => $request->name, 
            'duration' => $request->duration, 
            'type_id' => '1',  //defautl value?
            'brand_id' => $request->brand, 
            'price' => (isset($request->price) ? $request->price : 0), 
            'qty' => $request->qty, 
            'service_id' => $request->service, 
        ]);

        $this->createProductServicesJson();

        return $xx;

    }

    private function createProductServicesJson()
    {
        $serviceProds = DB::table('products')
                          ->leftJoin("services", "services.id", "products.service_id")
                          ->select("products.id", "products.name", "products.service_id", "services.name as servicename", "products.price", "products.qty")
                          ->where("products.qty", ">", 0)
                          ->orderBy('products.name')
                          ->get();

        file_put_contents('json/serviceProds.json', json_encode($serviceProds->toArray()));

    }
}
