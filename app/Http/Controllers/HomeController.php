<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/transactions');
    }

    public function logout() {
        Auth::logout();

        return view('/transactions');
    }


    /**
     * AJAX CALL TO GET BASE URL FROM CUSTOMJS
     *
     * @return String $url
     */
    public function getBaseURL(){
        return url('/');
    }
}
