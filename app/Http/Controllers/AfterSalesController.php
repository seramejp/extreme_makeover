<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAfterSalesRequest;
use App\Http\Requests\UpdateAfterSalesRequest;
use App\Repositories\AfterSalesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class AfterSalesController extends AppBaseController
{
    /** @var  AfterSalesRepository */
    private $afterSalesRepository;

    public function __construct(AfterSalesRepository $afterSalesRepo)
    {
        $this->afterSalesRepository = $afterSalesRepo;
    }

    /**
     * Display a listing of the AfterSales.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $afterSales = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
                'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company',
                'v.make as vehicle_name', 'p.name as personnel_name', 'd.plate as detail_plate',
                'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.is_done', '=', '1')
            ->where('t.is_asf', '=', '0')
            ->groupBy('t.odo_reading')
            ->get();

        return view('after_sales.index')
            ->with('afterSales', $afterSales);
    }

    /**
     * Show the form for creating a new AfterSales.
     *
     * @return Response
     */
    public function create()
    {
        return view('after_sales.create');
    }

    /**
     * Store a newly created AfterSales in storage.
     *
     * @param CreateAfterSalesRequest $request
     *
     * @return Response
     */
    public function store(CreateAfterSalesRequest $request)
    {
        $input = $request->all();

        $afterSales = $this->afterSalesRepository->create($input);

        Flash::success('After Sales saved successfully.');

        return redirect(route('afterSales.index'));
    }

    /**
     * Display the specified AfterSales.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $afterSales = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
                'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company', 'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate', 'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.id', '=', $id)
            ->first();

        if (empty($afterSales)) {
            Flash::error('After Sales not found');

            return redirect(route('afterSales.index'));
        }

        return view('after_sales.show')->with('afterSales', $afterSales);
    }

    /**
     * Show the form for editing the specified AfterSales.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $afterSales = $this->afterSalesRepository->findWithoutFail($id);

        if (empty($afterSales)) {
            Flash::error('After Sales not found');

            return redirect(route('afterSales.index'));
        }

        return view('after_sales.edit')->with('afterSales', $afterSales);
    }

    /**
     * Update the specified AfterSales in storage.
     *
     * @param  int              $id
     * @param UpdateAfterSalesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAfterSalesRequest $request)
    {
        $afterSales = $this->afterSalesRepository->findWithoutFail($id);

        if (empty($afterSales)) {
            Flash::error('After Sales not found');

            return redirect(route('afterSales.index'));
        }

        $afterSales = $this->afterSalesRepository->update($request->all(), $id);

        Flash::success('After Sales updated successfully.');

        return redirect(route('afterSales.index'));
    }

    /**
     * Remove the specified AfterSales from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $afterSales = $this->afterSalesRepository->findWithoutFail($id);

        if (empty($afterSales)) {
            Flash::error('After Sales not found');

            return redirect(route('afterSales.index'));
        }

        $this->afterSalesRepository->delete($id);

        Flash::success('After Sales deleted successfully.');

        return redirect(route('afterSales.index'));
    }


    
}
