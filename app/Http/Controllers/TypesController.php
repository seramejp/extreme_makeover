<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTypesRequest;
use App\Http\Requests\UpdateTypesRequest;
use App\Repositories\TypesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class TypesController extends AppBaseController
{
    /** @var  TypesRepository */
    private $typesRepository;

    public function __construct(TypesRepository $typesRepo)
    {
        $this->typesRepository = $typesRepo;
    }

    /**
     * Display a listing of the Types.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $types = DB::table('types')->get();

        return view('types.index')
            ->with('types', $types);
    }

    /**
     * Show the form for creating a new Types.
     *
     * @return Response
     */
    public function create()
    {
        return view('types.create');
    }

    /**
     * Store a newly created Types in storage.
     *
     * @param CreateTypesRequest $request
     *
     * @return Response
     */
    public function store(CreateTypesRequest $request)
    {
        $input = $request->all();

        $types = $this->typesRepository->create($input);

        Flash::success('Types saved successfully.');

        return redirect(route('types.index'));
    }

    /**
     * Display the specified Types.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $types = DB::table('types')->where("id", "=", $id)->first();

        if (empty($types)) {
            Flash::error('Types not found');

            return redirect(route('types.index'));
        }

        return view('types.show')->with('types', $types);
    }

    /**
     * Show the form for editing the specified Types.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $types = DB::table("types")->where("id", "=", $id)->first();

        if (empty($types)) {
            Flash::error('Types not found');

            return redirect(route('types.index'));
        }

        return view('types.edit')->with('types', $types);
    }

    /**
     * Update the specified Types in storage.
     *
     * @param  int              $id
     * @param UpdateTypesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypesRequest $request)
    {
        $types = DB::table("types")->where("id", "=", $id)->first();

        if (empty($types)) {
            Flash::error('Types not found');

            return redirect(route('types.index'));
        }

        DB::table('types')->where('id', $id)->update([
            'name'  => $request->name,
        ]);

        Flash::success('Types updated successfully.');

        return redirect(route('types.index'));
    }

    /**
     * Remove the specified Types from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $types = DB::table("types")->where("id", "=", $id)->first();

        if (empty($types)) {
            Flash::error('Types not found');

            return redirect(route('types.index'));
        }

        DB::table('types')->where("id", "=", $id)->delete();

        Flash::success('Types deleted successfully.');

        return redirect(route('types.index'));
    }
}
