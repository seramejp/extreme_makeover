<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomersRequest;
use App\Http\Requests\UpdateCustomersRequest;
use App\Models\Customers;
use App\Models\Transactions;
use App\Repositories\CustomersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class CustomersController extends AppBaseController
{
    /** @var  CustomersRepository */
    private $customersRepository;

    public function __construct(CustomersRepository $customersRepo)
    {
        $this->customersRepository = $customersRepo;
    }

    /**
     * Display a listing of the Customers.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->customersRepository->pushCriteria(new RequestCriteria($request));
        //$customers = $this->customersRepository->all();

        $customers = DB::table('customers')->groupBy('name')->get();

        return view('customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customers.
     *
     * @return Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created Customers in storage.
     *
     * @param CreateCustomersRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomersRequest $request)
    {
    
        $input = $request->all();
        $input['birthday'] = ($input['birthday'] == NULL) ? '0000-00-00 00:00:00' : $input['birthday'];
        $input['company'] = ($input['company'] == NULL) ? '' : $input['company'];
        $input['address'] = ($input['address'] == NULL) ? '' : $input['address'];

        $customers = $this->customersRepository->create($input);
        Flash::success('Customers saved successfully.');

        //return redirect(route('customers.index'));
        return redirect('/customers/'.$customers->id);
    }

    /**
     * Display the specified Customers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        // $customers = $this->customersRepository->findWithoutFail($id);

        // customer info
        $customers = DB::table("customers")->where("id", "=", $id)->first();

        // customer vehicles
        $vehicles = DB::table('customer_vehicles as cv')
            ->select('cv.id', 'v.make', 'd.plate', 'd.color', 'v.created_at')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.customer_id', '=', $id)
            ->get();

        // customer transactions
        $transactions = DB::table("transactions as t")
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at', 'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate', 'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.customer_id', '=', $id)->get();

        $productsAvailed = DB::table('transactions as t')
            ->select("t.id", "p.name", "tp.qty")
            ->leftJoin("transaction_products as tp", "t.id", "=", "tp.transaction_id")
            ->leftJoin("products as p", "tp.product_id", "=", "p.id")
            ->where("t.customer_id", "=", $id)->get();


        if (empty($customers)) {
            Flash::error('Customers not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customers', $customers)->with('vehicles', $vehicles)->with('transactions', $transactions);
    }





    public function showAjax($transid){
        $productsAvailed = DB::table('transaction_products as tp')
            ->select("tp.transaction_id", "p.name", "tp.qty")
            ->leftJoin("products as p", "tp.product_id", "=", "p.id")
            ->where("tp.transaction_id", "=", $transid)->get();

        $servicesAvailed = DB::table('transaction_services as ts')
            ->select("ts.transaction_id", "s.name", "ts.qty", "ts.unit")
            ->leftJoin("services as s", "s.id", "ts.service_id")
            ->where("ts.transaction_id", "=", $transid)->get();

        $payments = DB::table("payment_history as p")
            ->select("p.id", "p.amount", "p.payment_date")
            ->where('p.transaction_id', $transid)->get();

        $totalPaid = DB::table("payment_history as p")
            ->join("transactions as t", "t.id", "p.transaction_id")
            ->select(DB::raw("sum(p.amount) as amount"))
            ->where('t.id', $transid)
            ->where("t.total", ">", "sum(p.amount)")
            ->groupBy('t.id')
            ->first();

        $ptable = "";
        $stable = "";
        $paytable = "";

        if (count($productsAvailed) > 0) {
            foreach ($productsAvailed as $prod) {
                $ptable .= "<tr><td>$prod->name</td><td>$prod->qty</td></tr>";
            }
        }else{
            $ptable .= "<tr><td>No Data in table.</td></tr>";
        }

        if (count($servicesAvailed)) {
            foreach ($servicesAvailed as $serv) {
                $stable .= "<tr><td>$serv->name</td><td>$serv->qty</td><td>$serv->unit</td></tr>";
            }
        }else{
            $stable .=  "<tr><td>No Data in table.</td></tr>";
        }

        if (count($payments)) {
            foreach ($payments as $pay) {
                $amount = number_format($pay->amount,2);
                $paytable .= "<tr><td>$pay->payment_date</td><td>$amount</td></tr>";
            }
        }else{
            $paytable .=  "<tr><td>No Data in table.</td></tr>";
        }

        $total = 0;
        if (!empty($totalPaid)) {
            $total = $totalPaid->amount;
        }

        return response()->json([
            'products' => $ptable,
            'services' => $stable,
            'payments' => $paytable,
            'totalpaid' => number_format($total,2)
        ]);
    }




    /**
     * Show the form for editing the specified Customers.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $customers = $this->customersRepository->findWithoutFail($id);
        $customers = DB::table("customers")->where("id", "=", $id)->first();

        if (empty($customers)) {
            Flash::error('Customers not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')->with('customers', $customers);
    }

    /**
     * Update the specified Customers in storage.
     *
     * @param  int              $id
     * @param UpdateCustomersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomersRequest $request)
    {   
        //$customers = $this->customersRepository->findWithoutFail($id);
        $customers = DB::table("customers")->where("id", "=", $id)->first();

        if (empty($customers)) {
            Flash::error('Customers not found');

            return redirect(route('customers.index'));
        }

        //$isCompany = $request->is_company != "" ? 1 : 0;
        $isCompany = $request->is_company;

        // $customers = $this->customersRepository->update($request->all(), $id);
        DB::table('customers')->where('id', $id)->update([
            'name' => $request->name,
            'contact' => $request->contact,
            'company' => $request->company,
            'is_company' => $isCompany,
            'birthday' => $request->birthday,
            'address' => $request->address,
        ]);

        Flash::success('Customers updated successfully.');

        return redirect('/customers/' . $customers->id);
    }

    /**
     * Remove the specified Customers from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // $customers = $this->customersRepository->findWithoutFail($id);
        $customers = DB::table("customers")->where("id", "=", $id)->first();

        if (empty($customers)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        // $this->customersRepository->delete($id);
        DB::table('customers')->where("id", "=", $id)->delete();

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }



    /**
     * Store a newly created Customers in storage.
     *
     * 
     *
     * 
     */
    public function ajaxadd(Request $request)
    {
    
        $request->dob = ($request->dob == NULL) ? '0000-00-00 00:00:00' : $request->dob;
        $company = $request->name;
        $request->addr = ($request->addr == NULL) ? '' : $request->addr;

        $customer = new Customers();
        $customer->name = $request->name;
        $customer->contact = $request->contact;
        $customer->company = $company;
        $customer->address = $request->addr;
        $customer->birthday = $request->dob;
        $customer->is_company = $request->iscompany;
        $customer->save();

        $vehicleId = DB::table('vehicles')->insertGetId([
            'make' => $request->make, 
        ]);

        $detailId = DB::table('details')->insertGetId([
            'plate' => $request->plate, 
            'color' => $request->color, 
            'vehicle_id' => $vehicleId, 
         ]);

        $customerId = DB::table('customer_vehicles')->insertGetId([
            'customer_id'   => $customer->id,
            'vehicle_id'    => $vehicleId,
            'detail_id'     => $detailId, 
        ]);

        return $customer->id;

    }
}
