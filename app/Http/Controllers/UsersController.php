<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
use DB;

class UsersController extends Controller
{
     /**
     * UserController constructor
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $userList = DB::table('users as u')
        ->leftJoin('role_user as ru', 'ru.user_id', '=', 'u.id')
        ->leftJoin('roles as r','ru.role_id','=','r.id')
        ->select('u.id', 'u.name', 'u.email',  'r.display_name')->get();

        return view('users.index')->with('userList', $userList);
    }
    
    public function edit($user_id) {
        $userInfo = DB::table('users as u')
        ->leftJoin('role_user as ru', 'ru.user_id', '=', 'u.id')
        ->leftJoin('roles as r','ru.role_id','=','r.id')
        ->select('u.id', 'u.email',  'r.name')
        ->where('u.id', '=', $user_id)->first();

        $roles = Role::pluck('display_name', 'id');

        return view('users.editForm')->with('userinfo', $userInfo)->with('back', url()->previous())->with('roles', $roles);
    }

    public function update($user_id) {
        $user = User::find($user_id);
        $user->email = Input::get('email');

        if (!empty(Input::get('password'))) {
            $new_password = bcrypt(Input::get('password'));
            $user->password = $new_password;
        }

        $user->save();

        // Save role assigned
        //dd($user->roles);
        $user->detachRoles($user->roles);
        $role_id = Input::get('role_id');
        $user->attachRole($role_id);
        
        \Session::flash('flash_message', 'User: ' . $user->name . ' has been updated successfully!');

//        return  redirect()->action('UsersController@edit', ['user_id' => $user_id]);
        return redirect()->action('UsersController@index');
    }

    public function account() {
        return view('users.account')->with('back', url()->previous());
    }

    public function accountUpdate($user_id) {
        $user = User::find($user_id);
        $user->name = Input::get('name');
        $user->email = Input::get('email');

        if (!empty(Input::get('password'))) {
            $new_password = bcrypt(Input::get('password'));
            $user->password = $new_password;
        }

        $user->save();

        \Session::flash('flash_message', 'Your account has been updated successfully!');

        return  redirect()->action('UsersController@account');
    }

}

