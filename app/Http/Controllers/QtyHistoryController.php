<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateQtyHistoryRequest;
use App\Http\Requests\UpdateQtyHistoryRequest;
use App\Repositories\QtyHistoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class QtyHistoryController extends AppBaseController
{
    /** @var  QtyHistoryRepository */
    private $qtyHistoryRepository;

    public function __construct(QtyHistoryRepository $qtyHistoryRepo)
    {
        $this->qtyHistoryRepository = $qtyHistoryRepo;
    }

    /**
     * Display a listing of the QtyHistory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $qtyHistories = DB::table('qty_history as q')
            ->select('p.name', 'q.qty', 'q.adjusted', 'q.created_at', 'q.note')
            ->leftJoin('products as p', 'p.id', '=', 'q.product_id')
            ->where("q.adjusted", ">", 0)
            ->orderBy('q.id', 'desc')
            ->get();

        return view('qty_histories.index')
            ->with('qtyHistories', $qtyHistories);
    }

    /**
     * Show the form for creating a new QtyHistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('qty_histories.create');
    }

    /**
     * Store a newly created QtyHistory in storage.
     *
     * @param CreateQtyHistoryRequest $request
     *
     * @return Response
     */
    public function store(CreateQtyHistoryRequest $request)
    {
        $input = $request->all();

        $qtyHistory = $this->qtyHistoryRepository->create($input);

        Flash::success('Qty History saved successfully.');

        return redirect(route('qtyHistories.index'));
    }

    /**
     * Display the specified QtyHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $qtyHistory = $this->qtyHistoryRepository->findWithoutFail($id);

        if (empty($qtyHistory)) {
            Flash::error('Qty History not found');

            return redirect(route('qtyHistories.index'));
        }

        return view('qty_histories.show')->with('qtyHistory', $qtyHistory);
    }

    /**
     * Show the form for editing the specified QtyHistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $qtyHistory = $this->qtyHistoryRepository->findWithoutFail($id);

        if (empty($qtyHistory)) {
            Flash::error('Qty History not found');

            return redirect(route('qtyHistories.index'));
        }

        return view('qty_histories.edit')->with('qtyHistory', $qtyHistory);
    }

    /**
     * Update the specified QtyHistory in storage.
     *
     * @param  int              $id
     * @param UpdateQtyHistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQtyHistoryRequest $request)
    {
        $qtyHistory = $this->qtyHistoryRepository->findWithoutFail($id);

        if (empty($qtyHistory)) {
            Flash::error('Qty History not found');

            return redirect(route('qtyHistories.index'));
        }

        $qtyHistory = $this->qtyHistoryRepository->update($request->all(), $id);

        Flash::success('Qty History updated successfully.');

        return redirect(route('qtyHistories.index'));
    }

    /**
     * Remove the specified QtyHistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $qtyHistory = $this->qtyHistoryRepository->findWithoutFail($id);

        if (empty($qtyHistory)) {
            Flash::error('Qty History not found');

            return redirect(route('qtyHistories.index'));
        }

        $this->qtyHistoryRepository->delete($id);

        Flash::success('Qty History deleted successfully.');

        return redirect(route('qtyHistories.index'));
    }
}
