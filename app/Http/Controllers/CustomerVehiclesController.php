<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerVehiclesRequest;
use App\Http\Requests\UpdateCustomerVehiclesRequest;
use App\Repositories\CustomerVehiclesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class CustomerVehiclesController extends AppBaseController
{
    /** @var  CustomerVehiclesRepository */
    private $customerVehiclesRepository;

    public function __construct(CustomerVehiclesRepository $customerVehiclesRepo)
    {
        $this->customerVehiclesRepository = $customerVehiclesRepo;
    }

    /**
     * Display a listing of the CustomerVehicles.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->customerVehiclesRepository->pushCriteria(new RequestCriteria($request));
        $customerVehicles = $this->customerVehiclesRepository->all();

        return view('customer_vehicles.index')
            ->with('customerVehicles', $customerVehicles);
    }

    /**
     * Show the form for creating a new CustomerVehicles.
     *
     * @return Response
     */
    public function create($id)
    {
        return view('customer_vehicles.create')->with('customer_id', $id);
    }

    /**
     * Store a newly created CustomerVehicles in storage.
     *
     * @param CreateCustomerVehiclesRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerVehiclesRequest $request)
    {

        $input = $request->all();

        $backUrl = $input['customer_url'];
        $make = $input['make'];
        $plate = $input['plate'];
        $color = $input['color'];
        $customerId = $input['customer_id'];

        // update vehicle
        $vehicleId = DB::table('vehicles')->insertGetId(['make' => $make]);
        $detailId = DB::table('details')->insertGetId(['plate' => $plate, 'color' => $color, 'vehicle_id' => $vehicleId]);

        $customerId = DB::table('customer_vehicles')->insertGetId([
            'customer_id'   => $customerId,
            'vehicle_id'    => $vehicleId,
            'detail_id'     => $detailId
        ]);

        Flash::success('Customer vehicle updated successfully.');

        return redirect($backUrl);
    }

    /**
     * Display the specified CustomerVehicles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerVehicles = $this->customerVehiclesRepository->findWithoutFail($id);

        if (empty($customerVehicles)) {
            Flash::error('Customer Vehicles not found');

            return redirect(route('customers.index'));
        }

        return view('customer_vehicles.show')->with('customerVehicles', $customerVehicles);
    }

    /**
     * Show the form for editing the specified CustomerVehicles.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
//        $customerVehicles = $this->customerVehiclesRepository->findWithoutFail($id);

        $customerVehicles = DB::table('customer_vehicles as cv')
            ->select('cv.id as id', 'cv.vehicle_id as vehicle_id', 'cv.customer_id as customer_id', 'cv.detail_id as detail_id', 'v.make as make', 'd.plate as plate', 'd.color as color')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.id', '=', $id)->first();

//        $customerVehicles = DB::table('customer_vehicles')->where("id", "=", $id)->first();

        if (empty($customerVehicles)) {
            Flash::error('Customer Vehicles not found');

            return redirect(route('customers.index'));
        }



        return view('customer_vehicles.edit')->with('customerVehicles', $customerVehicles);
    }

    /**
     * Update the specified CustomerVehicles in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerVehiclesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerVehiclesRequest $request)
    {
        $input = $request->all();
        $backUrl = $input['customer_url'];
        $vehicleId = $input["vehicle_id"];
        $make = $input['make'];
        $detailId = $input['detail_id'];
        $plate = $input['plate'];
        $color = $input['color'];

        // update vehicle
        $update_vehicleId = 0;
        if ($vehicleId > 0)
        {
            DB::table('vehicles')->where('id', $vehicleId)->update(['make' => $make]);
            $update_vehicleId = $vehicleId;
        }
        else
        {
            $vehicleId = DB::table('vehicles')->insertGetId(['make' => $make]);
            $update_vehicleId = $vehicleId;
        }

        $update_detailId = 0;
        if ($detailId > 0)
        {
            DB::table('details')->where('id', $detailId)->update(['plate' => $plate, 'color' => $color]);
            $update_detailId = $detailId;
        }
        else
        {
            $detailId = DB::table('details')->insertGetId(['plate' => $plate, 'color' => $color]);
            $update_detailId = $detailId;
        }

        DB::table('customer_vehicles')->where('id', $id)->update(['vehicle_id' => $update_vehicleId, 'detail_id' => $update_detailId]);

        Flash::success('Customer vehicle updated successfully.');

        return redirect($backUrl);
    }

    /**
     * Remove the specified CustomerVehicles from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerVehicles = DB::table("customer_vehicles")->where("id", "=", $id)->first();

        if (empty($customerVehicles)) {
            Flash::error('Customer Vehicles not found');

            return redirect(route('customers.index'));
        }

        // $this->customersRepository->delete($id);
        DB::table('customer_vehicles')->where("id", "=", $id)->delete();

        Flash::success('Customer Vehicles deleted successfully.');

        return redirect(route('customers.show', [$customerVehicles->customer_id]));
    }
}
