<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCorporatesRequest;
use App\Http\Requests\UpdateCorporatesRequest;
use App\Repositories\CorporatesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class CorporatesController extends AppBaseController
{
    /** @var  CorporatesRepository */
    private $corporatesRepository;

    public function __construct(CorporatesRepository $corporatesRepo)
    {
        $this->corporatesRepository = $corporatesRepo;
    }

    /**
     * Display a listing of the Corporates.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->corporatesRepository->pushCriteria(new RequestCriteria($request));
        $corporates = DB::table('customers as c')
            ->select('c.id', 'v.make', 'd.plate', 'd.color', 'c.company', 'c.contact', 'c.address')
            ->leftJoin('customer_vehicles as cv', 'c.id', '=', 'cv.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('c.is_company', '1')
            ->orderBy('c.company')
            ->get();

        return view('corporates.index')
            ->with('corporates', $corporates);
    }

    /**
     * Show the form for creating a new Corporates.
     *
     * @return Response
     */
    public function create()
    {
        return view('corporates.create');
    }

    /**
     * Store a newly created Corporates in storage.
     *
     * @param CreateCorporatesRequest $request
     *
     * @return Response
     */
    public function store(CreateCorporatesRequest $request)
    {
        $input = $request->all();

        $corporates = $this->corporatesRepository->create($input);

        Flash::success('Corporates saved successfully.');

        return redirect(route('corporates.index'));
    }

    /**
     * Display the specified Corporates.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $corporates = DB::table('customers')->where('id', '=', $id)->first();

        $vehicles = DB::table('customer_vehicles as cv')
            ->select('v.id', 'v.make', 'd.plate', 'd.color', 'v.created_at')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.customer_id', '=', $id)
            ->get();

        $transactions = DB::table("transactions as t")
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
              'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate', 'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.customer_id', '=', $id)->get();
        
        if (empty($corporates)) {
            Flash::error('Corporates not found');

            return redirect(route('corporates.index'));
        }

        return view('corporates.show')->with('corporates', $corporates)->with('vehicles', $vehicles)->with('transactions', $transactions);
    }

    /**
     * Show the form for editing the specified Corporates.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $corporates = DB::table('customers')->where('id', '=', $id)->first();

        if (empty($corporates)) {
            Flash::error('Corporates not found');

            return redirect(route('corporates.index'));
        }

        return view('corporates.edit')->with('corporates', $corporates);
    }

    /**
     * Update the specified Corporates in storage.
     *
     * @param  int              $id
     * @param UpdateCorporatesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCorporatesRequest $request)
    {
        $corporates = DB::table('customers')->where('id', '=', $id)->first();

        if (empty($corporates)) {
            Flash::error('Corporates not found');

            return redirect(route('corporates.index'));
        }

        DB::table('customers')->where('id', $id)->update([
            'name' => $request->name,
            'contact' => $request->contact,
            'company' => $request->company,
            'is_company' => 1,
            'address' => $request->address,
        ]);

        Flash::success('Customer updated successfully.');

        return redirect(route('corporates.index'));
    }

    /**
     * Remove the specified Corporates from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $corporates = DB::table("customers")->where("id", "=", $id)->first();

        if (empty($corporates)) {
            Flash::error('Corporates not found');

            return redirect(route('corporates.index'));
        }

        DB::table('customers')->where("id", "=", $id)->delete();

        Flash::success('Corporates deleted successfully.');

        return redirect(route('corporates.index'));
    }
}
