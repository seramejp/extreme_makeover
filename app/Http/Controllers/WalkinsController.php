<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWalkinsRequest;
use App\Http\Requests\UpdateWalkinsRequest;
use App\Repositories\WalkinsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;

class WalkinsController extends AppBaseController
{
    /** @var  WalkinsRepository */
    private $walkinsRepository;

    public function __construct(WalkinsRepository $walkinsRepo)
    {
        $this->walkinsRepository = $walkinsRepo;
    }

    /**
     * Display a listing of the Walkins.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->walkinsRepository->pushCriteria(new RequestCriteria($request));
        $walkins = DB::table('customers as c')
            ->select('c.id', 'v.make', 'd.plate', 'd.color', 'c.name', 'c.contact', 'c.birthday')
            ->leftJoin('customer_vehicles as cv', 'c.id', '=', 'cv.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('c.is_company', '0')
            ->orderBy('c.name')
            ->get();

        return view('walkins.index')
            ->with('walkins', $walkins);
    }

    /**
     * Show the form for creating a new Walkins.
     *
     * @return Response
     */
    public function create()
    {
        return view('walkins.create');
    }

    /**
     * Store a newly created Walkins in storage.
     *
     * @param CreateWalkinsRequest $request
     *
     * @return Response
     */
    public function store(CreateWalkinsRequest $request)
    {
        $input = $request->all();

        $walkins = $this->walkinsRepository->create($input);

        Flash::success('Walkins saved successfully.');

        return redirect(route('walkins.index'));
    }

    /**
     * Display the specified Walkins.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $walkins = DB::table('customers')->where('id', '=', $id)->first();

        $vehicles = DB::table('customer_vehicles as cv')
            ->select('v.id', 'v.make', 'd.plate', 'd.color', 'v.created_at')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.customer_id', '=', $id)
            ->get();

        $transactions = DB::table("transactions as t")
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at',
              'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate', 'd.color as detail_color')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->where('t.customer_id', '=', $id)->get();
        
        if (empty($walkins)) {
            Flash::error('Walkins not found');

            return redirect(route('walkins.index'));
        }

        return view('walkins.show')->with('walkins', $walkins)->with('vehicles', $vehicles)->with('transactions', $transactions);
    }

    /**
     * Show the form for editing the specified Walkins.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $walkins = DB::table('customers')->where('id', '=', $id)->first();

        if (empty($walkins)) {
            Flash::error('Walkins not found');

            return redirect(route('walkins.index'));
        }

        return view('walkins.edit')->with('walkins', $walkins);
    }

    /**
     * Update the specified Walkins in storage.
     *
     * @param  int              $id
     * @param UpdateWalkinsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWalkinsRequest $request)
    {
        $walkins = DB::table('customers')->where('id', '=', $id)->first();

        if (empty($walkins)) {
            Flash::error('Walkins not found');

            return redirect(route('walkins.index'));
        }

        DB::table('customers')->where('id', $id)->update([
            'name'          => $request->name,
            'contact'       => $request->contact,
            'company'       => $request->company,
            'is_company'    => 0,
            'birthday' => $request->birthday,
        ]);

        Flash::success('Customer updated successfully.');

        return redirect(route('walkins.index'));
    }

    /**
     * Remove the specified Walkins from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $walkins = DB::table("customers")->where("id", "=", $id)->first();

        if (empty($walkins)) {
            Flash::error('Walkins not found');

            return redirect(route('walkins.index'));
        }

        DB::table('customers')->where("id", "=", $id)->delete();

        Flash::success('Walkins deleted successfully.');

        return redirect(route('walkins.index'));
    }
}
