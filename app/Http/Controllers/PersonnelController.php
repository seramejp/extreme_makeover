<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePersonnelRequest;
use App\Http\Requests\UpdatePersonnelRequest;
use App\Repositories\PersonnelRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class PersonnelController extends AppBaseController
{
    /** @var  PersonnelRepository */
    private $personnelRepository;

    public function __construct(PersonnelRepository $personnelRepo)
    {
        $this->personnelRepository = $personnelRepo;
    }

    /**
     * Display a listing of the Personnel.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->personnelRepository->pushCriteria(new RequestCriteria($request));
        $personnels = $this->personnelRepository->all();

        return view('personnels.index')
            ->with('personnels', $personnels);
    }

    /**
     * Show the form for creating a new Personnel.
     *
     * @return Response
     */
    public function create()
    {
        return view('personnels.create');
    }

    /**
     * Store a newly created Personnel in storage.
     *
     * @param CreatePersonnelRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonnelRequest $request)
    {
        $input = $request->all();

        $personnel = $this->personnelRepository->create($input);

        Flash::success('Personnel saved successfully.');

        return redirect(route('personnels.index'));
    }

    /**
     * Display the specified Personnel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $personnel = $this->personnelRepository->findWithoutFail($id);

        if (empty($personnel)) {
            Flash::error('Personnel not found');

            return redirect(route('personnels.index'));
        }

        return view('personnels.show')->with('personnel', $personnel);
    }

    /**
     * Show the form for editing the specified Personnel.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $personnel = $this->personnelRepository->findWithoutFail($id);

        if (empty($personnel)) {
            Flash::error('Personnel not found');

            return redirect(route('personnels.index'));
        }

        return view('personnels.edit')->with('personnel', $personnel);
    }

    /**
     * Update the specified Personnel in storage.
     *
     * @param  int              $id
     * @param UpdatePersonnelRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonnelRequest $request)
    {
        $personnel = $this->personnelRepository->findWithoutFail($id);

        if (empty($personnel)) {
            Flash::error('Personnel not found');

            return redirect(route('personnels.index'));
        }

        $personnel = $this->personnelRepository->update($request->all(), $id);

        Flash::success('Personnel updated successfully.');

        return redirect(route('personnels.index'));
    }

    /**
     * Remove the specified Personnel from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $personnel = $this->personnelRepository->findWithoutFail($id);

        if (empty($personnel)) {
            Flash::error('Personnel not found');

            return redirect(route('personnels.index'));
        }

        $this->personnelRepository->delete($id);

        Flash::success('Personnel deleted successfully.');

        return redirect(route('personnels.index'));
    }



    /**
     * Store a newly created Customers in storage.
     *
     * 
     *
     * 
     */
    public function ajaxadd(Request $request)
    {
        $xx = DB::table('personnels')->insertGetId([
            'name' => $request->name, 
        ]);

        return $xx;

    }
}
