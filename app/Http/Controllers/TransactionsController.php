<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransactionsRequest;
use App\Http\Requests\UpdateTransactionsRequest;
use App\Repositories\TransactionsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;



use App\Models\Services; // JP 1
use App\Models\Customers; // JP 2
use App\Models\Products; // JP 2
use App\Models\Payment;
use App\Transaction; // JP 1



class TransactionsController extends AppBaseController
{
    /** @var  TransactionsRepository */
    private $transactionsRepository;

    public function __construct(TransactionsRepository $transactionsRepo)
    {
        $this->transactionsRepository = $transactionsRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Transactions.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $transactions = DB::table('transactions as t')
            ->select('t.id', 't.date', 't.odo_reading', 't.subtotal', 't.km_run', 't.is_done', 't.created_at', 't.charge_invoice', 'c.name as customer_name', 'c.contact as customer_contact', 'c.is_company',
                'v.make as vehicle_name', 'p.name as product_name', 'd.plate as detail_plate',
                'd.color as detail_color', 'p.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('customer_vehicles as cv', 'cv.detail_id', '=', 't.detail_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('personnels as p', 'p.id', '=', 't.personnel_id')
            ->where('t.is_quote', 0)
            ->orderBy('t.date')
            ->get();

        return view('transactions.index')
            ->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new Transactions.
     *
     * @return Response
     */
    public function create()
    {
                                              // JP 
        $customers = $this->getAllCustomers();
        $products = $this->getAllProducts();
        $serviceProducts = $this->getAllProductsByServiceId();
        $productsObject = $this->getAllProducts(true);
        $personnels = $this->getAllPersonnel();

        $vehicles = array('0' => '---');


        $services = Services::select("id", "name")->get();
        $serviceProds = $this->createProductServicesJson();


        return view('transactions.create')
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('serviceProducts', $serviceProducts)
            ->with('personnels', $personnels)
            ->with('productsObject', $productsObject)
            ->with('services', $services)
            ->with('vehicles', $vehicles)
            ->with('serviceProds', $serviceProds)
            ->with('type', '');
    }

    /**
     * Store a newly created Transactions in storage.
     *
     * @param CreateTransactionsRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionsRequest $request)
    {
        $input = $request->all();
        
        // date
        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();

        // get detail id and vehicle id
        $detailId = $input['detail_id'];
        $vehicleIdClass = DB::table('details')->where('id', $detailId )->select('vehicle_id')->first(); //JP CHange
        $vehicleId = !is_null($vehicleIdClass) ? $vehicleIdClass->vehicle_id : 0; // JP Change
        
        // convert kmrun to int
        $kmRunInt = preg_replace("/[^0100-9]/","", $input['km_run']);
        $kmRunInt = intval($kmRunInt); // JP Change

        $odoReading = isset($input['odo_reading']) ? $input['odo_reading'] : 0;

        if (isset($input['is_quote'])) {
            $modelyear = $input['modelyear'];
            $transactionId = DB::table('transactions')->insertGetId([
                'date'              => $input['date'],
                'customer_id'       => $input['customer_id'],
                'vehicle_id'        => $vehicleId,
                'product_id'        => 0,
                'detail_id'         => $detailId,
                'modelyear'         => $modelyear,
                'subtotal'          => $input['subtotal'] ? $input['subtotal'] : 0,
                'discount'          => $input['discount'] ? $input['discount'] : 0,
                'total'             => $input['total'] ? $input['total'] : 0,
                'rendered'          => $input['rendered'] ? $input['rendered'] : 0,
                'balance'           => $input['balance'] ? $input['balance'] : 0,
                'km_run'            => $kmRunInt,
                'is_quote'          => $input['is_quote'] ? $input['is_quote'] : 0,
                'personnel_id'      => $input['personnel_id'],
                'charge_invoice'    => isset($input['charge_invoice']) ? $input['charge_invoice'] : '',
                'created_at'        => $today,
                'updated_at'        => $today
            ]);
        }else{
            // save transactions first
            $transactionId = DB::table('transactions')->insertGetId([
                'date'              => $input['date'],
                'customer_id'       => $input['customer_id'],
                'vehicle_id'        => $vehicleId,
                'product_id'        => 0,
                'detail_id'         => $detailId,
                'odo_reading'       => $odoReading,
                'subtotal'          => $input['subtotal'] ? $input['subtotal'] : 0,
                'discount'          => $input['discount'] ? $input['discount'] : 0,
                'total'             => $input['total'] ? $input['total'] : 0,
                'rendered'          => $input['rendered'] ? $input['rendered'] : 0,
                'balance'           => $input['balance'] ? $input['balance'] : 0,
                'km_run'            => $kmRunInt,
                'is_quote'          => $input['is_quote'] ? $input['is_quote'] : 0,
                'personnel_id'      => $input['personnel_id'],
                'charge_invoice'    => isset($input['charge_invoice']) ? $input['charge_invoice'] : '',
                'created_at'        => $today,
                'updated_at'        => $today
            ]);
        }
        

        // save products next
        $product_ids = $input["product_ids"];
        $qtys = $input["qtys"];
        $prices = $input["prices"];
        $subtotals = $input["subtotals"];
        $notes = $input["notes"];

        $lastProductId = 0;
        for($i = 0; $i < count($product_ids); $i++)
        {
            if ($product_ids[$i] > 0)
            {
                $productQty = $qtys[$i] ? $qtys[$i] : 0;
                $productId = $product_ids[$i];
                $lastProductId = $product_ids[$i];
                DB::table('transaction_products')->insertGetId([
                    'transaction_id'    => $transactionId,
                    'product_id'        => $productId,
                    'odo_reading'       => $odoReading,
                    'qty'               => $productQty,
                    'unit'              => 'pc',
                    'price'             => $prices[$i] ? $prices[$i] : 0,
                    'subtotal'          => $subtotals[$i] ? $subtotals[$i] : 0,
                    'notes'             => $notes[$i] ? $notes[$i] : '',
                    'created_at'        => $today,
                    'updated_at'        => $today
                ]);

                // deduct product qty
                $currentProductQty = DB::table('products')->where('id', $productId)->value('qty');
                $adjusted = $currentProductQty - $productQty;

                DB::table('qty_history')->insertGetId([
                    'product_id'    => $productId,
                    'qty'           => $adjusted,
                    'adjusted'      => $productQty,
                    'note'          => $productQty . " Items Bought",
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);

                DB::table('products')->where('id', $productId)->decrement('qty', $productQty);

            }
        }

        // save services next
        $service_ids = $input["service_ids"];
        $service_product_ids = $input["service_product_ids"];
        $service_details = $input["service_details"];
        $service_qtys = $input["service_qtys"];
        $service_units = $input["service_units"];
        $service_subtotals = $input["service_subtotals"];

        for ($i = 0; $i < count($service_ids); $i++)
        {
            if ($service_ids[$i] > 0)
            {
                DB::table('transaction_services')->insertGetId([
                    'transaction_id'    => $transactionId,
                    'service_id'        => $service_ids[$i],
                    'details'           => $service_details[$i] ? $service_details[$i] : '',
                    'product_id'        => $service_product_ids[$i],
                    'qty'               => $service_qtys[$i],
                    'unit'              => $service_units[$i],
                    'subtotal'          => $service_subtotals[$i] ? $service_subtotals[$i] : 0
                ]);
            }
        }

        // update transaction
        DB::table('transactions')
            ->where('id', $transactionId)
            ->update(['product_id' => $lastProductId, 'odo_reading' => $odoReading]);


        //STORE TO PAYMENT IF PAYMENT HAS BEEN MADE - NEW WALKIN
        if (is_null($input["is_quote"])) {
            if (intval($input['rendered']) > 0) {
                
                DB::table('payment_history')->insertGetId([
                    'transaction_id'=> $transactionId,
                    'customer_id'   => $input['customer_id'],
                    'amount'      => $input['rendered'],
                    'payment_date'    => $today,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }
        }

        Flash::success('Transactions saved successfully.');
//        return redirect(route('transactions.index'));
    
        return redirect('/transaction/transaction/'.$transactionId);

    }

    /**
     * Display the specified Transactions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transactions = $this->transactionsRepository->findWithoutFail($id);

        if (empty($transactions)) {
            Flash::error('Transactions not found');

            return redirect(route('transactions.index'));
        }

        return view('transactions.show')->with('transactions', $transactions);
    }

    /**
     * Show the form for editing the specified Transactions.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transactions = DB::table('transactions as t')
                    ->select("t.id", "t.date", "t.odo_reading", "t.km_run", "t.subtotal", "t.discount", "t.total", "t.rendered", "t.balance", "t.personnel_id", "t.charge_invoice", "c.id as customer_id", "t.detail_id", "c.name as customer_name", DB::raw('concat(v.make, " (", d.plate, ") ", d.color) as details'))
                    ->leftJoin("customers as c", "c.id", "t.customer_id")
                    ->leftJoin("vehicles as v", "v.id", "t.vehicle_id")
                    ->leftJoin("details as d", "d.id", "t.detail_id")
                    ->where("t.id", $id)
                    ->first();

        $totalPaid = DB::table("payment_history as p")
            ->join("transactions as t", "t.id", "p.transaction_id")
            ->select(DB::raw("sum(p.amount) as amount"))
            ->where('t.id', $id)
            ->where("t.total", ">", "sum(p.amount)")
            ->groupBy('t.id')
            ->first();

        $serviceProds = $this->createProductServicesJsonAll();
        $products = $this->getAllProducts(); //ALL Products
        $prods = $this->getAllProducts(true);
        $services = $this->getAllServices();
        $transactionProducts = $this->getTransactionProducts($transactions->id);
        $transactionServices = $this->getTransactionServices($transactions->id);
        $personnels = $this->getAllPersonnel(true);

        if (empty($transactions)) {
            Flash::error('Transactions not found');
            return redirect(route('transactions.index'));
        }

        //dd( $serviceProds->where("id", 158)->first()->service_id, $transactionProducts);
        
        return view('transactions.edit', compact('personnels'))->with('transactions', $transactions)
            ->with('products', $products)
            ->with('prods', $prods)
            ->with('transactionProducts', $transactionProducts)
            ->with('transactionServices', $transactionServices)
            ->with('services', $services)
            ->with('totalPaid', $totalPaid)
            ->with('serviceProds', $serviceProds);
    }

    /**
     * Update the specified Transactions in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionsRequest $request)
    {   
        //dd($request);
        $transactions = DB::table('transactions')->where("id", "=", $id)->first();

        if (empty($transactions)) {
            Flash::error('Transactions not found');

            return redirect(route('transactions.index'));
        }

        // date
        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();

        // get detail id and vehicle id
        $detailId = $request['detail_id'];
        $vehicleId = DB::table('details')->where('id', '=', $detailId )->value('vehicle_id');
        $vehicleId = $vehicleId ? $vehicleId : 0;

        // convert kmrun to int
        $kmRunInt = preg_replace("/[^0100-9]/","", $request['km_run']);
        $odoReading = $request['odo_reading'];

        // update transactions first
        $transactionId = DB::table('transactions')->where('id', $id)->update([
            'date'              => $request['date'],
            'customer_id'       => $request['customer_id'],
            'vehicle_id'        => $vehicleId,
            'detail_id'         => $detailId,
            'odo_reading'       => $odoReading,
            'subtotal'          => $request['subtotal'],
            'discount'          => $request['discount'],
            'km_run'            => $kmRunInt,
            'is_quote'          => $request['is_quote'] ? $request['is_quote'] : 0,
            'personnel_id'      => $request['personnel_id'],
            'charge_invoice'    => $request['charge_invoice']
        ]);

        // put back stocks for previous products
        $transaction_products = DB::table('transaction_products')->where('transaction_id', $id)->get();
        foreach ($transaction_products as $tp)
        {
            DB::table('products')->where('id', $tp->product_id)->increment('qty', $tp->qty);
        }

        // delete existing products and services to give room for new items
        DB::table('transaction_products')->where('transaction_id', $id)->delete();
        DB::table('transaction_services')->where('transaction_id', $id)->delete();

        // save products next
        $product_ids = $request["product_ids"];
        $qtys = $request["qtys"];
        $prices = $request["prices"];
        $subtotals = $request["subtotals"];
        $notes = $request["notes"];

        $lastProductId = 0;
        for($i = 0; $i < count($product_ids); $i++)
        {
            if ($product_ids[$i] > 0)
            {
                $productQty = $qtys[$i] ? $qtys[$i] : 0;
                $productId = $product_ids[$i];
                $lastProductId = $product_ids[$i];
                DB::table('transaction_products')->insertGetId([
                    'transaction_id'    => $id,
                    'product_id'        => $product_ids[$i],
                    'odo_reading'       => $odoReading,
                    'qty'               => $qtys[$i] ? $qtys[$i] : 0,
                    'unit'              => 'pc',
                    'price'             => $prices[$i] ? $prices[$i] : 0,
                    'subtotal'          => $subtotals[$i] ? $subtotals[$i] : 0,
                    'notes'             => $notes[$i] ? $notes[$i] : '',
                    'created_at'        => $today,
                    'updated_at'        => $today
                ]);

                // deduct product qty
                $currentProductQty = DB::table('products')->where('id', $productId)->value('qty');
                $adjusted = $currentProductQty - $productQty;

                $qhID = DB::table('qty_history')->insertGetId([
                    'product_id'    => $productId,
                    'qty'           => $productQty,
                    'adjusted'      => $adjusted,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);

                DB::table('products')->where('id', $productId)->decrement('qty', $productQty);
            }
        }

        // save services next
        $service_ids = $request["service_ids"];
        $service_product_ids = $request["service_product_ids"];
        $service_details = $request["service_details"];
        $service_qtys = $request["service_qtys"];
        $service_units = $request["service_units"];
        $service_subtotals = $request["service_subtotals"];

        for ($i = 0; $i < count($service_ids); $i++)
        {
            if ($service_ids[$i] > 0)
            {
                DB::table('transaction_services')->insertGetId([
                    'transaction_id'    => $id,
                    'service_id'        => $service_ids[$i],
                    'details'           => $service_details[$i] ? $service_details[$i] : '',
                    'product_id'        => $service_product_ids[$i],
                    'qty'               => $service_qtys[$i],
                    'subtotal'          => $service_subtotals[$i],
                    'unit'              => $service_units[$i]
                ]);
            }
        }

        // update transaction
        DB::table('transactions')
            ->where('id', $transactionId)
            ->update(['product_id' => $lastProductId, 'odo_reading' => $odoReading, 
                'subtotal'=> $request['subtotal'],
                'discount'=> $request['discount'],
                'total'=> $request['total'],
                'rendered'=>$request['rendered'],
                'balance'=>$request['balance']
                ]);


        Flash::success('Transactions updated successfully.');


        return redirect("/transaction/transaction/".$id);

    }

    /**
     * Remove the specified Transactions from storage.
     *1
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transactions = $this->transactionsRepository->findWithoutFail($id);

        if (empty($transactions)) {
            Flash::error('Transactions not found');

            return redirect(route('transactions.index'));
        }

        $this->transactionsRepository->delete($id);

        Flash::success('Transaction deleted successfully.');

        return redirect(route('transactions.index'));
    }


     /**
     * Remove the specified Transactions from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function delete($id)
    {
        $transaction = DB::table('transactions')->where("id", $id);

        if (is_null($transaction)) {
            Flash::error('Transaction not found');
        }else{
            $transaction->delete();
            Flash::success('Transaction deleted successfully.');
        }

        return redirect(route('transactions.index'));
    }
    
    /*
     * @description get all customers
     * @return $customers
     */
    private function getAllCustomers($isObject = false, $type = "")
    {
        $all_c = DB::table('customers')
            ->select('id', 'name')
            ->orderBy('name')
            ->get();

        if ($type == "walkin")
        {
            $all_c = DB::table('customers')
                       ->select('id', 'name')
                       ->where('is_company', 0)
                       ->orderBy('name')
                       ->get();
        }

        if ($type == "corporate")
        {
            $all_c = DB::table('customers')
                       ->select('id', 'name')
                       ->where('is_company', 1)
                       ->orderBy('name')
                       ->get();
        }

        $customers = array();
        $customers[''] = '---';
        foreach ($all_c as $customer)
        {
            $customers[$customer->id] = $customer->name;
        }

        if ($isObject)
        {
            return $all_c;
        }
        else
        {
            return $customers;
        }

    }
    
    /*
     * @description get all products
     */
    private function getAllProducts($isObject = false)
    {
        $all_p = DB::table('products')
            ->select('id', 'name', 'price', 'service_id')
            ->orderBy('name')
            ->get();
        /*->where('price', '>', '0')
        ->where('qty', '>', '0')
            ->groupBy('name')*/

        $products = array();
        $products[''] = '---';
        foreach ($all_p as $product)
        {
            $products[$product->id] = $product->name;
        }

        if ($isObject)
        {
            return $all_p;
        }
        else
        {
            return $products;
        }
    }

    /*
     * @description get all products by service_id
     */

    private function getAllProductsByServiceId($isObject = false)
    {
            $all_ps = DB::table('products')
                ->select('id', 'name', 'service_id')
                ->orderBy('name')
                ->get();
            /*->where('price', '>', '0')
            ->where('qty', '>', '0')
                ->groupBy('name')*/

            $serviceProducts = array();
            $serviceProducts[''] = '---';
            foreach ($all_ps as $product) {
                $serviceProducts[$product->id] = $product->name;
            }

            if ($isObject) {
                return $all_ps;
            } else {
                return $serviceProducts;
            }
    }
    
    /*
     * @description get all personnel names
     */
    private function getAllPersonnel($isObject = false)
    {
        // get all personnel
        $all_pe = DB::table('personnels')
            ->select('id', 'name')
            ->orderBy('name')
            ->get();

        $personnels = array();
        $personnels[''] = '---';
        foreach ($all_pe as $personnel)
        {
            $personnels[$personnel->id] = $personnel->name;
        }

        if ($isObject)
        {
            return $all_pe;
        }

        else
        {
            return $personnels;
        }
    }
    
    /*
     * @description get all transaction services
     */
    private function getAllServices($isObject = false)
    {
        $all_s = DB::table('services')
            ->select('id', 'name')
            ->orderBy('name')
            ->get();
        $services = array();
        $services[''] = '---';
        foreach ($all_s as $service)
        {
            $services[$service->id] = $service->name;
        }

        if ($isObject)
        {
            return $all_s;
        }
        else
        {
            return $services;
        }
    }


    private function getVehiclesByCustomer($id)
    {
        $all_v = DB::table('customer_vehicles as cv')
            ->select('d.id', 'v.make', 'd.plate', 'd.color')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.customer_id', '=', $id)
            ->get();

        $vehicles = array();
        $vehicles[''] = '---';
        foreach ($all_v as $vehicle)
        {
            $vehicles[$vehicle->id] = $vehicle->make . ' (' . $vehicle->plate . ', ' . ucwords(strtolower(($vehicle->color))) . ')' ;
        }

        return $vehicles;
    }

    private function getTransactionProducts($transactionId)
    {
        $products = DB::table("transaction_products as tp")
            ->where('tp.transaction_id', '=', $transactionId)
            ->orderBy('tp.id')
            ->get();

        return $products;
    }

    private function getTransactionServices($transactionId)
    {
        $services = DB::table('transaction_services as ts')
            ->select("ts.product_id", "p.name as product_name", "ts.service_id", "s.name as servicename", "ts.qty", "ts.subtotal", "ts.details", "ts.unit")
            ->join("services as s", "s.id", "ts.service_id")
            ->join("products as p", "p.id", "ts.product_id")
            ->where('ts.transaction_id', '=', $transactionId)
            ->get();
        return $services;
    }

    public function createWalkin()
    {
        $customers = $this->getAllCustomers(false, 'walkin');
        $products = $this->getAllProducts();
        $serviceProducts = $this->getAllProductsByServiceId();
        $productsObject = $this->getAllProducts(true);
        $personnels = $this->getAllPersonnel();
        $vehicles = array('0' => '---');
        $brands = DB::table('brands')->select('id', 'name')->orderBy('name')->get();

        $services = Services::select("id", "name")->get();
        $serviceProds = $this->createProductServicesJson();

        return view('transactions.create')
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('serviceProducts', $serviceProducts)
            ->with('personnels', $personnels)
            ->with('productsObject', $productsObject)
            ->with('services', $services)
            ->with('vehicles', $vehicles)
            ->with('brands', $brands)
            ->with('serviceProds', $serviceProds)
            ->with('type', '- Walk-in');
    }

    public function createCorporate()
    {
        $customers = $this->getAllCustomers(false, 'corporate');
        $products = $this->getAllProducts();
        $serviceProducts = $this->getAllProductsByServiceId();
        $productsObject = $this->getAllProducts(true);
        $personnels = $this->getAllPersonnel();
        $vehicles = array('0' => '---');
        $brands = DB::table('brands')->select('id', 'name')->orderBy('name')->get();

        $services = Services::select("id", "name")->get();
        $serviceProds = $this->createProductServicesJson();

        return view('transactions.create')
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('serviceProducts', $serviceProducts)
            ->with('personnels', $personnels)
            ->with('productsObject', $productsObject)
            ->with('services', $services)
            ->with('vehicles', $vehicles)
            ->with('brands', $brands)
            ->with('serviceProds', $serviceProds)
            ->with('type', '- Corporate');
    }

    public function createQuote()
    {
        $customers = $this->getAllCustomers();
        $products = $this->getAllProducts();
        $serviceProducts = $this->getAllProductsByServiceId();
        $productsObject = $this->getAllProducts(true);
        $personnels = $this->getAllPersonnel();
        $vehicles = array('0' => '---');

        $services = Services::select("id", "name")->get();
        $serviceProds = $this->createProductServicesJson();

        return view('transactions.create_quote')
            ->with('customers', $customers)
            ->with('products', $products)
            ->with('serviceProducts', $serviceProducts)
            ->with('personnels', $personnels)
            ->with('productsObject', $productsObject)
            ->with('services', $services)
            ->with('vehicles', $vehicles)
            ->with('serviceProds', $serviceProds)
            ->with('type', '- Quotation');
    }

    private function createProductServicesJson()
    {
        $serviceProds = DB::table('products')
                          ->leftJoin("services", "services.id", "products.service_id")
                          ->select("products.id", "products.name", "products.service_id", "services.name as servicename", "products.price", "products.qty")
                          ->where("products.qty", ">", 0)
                          ->orderBy('products.name')
                          ->get();

        file_put_contents('json/serviceProds.json', json_encode($serviceProds->toArray()));

        return $serviceProds;
    }

    private function createProductServicesJsonAll()
    {
        $serviceProds = DB::table('products')
                          ->leftJoin("services", "services.id", "products.service_id")
                          ->select("products.id", "products.name", "products.service_id", "services.name as servicename", "products.price")
                          ->orderBy('products.name')
                          ->get();

        file_put_contents('json/serviceProds.json', json_encode($serviceProds->toArray()));

        return $serviceProds;
    }


    /* --------------------------------------------------- 
    ------------------------------------------------------
    ------------------------------------------------------
        CHANGES MADE TO CODE                      
    ------------------------------------------------------
    ------------------------------------------------------
    --------------------------------------------------- */




    
}
