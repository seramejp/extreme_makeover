<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use App\Models\Products;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Session;
use App\Http\Controllers\ReportsController;

class TransactionController extends Controller
{
    protected $softDelete = false;

    /**
     * @description Serves as the transaction page's main view.
     * Should include at least the table to list all transactions
     * @return \Illu minate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $transactions = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->orderBy('t.id', 'desc')
            ->get();
        return view("transaction.index")->with('transactions', $transactions);
    }

    /**
     * @description The import process.
     * @param Request $request
     * @return Request
     */

    public function import(Request $request)
    {
        // file
        $import = $request->file('import');

        // import
        $filename = time() . "_" . $import->getClientOriginalName();
        $filepath = base_path() . '/public/files/';
        $import->move($filepath, $filename);
        $importCount = 0;

        // date
        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();

        // read csv
        $csv = $this->csvToArray($filepath, $filename);

        // import to database
        foreach ($csv as $key => $value)
        {
            $importCount++;

            $changeOilDate = isset($value[0]) ? $value[0] : "";
            $name = isset($value[1]) ? $value[1] : "";
            $birthday = isset($value[2]) ? $value[2] : "";
            $address = isset($value[3]) ? $value[3] : "";
            $contact = isset($value[4]) ? $value[4] : "";
            $vehicle = isset($value[5]) ? $value[5] : "";
            $color = isset($value[6]) ? $value[6] : "";
            $plate = isset($value[7]) ? $value[7] : "";
            $odo = isset($value[8]) ? $value[8] : "";
            $oiltype = isset($value[9]) ? $value[9] : "";
            $kmrun = isset($value[10]) ? $value[10] : "";
            $isCompany = isset($value[11]) ? $value[11] : "";
            $personnel = isset($value[12]) ? $value[12] : "";

            // services
            $underChassis = isset($value[13]) && $value[13] != ""   ? 4 : 0;
            $brakes = isset($value[14]) && $value[14] != ""         ? 5 : 0;
            $transmission = isset($value[15]) && $value[15] != ""   ? 6 : 0;
            $engine = isset($value[16]) && $value[16] != ""         ? 7 : 0;
            $radiator = isset($value[17]) && $value[17] != ""       ? 8 : 0;
            $electrical = isset($value[18]) && $value[18] != ""     ? 9 : 0;
            $aircon = isset($value[19]) && $value[19] != ""         ? 10 : 0;
            $tires = isset($value[20]) && $value[20] != ""          ? 11 : 0;
            $battery = isset($value[21]) && $value[21] != ""        ? 12 : 0;
            $accessories = isset($value[22]) && $value[22] != ""    ? 13 : 0;
            $others = isset($value[23]) && $value[23] != ""         ? 14 : 0;

            $transactionDate = $changeOilDate ? Carbon::createFromFormat('m/d/Y', $changeOilDate)->format('Y-m-d') : "";
            // date format is short e.g. 3/22/17
            if(strlen($changeOilDate) < 8)
            {
                $transactionDate = $changeOilDate ? Carbon::createFromFormat('m/d/y', $changeOilDate)->format('Y-m-d') : "";
            }

            $birthDate = $birthday ? Carbon::createFromFormat('m/d/Y', $birthday)->format('Y-m-d') : "";
            // date format is short e.g. 3/22/17
            if(strlen($birthDate) < 8)
            {
                $birthDate = $birthday ? Carbon::createFromFormat('m/d/y', $birthday)->format('Y-m-d') : "";
            }

            // I - CUSTOMER
            // create new customer if doesn't exist; get customer id if already exists.
            $name = str_replace('�','n', $name);
            $name = str_replace('\xF1a','n', $name);
            $customerId = 0;
            $customerId = DB::table('customers')->where('name', '=', $name)->value('id');
            if (!$customerId)
            {
                $customerId = DB::table('customers')->insertGetId([
                    'name'          => $name,
                    'contact'       => $contact,
                    'company'       => $name,
                    'is_company'    => $isCompany,
                    'birthday'      => $birthDate,
                    'address'       => $address,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

            // I - VEHICLE
            // create new vehicle information if doesn't exist; get vehicle information if already exists.
            $vehicleId = DB::table('vehicles')->where('make', 'like', $vehicle)->value('id');
            if (!$vehicleId)
            {
                $vehicleId = DB::table('vehicles')->insertGetId([
                    'make'          => $vehicle,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

            // I - DETAIL
            // create new detail information if doesn't exist; get detail information if already exists.
            $detailId = DB::table('details')->where('plate', 'like', $plate)->value('id');
            if (!$detailId)
            {
                $detailId = DB::table('details')->insertGetId([
                    'plate'         => $plate,
                    'color'         => $color,
                    'vehicle_id'    => $vehicleId,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

            // II - PRODUCT
            // create new product if doesn't exist; get product id if already exists.
            $productId = 0;
            $isProductExists = false;
            $product = DB::table('products')->where('name', 'like', $oiltype)->first();
            $productQty = 0;
            if ($product === null)
            {
                $productId = DB::table('products')->insertGetId([
                    'name'          => $oiltype,
                    'duration'      => $kmrun,
                    'type_id'       => 1,
                    'brand_id'      => 1,
                    'price'         => 0,
                    'qty'           => 0,
                    'odo_reading'   => $odo,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
                $productQty = 1;
            }
            else
            {
                $isProductExists = true;
                $productId = $product->id;
                $productQty = $product->qty;
            }

            // III - TRANSACTION
            // Check if personnel already exists.
            $personnelId = 0;
            $personnelName = isset($personnel) && $personnel !== null ? $personnel : null;

            if ($personnelName)
            {
                $personnelId = DB::table('personnels')->where('name', '=', $personnelName)->value('id');
                if (!$personnelId)
                {
                    $personnelId = DB::table('personnels')->insertGetId([
                        'name'         => $personnelName,
                        'created_at'    => $today,
                        'updated_at'    => $today
                    ]);
                }
            }

            // III - TRANSACTION
            // Check if transaction information is the same as the previous data. Get ID if no, create if yes.
            $transactionId = DB::table('transactions')
                ->where('date', '=', $transactionDate)
                ->where('customer_id', '=', $customerId)
                ->where('vehicle_id', '=', $vehicleId)
                ->where('detail_id', '=', $detailId)
                ->value('id');
            if (!$transactionId)
            {
                $kmRunInt = preg_replace("/[^0-9]/","", $kmrun);

                $transactionId = DB::table('transactions')->insertGetId([
                    'date'          => $transactionDate,
                    'customer_id'   => $customerId,
                    'vehicle_id'    => $vehicleId,
                    'product_id'    => $productId,
                    'detail_id'     => $detailId,
                    'odo_reading'   => $odo,
                    'subtotal'      => 0,
                    'km_run'        => $kmRunInt,
                    'personnel_id'  => $personnelId,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

            // III - TRANSACTION STATUS
            DB::table('transaction_statuses')->insertGetId([
                'transaction_id'    => $transactionId,
                'status'            => 'done',
                'created_at'        => $today,
                'updated_at'        => $today
            ]);

            // IV - TRANSACTION PRODUCTS
            // IV - CHECK IF TRANSACTION PRODUCTS ALREADY EXISTS
            $transactionProductExists = DB::table('transaction_products')
                ->where('transaction_id', '=', $transactionId)
                ->where('product_id', '=', $productId)
                ->value('id');
            if (!$transactionProductExists)
            {
                DB::table('transaction_products')->insertGetId([
                    'transaction_id'    => $transactionId,
                    'product_id'        => $productId,
                    'qty'               => 0,
                    'unit'              => 'liters',
                    'price'             => 0,
                    'subtotal'          => 0,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

            // IV - DEDUCT PRODUCT QTY
            if ($isProductExists)
            {
                if ($productQty < 0)
                {
                    DB::table('products')->where('id', $productId)->update(['qty' => 0]);

                    // IV - PRODUCT - QTY HISTORY
                    DB::table('qty_history')->insertGetId([
                        'product_id'    => $productId,
                        'qty'           => 0,
                        'adjusted'      => 0,
                        'created_at'    => $today,
                        'updated_at'    => $today
                    ]);
                }
                else
                {
                    $currentProductQty = DB::table('products')->where('id', $productId)->value('qty');
                    $adjusted = $currentProductQty - $productQty;

                    // IV - PRODUCT - QTY HISTORY
                    DB::table('qty_history')->insertGetId([
                        'product_id'    => $productId,
                        'qty'           => $productQty,
                        'adjusted'      => $adjusted,
                        'created_at'    => $today,
                        'updated_at'    => $today
                    ]);

                    DB::table('products')->where('id', $productId)->decrement('qty');
                }
            }

            // IV - PRODUCT - PRICE HISTORY
            DB::table('price_history')->insertGetId([
                'product_id'    => $productId,
                'price'         => 0,
                'created_at'    => $today,
                'updated_at'    => $today
            ]);

            // V - TRANSACTION SERVICES
            if ($underChassis > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $underChassis)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $underChassis,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($brakes > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $brakes)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $brakes,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($transmission > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $transmission)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $transmission,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($engine > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $engine)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $engine,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($radiator > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $radiator)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $radiator,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($electrical > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $electrical)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $electrical,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($aircon > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $aircon)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $aircon,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($tires > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $tires)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $tires,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($battery > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $battery)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $battery,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($accessories > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $accessories)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $accessories,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }
            if ($others > 0)
            {
                // V - CHECK IF TRANSACTION SERVICE ALREADY EXISTS
                $serviceExists = DB::table('transaction_services')
                    ->where('transaction_id', '=', $transactionId)
                    ->where('service_id', '=', $others)
                    ->where('product_id', '=', $productId)
                    ->value('id');

                if (!$serviceExists)
                {
                    DB::table('transaction_services')->insertGetId([
                        'transaction_id'    => $transactionId,
                        'service_id'        => $others,
                        'details'           => '',
                        'product_id'        => $productId,
                        'qty'               => 0,
                        'unit'              => 'liters'
                    ]);
                }
            }

            // vehicles - customer relationship
            $cv = DB::table('customer_vehicles')
                ->where('customer_id', '=', $customerId)
                ->where('vehicle_id', '=', $vehicleId)
                ->where('detail_id', '=', $detailId)
                ->value('id');

            if (!$cv)
            {
                DB::table('customer_vehicles')->insertGetId([
                    'customer_id'   => $customerId,
                    'vehicle_id'    => $vehicleId,
                    'detail_id'     => $detailId,
                    'created_at'    => $today,
                    'updated_at'    => $today
                ]);
            }

        }

        // redirect back to transaction page
        return redirect()->action("TransactionController@index")->with("status", "Successfully imported " . $importCount . " records.");
    }

    /**
     * @description convert csv file to array.
     * Commented section is for using headers as array keys.
     * @param $filepath
     * @param $filename
     * @return array
     */
    protected function csvToArray($filepath, $filename) {
        $csv = array_map('str_getcsv', file($filepath . $filename));
        /*array_walk($csv, function (&$array) use($csv) {
            $array = array_combine($csv[0], $array);
        });*/
        array_shift($csv);

        return $csv;
    }

    public function single($id, $type = 'walkin') {
        $transaction = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type', 'pl.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('personnels as pl', 'pl.id', '=', 't.personnel_id')
            ->where('t.id', '=', $id)->first();

        $products = DB::table("transaction_products as tp")
            ->select('tp.*', 'p.*', 'tp.qty as transaction_qty', 'tp.price as product_price')
            ->leftJoin('products as p', 'p.id', '=', 'tp.product_id')
            ->where('tp.transaction_id', '=', $id)
            ->get();

        $services = DB::table('transaction_services as ts')
            ->select('s.name as service', 'p.name as product', 'ts.details', 'ts.qty', 'ts.unit', 'ts.subtotal')
            ->leftJoin('services as s', 's.id', '=', 'ts.service_id')
            ->leftJoin('products as p', 'p.id', '=', 'ts.product_id')
            ->where('ts.transaction_id', '=', $id)
            ->get();

        $payments = DB::table("payment_history as p")
            ->select("p.id", "t.date as transaction_date", "c.name as customer_name", "p.amount", "p.payment_date", "p.transaction_id", "t.customer_id")
            ->leftJoin("transactions as t", "t.id", "p.transaction_id")
            ->leftJoin("customers as c", "c.id", "p.customer_id")
            ->where('transaction_id', $id)
            ->get();

        $totalPaid = DB::table("payment_history as p")
            ->join("transactions as t", "t.id", "p.transaction_id")
            ->select(DB::raw("sum(p.amount) as amount"))
            ->where('t.id', $id)
            ->where("t.total", ">", "sum(p.amount)")
            ->groupBy('t.id')
            ->first();
        //dd($totalPaid);

        if ($type == 'walkin') $type = 'walk-in';
        return view('transaction.single')
            ->with("transaction", $transaction)
            ->with("back", url()->previous())
            ->with("type", $type)
            ->with("products", $products)
            ->with('services', $services)
            ->with('payments', $payments)
            ->with('totalPaid', $totalPaid);
    }

    public function pendingItemMarkAsDone($id) {
        DB::table('transactions')->where('id', $id)->update(['is_done' => 1]);
        
        //Session::flash('notification-pending-to-done-message', 'Successfully marked transaction as Done!');

        return 0;
    }

    public function doneItemMarkAsAsf($id) {
        DB::table('transactions')->where('id', $id)->update(['is_asf' => 1]);
        
        //Session::flash('notification-pending-to-done-message', 'Successfully marked transaction as Done!');

        return 0;
    }

    public function markAsDone($id, $type) {
        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();

        DB::table('transactions')->where('id', $id)->update(['is_done' => 1]);
        DB::table('transaction_statuses')->insertGetId([
            'transaction_id' => $id,
            'status' => 'done',
            'created_at' => $today,
            'updated_at' => $today
        ]);

        if ($type == 'walk-in') 
        {
            $redirect_url = '/notifications/walk-in/pending';
        }
        else 
        {
            $redirect_url = '/notifications/corporate/pending';          
        }
        
        $notification = new ReportsController();
        $notification->updateNotificationRecord();
        
        Session::flash('notiification-mark-done-message', 'Successfully marked customer transaction as done!');

        return redirect($redirect_url);
    }

    public function markAsAsf($id, $type) {
        $dateClass = Carbon::now();
        $today = $dateClass->toDateTimeString();

        DB::table('transactions')->where('id', $id)->update(['is_done' => 1, 'is_asf' => 1]);
        DB::table('transaction_statuses')->insertGetId([
            'transaction_id'    => $id,
            'status' => 'asf',
            'created_at' => $today,
            'updated_at' => $today
        ]);

        if ($type == 'walk-in') 
        {
            $redirect_url = '/notifications/walk-in/done';
        }
        else 
        {
            $redirect_url = '/notifications/corporate/done';          
        }

        $notification = new ReportsController();
        $notification->updateNotificationRecord();
        
        Session::flash('notiification-mark-asf-message', 'Successfully moved customer transaction to ASF!');

        return redirect($redirect_url);
    }

    /**
     * @function getVehiclesByCustomer
     * @description Get vehicles by customer based on transaction table instead of customer_vehicles
     * @param $id
     * @return $this
     */
    public function getVehiclesByCustomer($id)
    {
        $vehicles = DB::table('customer_vehicles as cv')
            ->select('d.id', 'v.make', 'd.plate', 'd.color')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 'cv.detail_id')
            ->where('cv.customer_id', '=', $id)
            ->get();

        return view('transactions.vehicles')->with('vehicles', $vehicles);
    }

    public function printTransaction($id) {
        $transaction = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type', 'pl.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('personnels as pl', 'pl.id', '=', 't.personnel_id')
            ->where('t.id', '=', $id)->first();

        $products = DB::table("transaction_products as tp")
            ->select('tp.*', 'p.*', 'tp.qty as transaction_qty', 'tp.price as product_price')
            ->leftJoin('products as p', 'p.id', '=', 'tp.product_id')
            ->where('tp.transaction_id', '=', $id)
            ->get();

        $services = DB::table('transaction_services as ts')
                      ->select('s.name as service', 'p.name as product', 'ts.details', 'ts.qty', 'ts.unit', 'ts.subtotal')
            ->leftJoin('services as s', 's.id', '=', 'ts.service_id')
            ->leftJoin('products as p', 'p.id', '=', 'ts.product_id')
            ->where('ts.transaction_id', '=', $id)
            ->get();

        return view('transaction.print')
            ->with("transaction", $transaction)
            ->with("products", $products)
            ->with('services', $services);
    }

    public function printAfter($id) {
        $transaction = DB::table('transactions as t')
            ->select('t.*', 'c.*', 'v.*', 'd.*', 't.id as transaction_id', 'p.name as oil_type', 'pl.name as personnel_name')
            ->leftJoin('customers as c', 'c.id', '=', 't.customer_id')
            ->leftJoin('customer_vehicles as cv', 'cv.detail_id', '=', 't.detail_id')
            ->leftJoin('vehicles as v', 'v.id', '=', 'cv.vehicle_id')
            ->leftJoin('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('products as p', 'p.id', '=', 't.product_id')
            ->leftJoin('personnels as pl', 'pl.id', '=', 't.personnel_id')
            ->where('t.id', '=', $id)->first();

        $products = DB::table("transaction_products as tp")
            ->select('tp.*', 'p.*', 'tp.qty as transaction_qty', 'tp.price as product_price', 'tp.odo_reading as odo_reading')
            ->leftJoin('products as p', 'p.id', '=', 'tp.product_id')
            ->where('tp.transaction_id', '=', $id)
            ->get();

        $services = DB::table('transaction_services as ts')
            ->select('s.name as service', 'p.name as product', 'ts.details', 'ts.qty', 'ts.unit', 'ts.subtotal')
            ->leftJoin('services as s', 's.id', '=', 'ts.service_id')
            ->leftJoin('products as p', 'p.id', '=', 'ts.product_id')
            ->where('ts.transaction_id', '=', $id)
            ->get();

        return view('transaction.printAfter')
            ->with("transaction", $transaction)
            ->with("products", $products)
            ->with('services', $services);
    }
}
