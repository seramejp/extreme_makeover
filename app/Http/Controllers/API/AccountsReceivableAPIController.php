<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccountsReceivableAPIRequest;
use App\Http\Requests\API\UpdateAccountsReceivableAPIRequest;
use App\Models\AccountsReceivable;
use App\Repositories\AccountsReceivableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AccountsReceivableController
 * @package App\Http\Controllers\API
 */

class AccountsReceivableAPIController extends AppBaseController
{
    /** @var  AccountsReceivableRepository */
    private $accountsReceivableRepository;

    public function __construct(AccountsReceivableRepository $accountsReceivableRepo)
    {
        $this->accountsReceivableRepository = $accountsReceivableRepo;
    }

    /**
     * Display a listing of the AccountsReceivable.
     * GET|HEAD /accountsReceivables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->accountsReceivableRepository->pushCriteria(new RequestCriteria($request));
        $this->accountsReceivableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $accountsReceivables = $this->accountsReceivableRepository->all();

        return $this->sendResponse($accountsReceivables->toArray(), 'Accounts Receivables retrieved successfully');
    }

    /**
     * Store a newly created AccountsReceivable in storage.
     * POST /accountsReceivables
     *
     * @param CreateAccountsReceivableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAccountsReceivableAPIRequest $request)
    {
        $input = $request->all();

        $accountsReceivables = $this->accountsReceivableRepository->create($input);

        return $this->sendResponse($accountsReceivables->toArray(), 'Accounts Receivable saved successfully');
    }

    /**
     * Display the specified AccountsReceivable.
     * GET|HEAD /accountsReceivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AccountsReceivable $accountsReceivable */
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            return $this->sendError('Accounts Receivable not found');
        }

        return $this->sendResponse($accountsReceivable->toArray(), 'Accounts Receivable retrieved successfully');
    }

    /**
     * Update the specified AccountsReceivable in storage.
     * PUT/PATCH /accountsReceivables/{id}
     *
     * @param  int $id
     * @param UpdateAccountsReceivableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccountsReceivableAPIRequest $request)
    {
        $input = $request->all();

        /** @var AccountsReceivable $accountsReceivable */
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            return $this->sendError('Accounts Receivable not found');
        }

        $accountsReceivable = $this->accountsReceivableRepository->update($input, $id);

        return $this->sendResponse($accountsReceivable->toArray(), 'AccountsReceivable updated successfully');
    }

    /**
     * Remove the specified AccountsReceivable from storage.
     * DELETE /accountsReceivables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AccountsReceivable $accountsReceivable */
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            return $this->sendError('Accounts Receivable not found');
        }

        $accountsReceivable->delete();

        return $this->sendResponse($id, 'Accounts Receivable deleted successfully');
    }
}
