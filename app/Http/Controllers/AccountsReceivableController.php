<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAccountsReceivableRequest;
use App\Http\Requests\UpdateAccountsReceivableRequest;
use App\Repositories\AccountsReceivableRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;

class AccountsReceivableController extends AppBaseController
{
    /** @var  AccountsReceivableRepository */
    private $accountsReceivableRepository;

    public function __construct(AccountsReceivableRepository $accountsReceivableRepo)
    {
        $this->accountsReceivableRepository = $accountsReceivableRepo;
    }

    /**
     * Display a listing of the AccountsReceivable.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /*$this->accountsReceivableRepository->pushCriteria(new RequestCriteria($request));
        $accountsReceivables = $this->accountsReceivableRepository->all();*/

        $accountsReceivables = DB::table('transactions as t')
            ->select(DB::raw('t.date as transdate, c.name, v.make, d.color, d.plate, c.is_company as type, t.total, sum(ph.amount) as amount'))
            ->join('customers as c', 'c.id', '=', 't.customer_id')
            ->join('vehicles as v', 'v.id', '=', 't.vehicle_id')
            ->join('details as d', 'd.id', '=', 't.detail_id')
            ->leftJoin('payment_history as ph', 'ph.transaction_id', 't.id')
            ->where("t.total", ">", 0)
            /*->where("ph.amount", ">", 0)*/
            ->where("t.is_quote", 0)
            /*->where("t.total", ">", "sum(ph.amount)")*/
            ->groupBy('t.id')
            ->orderBy('ph.created_at')
            ->get();

        //dd($accountsReceivables);

        return view('accounts_receivables.index')
            ->with('accountsReceivables', $accountsReceivables);
    }

    /**
     * Show the form for creating a new AccountsReceivable.
     *
     * @return Response
     */
    public function create()
    {
        return view('accounts_receivables.create');
    }

    /**
     * Store a newly created AccountsReceivable in storage.
     *
     * @param CreateAccountsReceivableRequest $request
     *
     * @return Response
     */
    public function store(CreateAccountsReceivableRequest $request)
    {
        $input = $request->all();

        $accountsReceivable = $this->accountsReceivableRepository->create($input);

        Flash::success('Accounts Receivable saved successfully.');

        return redirect(route('accountsReceivables.index'));
    }

    /**
     * Display the specified AccountsReceivable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            Flash::error('Accounts Receivable not found');

            return redirect(route('accountsReceivables.index'));
        }

        return view('accounts_receivables.show')->with('accountsReceivable', $accountsReceivable);
    }

    /**
     * Show the form for editing the specified AccountsReceivable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            Flash::error('Accounts Receivable not found');

            return redirect(route('accountsReceivables.index'));
        }

        return view('accounts_receivables.edit')->with('accountsReceivable', $accountsReceivable);
    }

    /**
     * Update the specified AccountsReceivable in storage.
     *
     * @param  int              $id
     * @param UpdateAccountsReceivableRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccountsReceivableRequest $request)
    {
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            Flash::error('Accounts Receivable not found');

            return redirect(route('accountsReceivables.index'));
        }

        $accountsReceivable = $this->accountsReceivableRepository->update($request->all(), $id);

        Flash::success('Accounts Receivable updated successfully.');

        return redirect(route('accountsReceivables.index'));
    }

    /**
     * Remove the specified AccountsReceivable from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $accountsReceivable = $this->accountsReceivableRepository->findWithoutFail($id);

        if (empty($accountsReceivable)) {
            Flash::error('Accounts Receivable not found');

            return redirect(route('accountsReceivables.index'));
        }

        $this->accountsReceivableRepository->delete($id);

        Flash::success('Accounts Receivable deleted successfully.');

        return redirect(route('accountsReceivables.index'));
    }
}
