<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/users', 'UsersController@index');
Route::get('/users/edit/{user_id}', 'UsersController@edit');
Route::post('/user/update/{user_id}', 'UsersController@update')->name('updateUser');
Route::get('/account',  'UsersController@account');
Route::post('/account/update/{user_id}', 'UsersController@accountUpdate')->name('updateAccount');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'TransactionsController@index');
Route::get('/accountsReceivable', 'AccountsReceivableController@index');
Route::get('/transaction', 'TransactionController@index');
Route::get('/transaction/index', 'TransactionController@index');
Route::post('/transaction/import', 'TransactionController@import');
Route::get('/transaction/transaction/{id}', 'TransactionController@single');
Route::get('/transaction/transaction/{id}/{type}', 'TransactionController@single');
Route::get('/transaction/getVehiclesByCustomer/{id}', 'TransactionController@getVehiclesByCustomer');
Route::get('/transaction/update/{id}/done/{type}', 'TransactionController@markAsDone');
Route::get('/transaction/update/{id}/asf/{type}', 'TransactionController@markAsAsf');
Route::get('/transaction/create/walkin', 'TransactionsController@createWalkin');
Route::get('/transaction/create/corporate', 'TransactionsController@createCorporate');
Route::get('/transaction/create/quote', 'TransactionsController@createQuote');

Route::get('/transaction/markAsDone/{id}', 'TransactionController@pendingItemMarkAsDone');
Route::get('/transaction/markAsAsf/{id}', 'TransactionController@doneItemMarkAsAsf');

Route::get('/transaction/printTransaction/{id}', 'TransactionController@printTransaction');
Route::get('/transaction/printAfter/{id}', 'TransactionController@printAfter');

Route::get('/quotes/approve/{id}', 'QuotesController@approveQuote');
Route::get('/quotes/disapprove/{id}', 'QuotesController@disapproveQuote');

Route::get('/reports', 'ReportsController@index');
Route::get('/notifications', 'ReportsController@notifications');
Route::get('/notifications/{type}/{status}', 'ReportsController@notifications');
Route::get('/reports/notifications', 'ReportsController@reports_notifications');
Route::get('/reports/notifications/{type}', 'ReportsController@reports_notifications_redirect');
Route::get('/transaction/transaction/', 'ReportsController@index');
Route::get('/notifications/{type1}/{type2}', 'ReportsController@notifications_redirect');
Route::get('/reports/filter/', 'ReportsController@show_with_filter');

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder');
Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate');
Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate');

Route::get('/notifications/refresh-notification', 'ReportsController@refreshNotification');
Route::post('/refresh-notification-record', 'ReportsController@updateNotificationRecord');

Route::get('/product-orders', 'ProductsController@productOrderIndex');
Route::get('/product-orders/paid', 'ProductsController@productOrderIndexPaid');
Route::get('/product-orders/incomplete', 'ProductsController@productOrderIndexIncomplete');
Route::get('/product-orders/add', 'ProductsController@productOrderAdd');
Route::post('/product-orders/insert', 'ProductsController@productOrderInsert');
Route::get('/product-orders/order/{id}', 'ProductsController@productOrderShow');
Route::get('/product-orders/edit/order/{id}', 'ProductsController@productOrderEdit');
Route::post('/product-orders/update/order/{id}', 'ProductsController@productOrderUpdate');
Route::delete('/product-orders/delete/order/{id}', 'ProductsController@productOrderDelete');
Route::get('/product-orders/add-delivery/{id}', 'ProductsController@productOrderDeliveryAdd');
Route::post('/product-orders/add-delivery/{id}/insert', 'ProductsController@productOrderDeliveryInsert');
Route::get('/product-orders/edit-delivery/{id}', 'ProductsController@productOrderDeliveryEdit');
Route::post('/product-orders/edit-delivery/{id}/update', 'ProductsController@productOrderDeliveryUpdate');
Route::get('/product-orders/add-payment/{id}', 'ProductsController@productOrderPaymentAdd');
Route::post('/product-orders/add-payment/{id}/insert', 'ProductsController@productOrderPaymentInsert');
Route::get('/product-orders/edit-payment/{id}', 'ProductsController@productOrderPaymentEdit');
Route::post('/product-orders/edit-payment/{id}/update', 'ProductsController@productOrderPaymentUpdate');

Route::resource('customerVehicles', 'CustomerVehiclesController');
Route::get('/customerVehicles/create/{id}', 'CustomerVehiclesController@create');

Route::get('payment/createFromTransaction/{id}', 'PaymentController@createFromTransaction');

Route::get('/transactions/delete/{id}', 'TransactionsController@delete');
Route::delete('/transactions/delete/{id}', 'TransactionsController@delete');
Route::get('/customers/delete/{id}', 'CustomersController@destroy');
Route::get('/products/delete/{id}', 'ProductsController@destroy');


Route::post('/customers/ajax/add', 'CustomersController@ajaxadd');
Route::get('/customers/gettransactiondetails/{transid}', 'CustomersController@showAjax');
Route::post('/personnel/ajax/add', 'PersonnelController@ajaxadd');
Route::post('/product/ajax/add', 'ProductsController@ajaxadd');


Route::resource('customers', 'CustomersController');
Route::resource('products', 'ProductsController');
Route::resource('brands', 'BrandsController');
Route::resource('types', 'TypesController');
Route::resource('walkins', 'WalkinsController');
Route::resource('corporates', 'CorporatesController');
Route::resource('transactions', 'TransactionsController');
Route::resource('afterSales', 'AfterSalesController');
Route::resource('priceHistories', 'PriceHistoryController');
Route::resource('qtyHistories', 'QtyHistoryController');
Route::resource('accountsReceivable', 'AccountsReceivableController');
Route::resource('asfTransactions', 'AsfTransactionsController');
Route::resource('personnels', 'PersonnelController');
Route::resource('services', 'ServicesController');
Route::resource('suppliers', 'SupplierController');
Route::resource('customerVehicles', 'CustomerVehiclesController');
Route::resource('quotes', 'QuotesController');
Route::resource('payments', 'PaymentController');
Route::resource('roles', 'RolesController');
Route::resource('user', 'UserController');
Route::resource('accountsPayable', 'AccountsPayableController');

Route::get('/getBaseURL', 'HomeController@getBaseURL');

