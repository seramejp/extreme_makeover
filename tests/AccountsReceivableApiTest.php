<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountsReceivableApiTest extends TestCase
{
    use MakeAccountsReceivableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAccountsReceivable()
    {
        $accountsReceivable = $this->fakeAccountsReceivableData();
        $this->json('POST', '/api/v1/accountsReceivables', $accountsReceivable);

        $this->assertApiResponse($accountsReceivable);
    }

    /**
     * @test
     */
    public function testReadAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $this->json('GET', '/api/v1/accountsReceivables/'.$accountsReceivable->id);

        $this->assertApiResponse($accountsReceivable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $editedAccountsReceivable = $this->fakeAccountsReceivableData();

        $this->json('PUT', '/api/v1/accountsReceivables/'.$accountsReceivable->id, $editedAccountsReceivable);

        $this->assertApiResponse($editedAccountsReceivable);
    }

    /**
     * @test
     */
    public function testDeleteAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $this->json('DELETE', '/api/v1/accountsReceivables/'.$accountsReceivable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/accountsReceivables/'.$accountsReceivable->id);

        $this->assertResponseStatus(404);
    }
}
