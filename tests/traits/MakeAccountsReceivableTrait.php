<?php

use Faker\Factory as Faker;
use App\Models\AccountsReceivable;
use App\Repositories\AccountsReceivableRepository;

trait MakeAccountsReceivableTrait
{
    /**
     * Create fake instance of AccountsReceivable and save it in database
     *
     * @param array $accountsReceivableFields
     * @return AccountsReceivable
     */
    public function makeAccountsReceivable($accountsReceivableFields = [])
    {
        /** @var AccountsReceivableRepository $accountsReceivableRepo */
        $accountsReceivableRepo = App::make(AccountsReceivableRepository::class);
        $theme = $this->fakeAccountsReceivableData($accountsReceivableFields);
        return $accountsReceivableRepo->create($theme);
    }

    /**
     * Get fake instance of AccountsReceivable
     *
     * @param array $accountsReceivableFields
     * @return AccountsReceivable
     */
    public function fakeAccountsReceivable($accountsReceivableFields = [])
    {
        return new AccountsReceivable($this->fakeAccountsReceivableData($accountsReceivableFields));
    }

    /**
     * Get fake data of AccountsReceivable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAccountsReceivableData($accountsReceivableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'Name' => $fake->word,
            'Unit' => $fake->word,
            'Plate_No' => $fake->word,
            'last_change_oil' => $fake->word,
            'type' => $fake->word,
            'grand_total' => $fake->word,
            'paid' => $fake->word,
            'balance' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $accountsReceivableFields);
    }
}
