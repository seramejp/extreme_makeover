<?php

use App\Models\AccountsReceivable;
use App\Repositories\AccountsReceivableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AccountsReceivableRepositoryTest extends TestCase
{
    use MakeAccountsReceivableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AccountsReceivableRepository
     */
    protected $accountsReceivableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->accountsReceivableRepo = App::make(AccountsReceivableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAccountsReceivable()
    {
        $accountsReceivable = $this->fakeAccountsReceivableData();
        $createdAccountsReceivable = $this->accountsReceivableRepo->create($accountsReceivable);
        $createdAccountsReceivable = $createdAccountsReceivable->toArray();
        $this->assertArrayHasKey('id', $createdAccountsReceivable);
        $this->assertNotNull($createdAccountsReceivable['id'], 'Created AccountsReceivable must have id specified');
        $this->assertNotNull(AccountsReceivable::find($createdAccountsReceivable['id']), 'AccountsReceivable with given id must be in DB');
        $this->assertModelData($accountsReceivable, $createdAccountsReceivable);
    }

    /**
     * @test read
     */
    public function testReadAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $dbAccountsReceivable = $this->accountsReceivableRepo->find($accountsReceivable->id);
        $dbAccountsReceivable = $dbAccountsReceivable->toArray();
        $this->assertModelData($accountsReceivable->toArray(), $dbAccountsReceivable);
    }

    /**
     * @test update
     */
    public function testUpdateAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $fakeAccountsReceivable = $this->fakeAccountsReceivableData();
        $updatedAccountsReceivable = $this->accountsReceivableRepo->update($fakeAccountsReceivable, $accountsReceivable->id);
        $this->assertModelData($fakeAccountsReceivable, $updatedAccountsReceivable->toArray());
        $dbAccountsReceivable = $this->accountsReceivableRepo->find($accountsReceivable->id);
        $this->assertModelData($fakeAccountsReceivable, $dbAccountsReceivable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAccountsReceivable()
    {
        $accountsReceivable = $this->makeAccountsReceivable();
        $resp = $this->accountsReceivableRepo->delete($accountsReceivable->id);
        $this->assertTrue($resp);
        $this->assertNull(AccountsReceivable::find($accountsReceivable->id), 'AccountsReceivable should not exist in DB');
    }
}
